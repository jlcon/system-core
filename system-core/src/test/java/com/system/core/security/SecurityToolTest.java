package com.system.core.security;
import com.alibaba.fastjson.JSON;
import com.system.core.security.SecurityTool;
import com.system.core.security.enums.AlgorithmEnum;
import com.system.core.security.enums.KeyPairGeneratorAlgorithmEnum;
import org.apache.commons.codec.binary.Base64;

import java.security.Key;
import java.security.KeyPair;
import java.util.List;

public class SecurityToolTest {

    public static void main(String[] args) throws Exception {
//        generateKey();

        String strConfigItemListEncrypt =  "NPYWOzuomOffAtqcY/4nUKTSh3HRDRPawlsXtvOYQboY6wVsHiUAgL2mrRn5uy7xkCCOClZTcIptEGCeevqomIjQfwSO1OyQHUuh5cTdjt+Amr9oY3t7NFEWtK4cRhq8NVTkjsVW7lB/d2A0oKTLMnt6D+t8JDcP/w7ZH/ze9/EHP9Hy5CkbH4x5CoRhRn/4J5wtocJezbOV6ZuAIZduN+eBfaBlZuNSKnNya9BZOO+pGB9bgZRV0FAhPfALpWGd/y3ZRwytP62Um3jLNkSdHtAfgHIO8SfyNRGQgGJcajf1c1LHLBOHrBVDH/uMCL7xKIhUBnwacnElje5trEcRWDxc7JG8dmqoc33q3beZGf6Ro7M4tuJe4WIq/M2jHwy6uEmFvEV8Pc8GiGE8UTxxg06rZlqb5L26OvjvfEIG6M2QMztuTC95v0JrP8choyjtxTXVYeVuYcIFsbguB9tD616ltTVhD8hyMjmqSfW/QAijvEu4GeaRg8iA3QQar8zYSotfInRY/MBifJ6H9lMiie4VvYrYYEBBikUSbiqPxUDy9xgGVFLrX4STbdPD/jLhr5YTmP5i1MVos3bQdM0ET44mLNQRaHO05Hopl4V4m9VbuzJhDZpwYTY/M1lWzPYwRoHDN2ytKNXRmhiuWYKk0L4cWegMtKGMJGYd4LvDgtR//cakR0+TaCKn2cw9cO9vhW2NLehb27Bw5EyXIvXYjBtbMHk1IrPV7gKkD1s69bh4TzvQeg4jJqODhG2op3HfX7qViBrXo3cWBBQU31zawXyv7Tz2MzTgsAPvpL/jmSlsr68I2xi6uXSAI9BDbRF+9X5kIkY6fggmOrTkIDwjNzLb+OVJz2Fvq8HYrijIcZzB8PYy5PQNHjIAs5kS3sIBK1K+D4PM9lF3OoiJYfur2w2SLq2UUcU/LaKawTFtm8fLHICiRmu/jrzMyAtMG5U1LwcoCk/7akzCdILig0PJAbmIQdHqh9PfFmOJP233Z3m23QqHmwx7gjQTdVZJCMRQCpLUB/y2HRSnPKJQrrkcAHaVYS9JzN/WUsJWmy4QA8nsbcG/N272scgkAwRT0Rdkw4g6c0CSSryXVKJNqGVDY7XNqqVesfyuXas5T9LQlwDNmLu5FfgGNckPtm5lTKifndHRzO3YltpBw1J6yjRCGrzqGskGXVCg6XKrz2u0YNgJ475hIE4iOnWzmz0rAilXXJp6BPuioSvCP9jvwCPYnx3MI/e7jCXTG3G4EhN2rZ2x1BO8iTRBlVMZetETRJQUs4P5GsF3N/v4GRsMSriDapL6Y+ZfRTjW4vW6CPMkvad8N3R3HJrgu1HW++VyAi0bb3bUZYTELNx1raRf/wEK1Gq9pg4Ntmv3OfSeU8MyHiKEYqDTUKEHdx0Ioy4Au1MQHUXmAeAkWTgrg7ROi5Llx0s9MPfMR11oPvOOzY5iInVFXSbIB1/nF752kET2G1FSEkrNfJOMsa0CS8ZQcmy5E8SkUdmUbVIo7B82XcktXPRgMqROQEY5dRMXh4cXfdWneSfuFYjfbGEeWwd8k5dnI7jjqoRNH8aMldYJmeouCrLgpiTLMDgKo7t8HRIjg1H1BmK9RiwWE12QvKMu5OceL16oj2ZcQ8ZCqJIrdXVqYrXWLj8gNVt2pkxd5aMMPpGiAHm1RnNZmbIzIerhuGUURzvaoiKBk0TY6KiR4hy0w6gfj/fZ1n/nR9LnLf4x9pPAKFJZsnIOpROt4HGZrMq6BPPJxMXG1NjQ1y1SPjGw3bDa3wbBYUZafhK86yoWgUh0O6opgrI8kZJwghwLJw8Ll9MUF04kt7ilK2Zlqx6ZezZFcqfGkzyY5RbXIqzhrWKhtxXCTEcdntNXBsL63htXRk/aFOIIrucUZoUIdTqmgWxzq8UP433X9Qdf/ZkMWF0G+tbDiMCyWtpMq3u1tyb1WjXDvV2d2RY/Y4Zb46rwpZE1dIAjoz15AKT24BD6eI8dXKSR63gAAk0Erp97tL+w2ei7JdCk+1BC+XZVSzjS1o4Eqbws79Ytk97F96cPdMMFOkdcMXeGyqF+Znu9sdLNX75ZRJ6rBcSJR3dJ0+G5ZsYeq9iGIZaTN2883bPmjWrMUA9RkL/vGNZ7X9fxwybWzKadLtMiVOhHy/oxyBPKxX2Hiz10vHq09xIvaF3+K2xC9JtZQ/OG7Yhvk9gXkd7pyMHipuNpvv2li5jM2+piXiIR7n7xxarbjpFKE+tz0U4JnV7ZG2SxDmvwZQJ7Bw1MteYPq1Lp9xq8hy/2zxqY7I1hHXR39bWEgONhEdcTLRDAhTbBeucPNNuTW97k08xFZgjtirQCZ6N4DqEgdOpOT1TP8fPErT8TzKvXDa5z/DqjoYGL54r0XYANGlmOMepJOKaJUCHiOPGtkZ+u8JgrevlCKTBpxArmjwst15PeaB5OcYAYo4gR3AVUWUBYdIuzf77rZekd1mnnTI6Am9W21KX8JbVIqYDJLsZwWpFv061a0VuRADa1GzZytjY8wB8+uKgbRC5qpCtPHNlmTwRo6xbaHBQH9/RBZpOfpQNvmqSTaiDnXZa4OkDyNwONz4MXvl7ELz86n/Ug9A7cNSCt3Ib2NuaJ37zcs2huv3zaDO7NPBPme+1hsNvNfMg6tEU9PzcO/nFMoW0a6B+x/DkNKwfZT+5TrulxGfo5g3mmv8ZiymkKEskGemqqXbdQkFxmJ+HMpEsB69U4gzb0KIg/ySlHMGUHa2V7LL5OACZDjf+vHjfxaOLudj3tnvBwvQUgOUiCiA9q8fRr8gW7bQ1fVgrdvdaFBWhlbRlzSqmLE5pkdgOpRVazflGLyWaiosLiRL71WgTkwjApOtyR8r53Wx9AfXeoxxYbnJhIni3q2zwsqptAoGHAUY2IyeRa6FWuKBViYnQWKR/oWPTRYL2aWd7TT0026+93El73GP/FqIGdJrayh/axP/OZD8y9IIki+8Z2GL46dUDHkTOOOXRlCnYs3QGrfO+P+u4jVnxqvb9spjLV+DmhJY4fR6njKJanrz3qDZ5WTq2OYIfwAX+V0x0xcBTzPn7k1uwK+8ZM7g/efnwgNMgPjhD14fQaX0KWdmJBiJ1QWmR+ZHp7XNenZ++X2gPZzGMtPjkKx7LnMHd4i72NDwprwv8qB0yLQJe3a3BPzChLPGJ4eFqKwnhSNXgckbrF/DbyEF4cECgA+0Yo9IcUmuyM7BR6/83NL/CfHYaBuveOoW2KSr9kggXjxJmh/Zh5qRuPUXM/n7tCS4oGG1HODO+5ICruNqPOdfuSR6QumWVhq8lPdfGqn7KDM6TEVNEOmIfEwmMBRdppoQtVPVstRRbl9ZAxq9NkX0zugAk11EJn9aSgn4nzJbojDJhAUhnWI4js47eJJwjFr3ikOQvu8/hAoToW3RSm1txFnA==";
        String privateKey = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAKk8DBjsygUL1sHrS5yO1cOGnEkRtfxsiUSTrGCi/r2OlHAbcqhWelb7H5jA/o60UF8ZB8nrz7D4iEX68ZK+Vd24DD/O55o4aMg+y7bG5qXa48g0jPj2LV8uon2wd+Kt/DSTRwB37AUSWtCDO4JKGAbd9SZYgl6VS7bigeaEXOVhAgMBAAECgYEAnj0+VoP59bP3L8VyIsZPzI5uTchYsNlPlKa8FHPz24yY8SZJFGwf4nrXNX6nxwmE6Ra/eKecwK3yxfR5sytznuLyZNAszc3+97JW2JniMjlAAxWtMEim+7bZOBq/mqZREZBL5LNF+XQtTy/ima27+R2UnUQy4BGlSKU/8BFnutECQQDYUmw6Re2/wmF9x4En4s/wX/wRtO6NThOBBn6f8CpZCWdCP3L8hSv4ij+VZ625rAKU4DdDB0c+i7NLF1WQmOY1AkEAyEaYq3BQtwsqrbjSC2/8OAAPI1K8wtHAETMkgjd+49UWT3k4q+4mSjv3haSRDoMbZu6Uc4wtLnJ3yzMU/hw3/QJAUKrTzxs6oHKlFllqUWxkLHgusI+vEaSW41t/prbRo+g8yFiO0Zn7nrA3K8jA4OdDAy9ljCpPqARCdPMbplNOLQJAGz2O8nKaZ+s66Nokp62ZrrxDUtikGBOTfkNoidWmirKnEDQ6wxfnWV2Cs0jI+iJXDc8Rng3I73JeXOMbzdxkuQJASAQeHVpK0TYGqNxEbaG4ekphEuf6s5Fa1Ykgy/9DxWvSl/Jy0QpNvhCZIuQmhrogLfSSTKeMljA/lG7riN83bg==";
        Key key = SecurityTool.getPrivateKey(privateKey, AlgorithmEnum.RSA);
        String strConfigItemList =  SecurityTool.decryptWithKey(strConfigItemListEncrypt,key);
        System.out.println("解密："+strConfigItemList);

    }

    private static void generateKey() {
        System.out.println("---------------------------");
        KeyPair keyPair = SecurityTool.generateKey(KeyPairGeneratorAlgorithmEnum.RSA, 1024);
        String privateKey =  Base64.encodeBase64String(keyPair.getPrivate().getEncoded());
        String publicKey = Base64.encodeBase64String(keyPair.getPublic().getEncoded());
        System.out.println("--publicKey:"+publicKey);
        System.out.println("--privateKey:"+privateKey);
    }
}
