package com.system.boss.optlog.dto;

import org.apache.commons.lang3.StringUtils;

import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;
import com.system.core.view.layui.table.TableViewReqDto;

import io.swagger.v3.oas.annotations.media.Schema;

@Data
@EqualsAndHashCode(callSuper = true)
public class BossOptLogQueryFilter extends TableViewReqDto{

	//
	@Schema(description = "")
	private Long logId;
	//操作人称谓
	@Schema(description = "操作人称谓")
	private String optUserName;
	//操作人IP地址
	@Schema(description = "操作人IP地址")
	private String ipAddr;
	//操作项
	@Schema(description = "操作项")
	private String optItemCode;
	//备注
	@Schema(description = "备注")
	private String comment;
	//记录创建时间
	@Schema(description = "记录创建时间")
	private Date createTime;
	//记录更新时间
	@Schema(description = "记录更新时间")
	private Date updateTime;
	/**
	 * 记录创建时间
	 * jdbctype:timestamp
	 */
	private String createTimeStrStart;
	private String createTimeStrEnd;
	
	/**
	 * 记录更新时间
	 * jdbctype:timestamp
	 */
	private String updateTimeStrStart;
	private String updateTimeStrEnd;
	

	public String toQueryString() {
		StringBuilder param = new StringBuilder();
		/**
		 * 
		 * jdbcType:bigint
		 */
		if(getLogId() != null) {
			param.append("&logId=");
			param.append(getLogId());
		}
		/**
		 * 操作人称谓
		 * jdbcType:varchar(20)
		 */
		if(StringUtils.isNotBlank(getOptUserName())) {
			param.append("&optUserName=");
			param.append(getOptUserName());
		}
		/**
		 * 操作人IP地址
		 * jdbcType:varchar(10)
		 */
		if(StringUtils.isNotBlank(getIpAddr())) {
			param.append("&ipAddr=");
			param.append(getIpAddr());
		}
		/**
		 * 操作项
		 * jdbcType:varchar(200)
		 */
		if(StringUtils.isNotBlank(getOptItemCode())) {
			param.append("&optItemCode=");
			param.append(getOptItemCode());
		}
		/**
		 * 备注
		 * jdbcType:varchar(200)
		 */
		if(StringUtils.isNotBlank(getComment())) {
			param.append("&comment=");
			param.append(getComment());
		}
		/**
		 * 记录创建时间
		 * jdbcType:timestamp
		 */
		if(StringUtils.isNotBlank(getCreateTimeStrStart())) {
			param.append("&createTimeStrStart=");
			param.append(getCreateTimeStrStart());
		}
		if(StringUtils.isNotBlank(getCreateTimeStrEnd())) {
			param.append("&createTimeStrEnd=");
			param.append(getCreateTimeStrEnd());
		}
		/**
		 * 记录更新时间
		 * jdbcType:timestamp
		 */
		if(StringUtils.isNotBlank(getUpdateTimeStrStart())) {
			param.append("&updateTimeStrStart=");
			param.append(getUpdateTimeStrStart());
		}
		if(StringUtils.isNotBlank(getUpdateTimeStrEnd())) {
			param.append("&updateTimeStrEnd=");
			param.append(getUpdateTimeStrEnd());
		}
//		if(param.length() > 0)
//			return "?"+param.toString().substring(1, param.length());
//		else
		return param.toString();
	}
	
	@Override
	public String toString() {
		return toQueryString();
	}
}
