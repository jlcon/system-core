package com.system.boss.optlog.controller;

import java.util.List;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.system.boss.optlog.dto.BossOptLogQueryFilter;
import com.system.boss.optlog.model.BossOptLog;
import com.system.boss.optlog.service.BossOptLogService;
import com.system.core.results.ResultBase;
import com.system.core.view.layui.table.TableViewRspDto;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("boss/boss-log")
@Tag(name = "操作日志")
@RequiredArgsConstructor
public class BossOptLogRestController {
	
	private final BossOptLogService bossOptLogService;
	
	@PostMapping(value = "query-log-list")
	@Operation(summary ="日志列表")
	public TableViewRspDto<BossOptLog> bossOptLogQueryList(@RequestBody BossOptLogQueryFilter filter) {
		TableViewRspDto<BossOptLog> rsp = bossOptLogService.bossOptLogList(filter);
		return rsp;
	}
	
	@PostMapping(value = "get-log-key")
	@Operation(summary ="操作日志详情")
	public BossOptLog bossOptLogGetByPrimaryKey(@RequestBody BossOptLog bossOptLog){
		return bossOptLogService.bossOptLogGetByPrimaryKey(bossOptLog);
	}
	
	
	@PostMapping(value = "delete-log")
	@Operation(summary ="删除操作日志")
	public ResultBase bossOptLogDeleteItem(@RequestBody List<Long> keys) {
		return bossOptLogService.bossOptLogDeleteByPrimaryKey(keys);
	}
	
}