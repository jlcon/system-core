package com.system.boss.optlog.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Builder
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class IPAddr {

    private String origip;
    private String location;
    
    @Override
	public int hashCode() {
		return origip.hashCode();
	}
	@Override
	public boolean equals(Object obj) {
		IPAddr ipaddr = (IPAddr)obj;
		return this.origip.equals(ipaddr.getOrigip());
	}
}