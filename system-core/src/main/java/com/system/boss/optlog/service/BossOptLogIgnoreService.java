package com.system.boss.optlog.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.system.boss.optlog.dto.BossOptLogIgnoreAddItemReq;
import com.system.boss.optlog.dto.BossOptLogIgnoreQueryFilter;
import com.system.boss.optlog.dto.BossOptLogIgnoreUpdateItemReq;
import com.system.boss.optlog.mapper.BossOptLogIgnoreMapper;
import com.system.boss.optlog.mapper.BossOptLogMapper;
import com.system.boss.optlog.model.BossOptLogExample;
import com.system.boss.optlog.model.BossOptLogIgnore;
import com.system.boss.optlog.model.BossOptLogIgnoreExample;
import com.system.boss.optlog.model.BossOptLogIgnoreExample.Criteria;
import com.system.core.results.ResultBase;
import com.system.core.results.SimpleResultBuilder;
import com.system.core.security.NameUtil;
import com.system.core.view.layui.table.TableViewRspDto;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class BossOptLogIgnoreService {
	
	private SimpleDateFormat timeformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	@Autowired
	private BossOptLogIgnoreMapper bossOptLogIgnoreMapper;
	@Autowired
	private BossOptLogMapper bossOptLogMapper;
	
	/**
	 * 获取所有BossOptLogIgnore
	 */
	public List<BossOptLogIgnore> bossOptLogIgnoreListAll(){
		return bossOptLogIgnoreMapper.selectByExample(null);
	}
	
	/**
	 * 页面列表查询
	 * @return TableViewRspDto 返回TableViewRspDto视图对象，符合Layui规范
	 */
	public TableViewRspDto<BossOptLogIgnore> bossOptLogIgnoreList(BossOptLogIgnoreQueryFilter filter){
		log.debug("[bossOptLogIgnore]查询入参：{}",filter);
		TableViewRspDto<BossOptLogIgnore> pageResult = new TableViewRspDto<BossOptLogIgnore>();
		BossOptLogIgnoreExample example = new BossOptLogIgnoreExample();
		Criteria criteria = example.createCriteria();
		//在此插入查询条件start
		/**
		 * jdbcType:timestamp
		 */
		try {
			if(StringUtils.isNotBlank(filter.getCreateTimeStrStart())) {
				criteria.andCreateTimeGreaterThanOrEqualTo(timeformat.parse(filter.getCreateTimeStrStart()));
			}
			if(StringUtils.isNotBlank(filter.getCreateTimeStrEnd())) {
				criteria.andCreateTimeLessThanOrEqualTo(timeformat.parse(filter.getCreateTimeStrEnd()));
			}
		} catch (ParseException e) {
			e.printStackTrace();
			pageResult.setMessage(e.getMessage());
			return pageResult;
		}
		if(filter.getEnable() != null) {
			criteria.andEnableEqualTo(filter.getEnable());
		}
		if(filter.getLogIgnoreId() != null) {
			criteria.andLogIgnoreIdEqualTo(filter.getLogIgnoreId());
		}
		if(StringUtils.isNotBlank(filter.getOptItemCode())) {
			criteria.andOptItemCodeEqualTo(filter.getOptItemCode());
		}
		/**
		 * jdbcType:timestamp
		 */
		try {
			if(StringUtils.isNotBlank(filter.getUpdateTimeStrStart())) {
				criteria.andUpdateTimeGreaterThanOrEqualTo(timeformat.parse(filter.getUpdateTimeStrStart()));
			}
			if(StringUtils.isNotBlank(filter.getUpdateTimeStrEnd())) {
				criteria.andUpdateTimeLessThanOrEqualTo(timeformat.parse(filter.getUpdateTimeStrEnd()));
			}
		} catch (ParseException e) {
			e.printStackTrace();
			pageResult.setMessage(e.getMessage());
			return pageResult;
		}
		if(StringUtils.isNotBlank(filter.getOrderType())) {
        	example.setOrderByClause(NameUtil.javaName2_(filter.getOrderField())+" "+filter.getOrderType());
        } else {
        	example.setOrderByClause("create_time desc");
        }
		//在此插入查询条件end
		
		Page<BossOptLogIgnore> page = PageHelper.startPage(filter.getPage(), filter.getLimit());
		pageResult.setData(bossOptLogIgnoreMapper.selectByExample(example));
		pageResult.setCount(page.getTotal());
		pageResult.setCode(0);
		pageResult.setMessage("查询成功");
		return pageResult;
	}
	
	/**
	 * 根据主键获取单条数据
	 * @param bossOptLogIgnore 使用bossOptLogIgnore对象的主键属性LogIgnoreId类型是Long
	 */
	public BossOptLogIgnore bossOptLogIgnoreGetByPrimaryKey(BossOptLogIgnore bossOptLogIgnore) {
		BossOptLogIgnore result = bossOptLogIgnoreMapper.selectByPrimaryKey(bossOptLogIgnore.getLogIgnoreId());
		return result;
	}
	
	/**
	 * 根据主键更新数据
	 * @param bossOptLogIgnore 被更新对象（请注意，只更新有值属性）
	 */
	public ResultBase bossOptLogIgnoreUpdateByPrimaryKey(BossOptLogIgnoreUpdateItemReq bossOptLogIgnoreUpdateItemReq) {
		ResultBase resultBase = new ResultBase();
		BossOptLogIgnore bossOptLogIgnore = new BossOptLogIgnore();
		BeanUtils.copyProperties(bossOptLogIgnoreUpdateItemReq, bossOptLogIgnore);
		bossOptLogIgnoreMapper.updateByPrimaryKeySelective(bossOptLogIgnore);
		resultBase.setSuccess(true);
		resultBase.setMessage("更新成功");
		return resultBase;
	}
	
	/**
	 * 根据主键删除数据
	 * @param keys 被删除对象主键List集合
	 */
	@Transactional
	public ResultBase bossOptLogIgnoreDeleteByPrimaryKey(List<Long> keys) {
		ResultBase resultBase = new ResultBase();
		try {
			keys.forEach((key)->{
				bossOptLogIgnoreMapper.deleteByPrimaryKey(key);
			});
		} catch (Exception e) {
			resultBase.setMessage(e.getMessage());
			return resultBase;
		}
		resultBase.setSuccess(true);
		resultBase.setMessage("成功删除"+keys.size()+"条记录");
		return resultBase;
	}
	
	/**
	 * 新增数据
	 * @param bossOptLogIgnoreAddItemReqDto 新增对象
	 */
	@Transactional
	public ResultBase bossOptLogIgnoreSave(BossOptLogIgnoreAddItemReq bossOptLogIgnoreAddItemReq) {
		ResultBase resultBase = new ResultBase();
		BossOptLogIgnore bossOptLogIgnore = new BossOptLogIgnore();
		BeanUtils.copyProperties(bossOptLogIgnoreAddItemReq, bossOptLogIgnore);
		
		BossOptLogIgnoreExample bossOptLogIgnoreExample = new BossOptLogIgnoreExample();
		bossOptLogIgnoreExample.createCriteria().andOptItemCodeEqualTo(bossOptLogIgnore.getOptItemCode());
		List<BossOptLogIgnore> list = bossOptLogIgnoreMapper.selectByExample(bossOptLogIgnoreExample);
		
		
		BossOptLogExample bossOptLogExample = new BossOptLogExample();
		bossOptLogExample.createCriteria().andOptItemCodeEqualTo(bossOptLogIgnore.getOptItemCode());
		bossOptLogMapper.deleteByExample(bossOptLogExample);
		
		if(list.size() > 0) {
			return SimpleResultBuilder.success("已添加");
		}
		
		bossOptLogIgnoreMapper.insertSelective(bossOptLogIgnore);
		resultBase.setSuccess(true);
		resultBase.setMessage("新增成功");
		return resultBase;
	}
}
