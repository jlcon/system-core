package com.system.boss.optlog.controller;

import java.util.List;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.system.boss.optlog.dto.BossOptLogIgnoreAddItemReq;
import com.system.boss.optlog.dto.BossOptLogIgnoreQueryFilter;
import com.system.boss.optlog.dto.BossOptLogIgnoreUpdateItemReq;
import com.system.boss.optlog.model.BossOptLogIgnore;
import com.system.boss.optlog.service.BossOptLogIgnoreService;
import com.system.core.controller.ControllerBase;
import com.system.core.results.ResultBase;
import com.system.core.view.layui.table.TableViewRspDto;

@Controller
@RequestMapping("boss/boss_opt_log_ignore")
public class BossOptLogIgnoreController extends ControllerBase {
	@Autowired
	private BossOptLogIgnoreService bossOptLogIgnoreService;
	
	@RequiresPermissions("bossOptLogIgnore_topage")
	@RequestMapping(value = "to_page",method = RequestMethod.GET)
	public String toPage() {
		return "/boss/bossOptLog/bossOptLogIgnore-list";
	}
	
	@RequestMapping(value = "query_list",method = RequestMethod.POST)
	@ResponseBody
	public TableViewRspDto<BossOptLogIgnore> bossOptLogIgnoreQueryList(BossOptLogIgnoreQueryFilter filter) {
		TableViewRspDto<BossOptLogIgnore> rsp = bossOptLogIgnoreService.bossOptLogIgnoreList(filter);
		return rsp;
	}
	
	@RequestMapping(value = "get_item_by_key",method = RequestMethod.POST)
	@ResponseBody
	public BossOptLogIgnore bossOptLogIgnoreGetByPrimaryKey(BossOptLogIgnore bossOptLogIgnore){
		return bossOptLogIgnoreService.bossOptLogIgnoreGetByPrimaryKey(bossOptLogIgnore);
	}
	
	@RequiresPermissions("bossOptLogIgnore_add_opt")
	@RequestMapping(value = "add_item",method = RequestMethod.POST)
	@ResponseBody
	public ResultBase bossOptLogIgnoreAddItem(@Validated BossOptLogIgnoreAddItemReq bossOptLogIgnoreAddItemReqDto) {
		return bossOptLogIgnoreService.bossOptLogIgnoreSave(bossOptLogIgnoreAddItemReqDto);
	}
	
	@RequiresPermissions("bossOptLogIgnore_edit_opt")
	@RequestMapping(value = "update_item",method = RequestMethod.POST)
	@ResponseBody
	public ResultBase bossOptLogIgnoreUpdateItem(@Validated BossOptLogIgnoreUpdateItemReq bossOptLogIgnoreUpdateItemReqDto) {
		return bossOptLogIgnoreService.bossOptLogIgnoreUpdateByPrimaryKey(bossOptLogIgnoreUpdateItemReqDto);
	}
	
	@RequiresPermissions("bossOptLogIgnore_delete_opt")
	@RequestMapping(value = "delete_item",method = RequestMethod.POST)
	@ResponseBody
	public ResultBase bossOptLogIgnoreDeleteItem(@RequestBody List<Long> keys) {
		return bossOptLogIgnoreService.bossOptLogIgnoreDeleteByPrimaryKey(keys);
	}
}
