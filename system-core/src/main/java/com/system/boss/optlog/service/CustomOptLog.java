package com.system.boss.optlog.service;

import com.system.boss.optlog.model.BossOptLog;

public interface CustomOptLog {

	public void record(BossOptLog bossOptLog);
}
