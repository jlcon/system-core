package com.system.boss.optlog.mapper;

import java.util.List;

import com.system.boss.optlog.dto.BossOptLogQueryFilter;
import com.system.boss.optlog.model.BossOptLog;

public interface MyBossOptLogMapper {

	public List<BossOptLog> bossOptLogFindList(BossOptLogQueryFilter bossOptLog);
}
