package com.system.boss.optlog.service;

import org.springframework.stereotype.Service;

import com.system.boss.optlog.model.BossOptLog;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class DefaultCustomOptLog implements CustomOptLog{

	@Override
	public void record(BossOptLog bossOptLog) {
		log.debug("没有执行任何自定义日志");
	}

}
