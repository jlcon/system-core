package com.system.boss.optlog.dto;

import org.apache.commons.lang3.StringUtils;

import lombok.Data;

@Data
public class OptLogDeleteReq {

	private String startTime;
	private String endTime;
	
	public String getStartTimeStr() {
		if(StringUtils.isNotBlank(startTime)) {
			return startTime+" 00:00:00";
		} else {
			return null;
		}
	}
	
	public String getEndTimeStr() {
		if(StringUtils.isNotBlank(startTime)) {
			return endTime+" 23:59:59";
		} else {
			return null;
		}
	}
}
