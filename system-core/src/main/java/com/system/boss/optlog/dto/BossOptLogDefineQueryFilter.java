package com.system.boss.optlog.dto;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.system.core.view.layui.table.TableViewReqDto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BossOptLogDefineQueryFilter extends TableViewReqDto{

	//操作项定义
	@Schema(description = "操作项定义")
	private String codeDefine;
	//记录创建时间
	@Schema(description = "记录创建时间")
	private Date createTime;
	//
	@Schema(description = "")
	private Long defineId;
	//操作项
	@Schema(description = "操作项")
	private String optItemCode;
	//记录更新时间
	@Schema(description = "记录更新时间")
	private Date updateTime;
	/**
	 * 记录创建时间
	 * jdbctype:timestamp
	 */
	private String createTimeStrStart;
	private String createTimeStrEnd;
	
	/**
	 * 记录更新时间
	 * jdbctype:timestamp
	 */
	private String updateTimeStrStart;
	private String updateTimeStrEnd;
	

	public String toQueryString() {
		StringBuilder param = new StringBuilder();
		/**
		 * 操作项定义
		 * jdbcType:varchar(255)
		 */
		if(StringUtils.isNotBlank(getCodeDefine())) {
			param.append("&codeDefine=");
			param.append(getCodeDefine());
		}
		/**
		 * 记录创建时间
		 * jdbcType:timestamp
		 */
		if(StringUtils.isNotBlank(getCreateTimeStrStart())) {
			param.append("&createTimeStrStart=");
			param.append(getCreateTimeStrStart());
		}
		if(StringUtils.isNotBlank(getCreateTimeStrEnd())) {
			param.append("&createTimeStrEnd=");
			param.append(getCreateTimeStrEnd());
		}
		/**
		 * 
		 * jdbcType:bigint
		 */
		if(getDefineId() != null) {
			param.append("&defineId=");
			param.append(getDefineId());
		}
		/**
		 * 操作项
		 * jdbcType:varchar(200)
		 */
		if(StringUtils.isNotBlank(getOptItemCode())) {
			param.append("&optItemCode=");
			param.append(getOptItemCode());
		}
		/**
		 * 记录更新时间
		 * jdbcType:timestamp
		 */
		if(StringUtils.isNotBlank(getUpdateTimeStrStart())) {
			param.append("&updateTimeStrStart=");
			param.append(getUpdateTimeStrStart());
		}
		if(StringUtils.isNotBlank(getUpdateTimeStrEnd())) {
			param.append("&updateTimeStrEnd=");
			param.append(getUpdateTimeStrEnd());
		}
//		if(param.length() > 0)
//			return "?"+param.toString().substring(1, param.length());
//		else
		return param.toString();
	}
	
	@Override
	public String toString() {
		return toQueryString();
	}
}
