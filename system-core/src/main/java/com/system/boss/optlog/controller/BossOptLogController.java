package com.system.boss.optlog.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.system.boss.optlog.dto.BossOptLogQueryFilter;
import com.system.boss.optlog.dto.BossOptLogUpdateItemReq;
import com.system.boss.optlog.model.BossOptLog;
import com.system.boss.optlog.service.BossOptLogService;
import com.system.core.controller.ControllerBase;
import com.system.core.results.ResultBase;
import com.system.core.view.layui.table.TableViewRspDto;

/**
 * 1、菜单增加操作日志：/boss/boss_opt_log/to_page
 */
@Controller
@RequestMapping("boss/boss_opt_log")
public class BossOptLogController extends ControllerBase {
	@Autowired
	private BossOptLogService bossOptLogService;
	
//	@RequiresPermissions("bossOptLog_topage")
	@GetMapping(value = "to_page")
	public String toPage() {
		return "/boss/bossOptLog/bossOptLog-list";
	}
	
	@PostMapping(value = "query-list")
	@ResponseBody
	public TableViewRspDto<BossOptLog> bossOptLogQueryList(BossOptLogQueryFilter filter) {
		TableViewRspDto<BossOptLog> rsp = bossOptLogService.bossOptLogList(filter);
		return rsp;
	}
	
	@PostMapping(value = "get_item_by_key")
	@ResponseBody
	public BossOptLog bossOptLogGetByPrimaryKey(BossOptLog bossOptLog){
		return bossOptLogService.bossOptLogGetByPrimaryKey(bossOptLog);
	}
	
//	@RequiresPermissions("bossOptLog_edit_opt")
	@PostMapping(value = "update_item")
	@ResponseBody
	public ResultBase bossOptLogUpdateItem(@Validated BossOptLogUpdateItemReq bossOptLogUpdateItemReqDto) {
		return bossOptLogService.bossOptLogUpdateByPrimaryKey(bossOptLogUpdateItemReqDto);
	}
	
//	@RequiresPermissions("bossOptLog_delete_opt")
	@PostMapping(value = "delete_item")
	@ResponseBody
	public ResultBase bossOptLogDeleteItem(@RequestBody List<Long> keys) {
		return bossOptLogService.bossOptLogDeleteByPrimaryKey(keys);
	}
	
//	@RequiresPermissions("bossOptLog_delete_all_opt")
	@PostMapping(value = "delete_all_item")
	@ResponseBody
	public ResultBase bossOptLogDeleteAllItem() {
		return bossOptLogService.bossOptLogDeleteAll();
	}
}
