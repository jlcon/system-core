package com.system.boss.optlog.controller;

import java.util.List;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.system.boss.optlog.dto.BossOptLogDefineAddItemReq;
import com.system.boss.optlog.dto.BossOptLogDefineQueryFilter;
import com.system.boss.optlog.dto.BossOptLogDefineUpdateItemReq;
import com.system.boss.optlog.model.BossOptLogDefine;
import com.system.boss.optlog.service.BossOptLogDefineService;
import com.system.core.controller.ControllerBase;
import com.system.core.results.ResultBase;
import com.system.core.view.layui.table.TableViewRspDto;

@Controller
@RequestMapping("boss/boss_opt_log_define")
public class BossOptLogDefineController extends ControllerBase {
	@Autowired
	private BossOptLogDefineService bossOptLogDefineService;
	
	@RequiresPermissions("bossOptLogDefine_topage")
	@RequestMapping(value = "to_page",method = RequestMethod.GET)
	public String toPage() {
		return "/boss/bossOptLog/bossOptLogDefine-list";
	}
	
	@RequestMapping(value = "query_list",method = RequestMethod.POST)
	@ResponseBody
	public TableViewRspDto<BossOptLogDefine> bossOptLogDefineQueryList(BossOptLogDefineQueryFilter filter) {
		TableViewRspDto<BossOptLogDefine> rsp = bossOptLogDefineService.bossOptLogDefineList(filter);
		return rsp;
	}
	
	@RequestMapping(value = "get_item_by_key",method = RequestMethod.POST)
	@ResponseBody
	public BossOptLogDefine bossOptLogDefineGetByPrimaryKey(BossOptLogDefine bossOptLogDefine){
		return bossOptLogDefineService.bossOptLogDefineGetByPrimaryKey(bossOptLogDefine);
	}
	
	@RequiresPermissions("bossOptLogDefine_add_opt")
	@RequestMapping(value = "add_item",method = RequestMethod.POST)
	@ResponseBody
	public ResultBase bossOptLogDefineAddItem(@Validated BossOptLogDefineAddItemReq bossOptLogDefineAddItemReqDto) {
		return bossOptLogDefineService.bossOptLogDefineSave(bossOptLogDefineAddItemReqDto);
	}
	
	@RequiresPermissions("bossOptLogDefine_edit_opt")
	@RequestMapping(value = "update_item",method = RequestMethod.POST)
	@ResponseBody
	public ResultBase bossOptLogDefineUpdateItem(@Validated BossOptLogDefineUpdateItemReq bossOptLogDefineUpdateItemReqDto) {
		return bossOptLogDefineService.bossOptLogDefineUpdateByPrimaryKey(bossOptLogDefineUpdateItemReqDto);
	}
	
	@RequestMapping(value = "add_update_item",method = RequestMethod.POST)
	@ResponseBody
	public ResultBase bossOptLogDefineAddOrUpate(BossOptLogDefineAddItemReq bossOptLogDefineAddItemReq) {
		return bossOptLogDefineService.bossOptLogDefineAddOrUpdateByItemCode(bossOptLogDefineAddItemReq);
	}
	
	@RequiresPermissions("bossOptLogDefine_delete_opt")
	@RequestMapping(value = "delete_item",method = RequestMethod.POST)
	@ResponseBody
	public ResultBase bossOptLogDefineDeleteItem(@RequestBody List<Long> keys) {
		return bossOptLogDefineService.bossOptLogDefineDeleteByPrimaryKey(keys);
	}
	
	@RequestMapping(value = "select_by_logid",method = RequestMethod.POST)
	@ResponseBody
	public BossOptLogDefine bossOptLogDefineSelectByLogId(Long logId) {
		return bossOptLogDefineService.bossOptLogDefineByLogId(logId);
	}
	
}
