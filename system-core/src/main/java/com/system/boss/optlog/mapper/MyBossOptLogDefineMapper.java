package com.system.boss.optlog.mapper;

import java.util.List;

import com.system.boss.optlog.dto.BossOptLogQueryFilter;
import com.system.boss.optlog.model.BossOptLogDefine;

public interface MyBossOptLogDefineMapper {

	public List<BossOptLogDefine> selectDefineByLogId(BossOptLogQueryFilter bossOptLog);
}