package com.system.boss.optlog.dto;

import java.util.Date;

import jakarta.validation.constraints.Size;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class BossOptLogIgnoreUpdateItemReq {
	/**
	 * 记录创建时间(boss_opt_log_ignore)
	 */
	@Schema(description = "记录创建时间")
	private Date createTime;
	/**
	 * 是否可用。0-不可用；1-可用；(boss_opt_log_ignore)
	 */
	@Schema(description = "是否可用。0-不可用；1-可用；")
	private Boolean enable = false;
	/**
	 * (boss_opt_log_ignore)
	 */
	@Schema(description = "")
	private Long logIgnoreId;
	/**
	 * 忽略项code(boss_opt_log_ignore)
	 */
	@Schema(description = "忽略项code")
	@Size(min = 0,max = 200,message ="optItemCode字段超过限长（200）")
	private String optItemCode;
	/**
	 * 记录更新时间(boss_opt_log_ignore)
	 */
	@Schema(description = "记录更新时间")
	private Date updateTime;
}