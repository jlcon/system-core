package com.system.boss.optlog.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.system.boss.optlog.dto.BossOptLogDefineAddItemReq;
import com.system.boss.optlog.dto.BossOptLogDefineQueryFilter;
import com.system.boss.optlog.dto.BossOptLogDefineUpdateItemReq;
import com.system.boss.optlog.dto.BossOptLogQueryFilter;
import com.system.boss.optlog.mapper.BossOptLogDefineMapper;
import com.system.boss.optlog.mapper.MyBossOptLogDefineMapper;
import com.system.boss.optlog.model.BossOptLogDefine;
import com.system.boss.optlog.model.BossOptLogDefineExample;
import com.system.boss.optlog.model.BossOptLogDefineExample.Criteria;
import com.system.core.results.ResultBase;
import com.system.core.results.SimpleResultBuilder;
import com.system.core.security.NameUtil;
import com.system.core.view.layui.table.TableViewRspDto;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class BossOptLogDefineService {
	
	private SimpleDateFormat timeformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	@Autowired
	private BossOptLogDefineMapper bossOptLogDefineMapper;
	
	@Autowired
	private MyBossOptLogDefineMapper myBossOptLogDefineMapper;
	
	/**
	 * 获取所有BossOptLogDefine
	 */
	public List<BossOptLogDefine> bossOptLogDefineListAll(){
		return bossOptLogDefineMapper.selectByExample(null);
	}
	
	/**
	 * 页面列表查询
	 * @return TableViewRspDto 返回TableViewRspDto视图对象，符合Layui规范
	 */
	public TableViewRspDto<BossOptLogDefine> bossOptLogDefineList(BossOptLogDefineQueryFilter filter){
		log.debug("[bossOptLogDefine]查询入参：{}",filter);
		TableViewRspDto<BossOptLogDefine> pageResult = new TableViewRspDto<BossOptLogDefine>();
		BossOptLogDefineExample example = new BossOptLogDefineExample();
		Criteria criteria = example.createCriteria();
		//在此插入查询条件start
		if(StringUtils.isNotBlank(filter.getCodeDefine())) {
			criteria.andCodeDefineEqualTo(filter.getCodeDefine());
		}
		/**
		 * jdbcType:timestamp
		 */
		try {
			if(StringUtils.isNotBlank(filter.getCreateTimeStrStart())) {
				criteria.andCreateTimeGreaterThanOrEqualTo(timeformat.parse(filter.getCreateTimeStrStart()));
			}
			if(StringUtils.isNotBlank(filter.getCreateTimeStrEnd())) {
				criteria.andCreateTimeLessThanOrEqualTo(timeformat.parse(filter.getCreateTimeStrEnd()));
			}
		} catch (ParseException e) {
			e.printStackTrace();
			pageResult.setMessage(e.getMessage());
			return pageResult;
		}
		if(filter.getDefineId() != null) {
			criteria.andDefineIdEqualTo(filter.getDefineId());
		}
		if(StringUtils.isNotBlank(filter.getOptItemCode())) {
			criteria.andOptItemCodeEqualTo(filter.getOptItemCode());
		}
		/**
		 * jdbcType:timestamp
		 */
		try {
			if(StringUtils.isNotBlank(filter.getUpdateTimeStrStart())) {
				criteria.andUpdateTimeGreaterThanOrEqualTo(timeformat.parse(filter.getUpdateTimeStrStart()));
			}
			if(StringUtils.isNotBlank(filter.getUpdateTimeStrEnd())) {
				criteria.andUpdateTimeLessThanOrEqualTo(timeformat.parse(filter.getUpdateTimeStrEnd()));
			}
		} catch (ParseException e) {
			e.printStackTrace();
			pageResult.setMessage(e.getMessage());
			return pageResult;
		}
		if(StringUtils.isNotBlank(filter.getOrderType())) {
        	example.setOrderByClause(NameUtil.javaName2_(filter.getOrderField())+" "+filter.getOrderType());
        } else {
        	example.setOrderByClause("create_time desc");
        }
		//在此插入查询条件end
		
		Page<BossOptLogDefine> page = PageHelper.startPage(filter.getPage(), filter.getLimit());
		pageResult.setData(bossOptLogDefineMapper.selectByExample(example));
		pageResult.setCount(page.getTotal());
		pageResult.setCode(0);
		pageResult.setMessage("查询成功");
		return pageResult;
	}
	
	public BossOptLogDefine bossOptLogDefineByLogId(Long logId){
		BossOptLogQueryFilter bossOptLogQueryFilter = new BossOptLogQueryFilter();
		bossOptLogQueryFilter.setLogId(logId);
		List<BossOptLogDefine> list = myBossOptLogDefineMapper.selectDefineByLogId(bossOptLogQueryFilter);
		if (list.size() > 0) {
			return list.get(0);
		} else {
			return new BossOptLogDefine();
		}
	}
	
	/**
	 * 根据主键获取单条数据
	 * @param bossOptLogDefine 使用bossOptLogDefine对象的主键属性DefineId类型是Long
	 */
	public BossOptLogDefine bossOptLogDefineGetByPrimaryKey(BossOptLogDefine bossOptLogDefine) {
		BossOptLogDefine result = bossOptLogDefineMapper.selectByPrimaryKey(bossOptLogDefine.getDefineId());
		return result;
	}
	
	/**
	 * 根据主键更新数据
	 * @param bossOptLogDefine 被更新对象（请注意，只更新有值属性）
	 */
	public ResultBase bossOptLogDefineUpdateByPrimaryKey(BossOptLogDefineUpdateItemReq bossOptLogDefineUpdateItemReq) {
		ResultBase resultBase = new ResultBase();
		BossOptLogDefine bossOptLogDefine = new BossOptLogDefine();
		BeanUtils.copyProperties(bossOptLogDefineUpdateItemReq, bossOptLogDefine);
		bossOptLogDefineMapper.updateByPrimaryKeySelective(bossOptLogDefine);
		resultBase.setSuccess(true);
		resultBase.setMessage("更新成功");
		return resultBase;
	}
	
	/**
	 * 根据主键删除数据
	 * @param keys 被删除对象主键List集合
	 */
	@Transactional
	public ResultBase bossOptLogDefineDeleteByPrimaryKey(List<Long> keys) {
		ResultBase resultBase = new ResultBase();
		try {
			keys.forEach((key)->{
				bossOptLogDefineMapper.deleteByPrimaryKey(key);
			});
		} catch (Exception e) {
			resultBase.setMessage(e.getMessage());
			return resultBase;
		}
		resultBase.setSuccess(true);
		resultBase.setMessage("成功删除"+keys.size()+"条记录");
		return resultBase;
	}
	
	/**
	 * 新增数据
	 * @param bossOptLogDefineAddItemReqDto 新增对象
	 */
	@Transactional
	public ResultBase bossOptLogDefineSave(BossOptLogDefineAddItemReq bossOptLogDefineAddItemReq) {
		ResultBase resultBase = new ResultBase();
		BossOptLogDefine bossOptLogDefine = new BossOptLogDefine();
		BeanUtils.copyProperties(bossOptLogDefineAddItemReq, bossOptLogDefine);
		bossOptLogDefineMapper.insertSelective(bossOptLogDefine);
		resultBase.setSuccess(true);
		resultBase.setMessage("新增成功");
		return resultBase;
	}
	
	/**
	 * 新增或更新定义项目
	 * @param bossOptLogDefineAddItemReq
	 * @return 通用执行结果
	 */
	@Transactional
	public ResultBase bossOptLogDefineAddOrUpdateByItemCode(BossOptLogDefineAddItemReq bossOptLogDefineAddItemReq) {
		BossOptLogDefineExample bossOptLogDefineExample = new BossOptLogDefineExample();
		bossOptLogDefineExample.createCriteria().andOptItemCodeEqualTo(bossOptLogDefineAddItemReq.getOptItemCode());
		
		List<BossOptLogDefine> list = bossOptLogDefineMapper.selectByExample(bossOptLogDefineExample);
		
		if(list.size() == 0) {
			return bossOptLogDefineSave(bossOptLogDefineAddItemReq);
		} else {
			BossOptLogDefine bossOptLogDefine = list.get(0);
			bossOptLogDefine.setCodeDefine(bossOptLogDefineAddItemReq.getCodeDefine());
			bossOptLogDefineMapper.updateByPrimaryKeySelective(bossOptLogDefine);
			return SimpleResultBuilder.success("修改成功");
		}
	}
}
