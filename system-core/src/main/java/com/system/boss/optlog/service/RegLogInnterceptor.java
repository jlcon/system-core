package com.system.boss.optlog.service;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class RegLogInnterceptor implements WebMvcConfigurer{

	@Bean
	BossOptLogInterceptorService authInterceptor(){
        return new BossOptLogInterceptorService();
    }
	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		InterceptorRegistration interceptorRegistration = registry.addInterceptor(authInterceptor());
		interceptorRegistration.addPathPatterns("/boss/**");
		interceptorRegistration.addPathPatterns("/api/**");
	}

}
