package com.system.boss.optlog.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.AntPathMatcher;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.system.boss.optlog.dto.BossOptLogQueryFilter;
import com.system.boss.optlog.dto.BossOptLogUpdateItemReq;
import com.system.boss.optlog.dto.OptLogDeleteReq;
import com.system.boss.optlog.mapper.BossOptLogIgnoreMapper;
import com.system.boss.optlog.mapper.BossOptLogMapper;
import com.system.boss.optlog.mapper.MyBossOptLogMapper;
import com.system.boss.optlog.model.BossOptLog;
import com.system.boss.optlog.model.BossOptLogExample;
import com.system.boss.optlog.model.BossOptLogIgnore;
import com.system.boss.optlog.model.BossOptLogIgnoreExample;
import com.system.core.results.ResultBase;
import com.system.core.results.SimpleResultBuilder;
import com.system.core.view.common.response.CommonResponse;
import com.system.core.view.layui.table.TableViewRspDto;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class BossOptLogService {
	
	@Resource
	private BossOptLogMapper bossOptLogMapper;
	@Resource
	private MyBossOptLogMapper myBossOptLogMapper;
	@Resource
	private BossOptLogIgnoreMapper bossOptLogIgnoreMapper;
	@Resource
	private IpComponent ipComponent;
	
	public CommonResponse optLogDelete(OptLogDeleteReq req) {
		var bossOptLogExample = new BossOptLogExample();
		bossOptLogMapper.deleteByExample(bossOptLogExample);
		return CommonResponse.buildSuccessResult();
	}
	
	/**
	 * 页面列表查询
	 * @return TableViewRspDto 返回TableViewRspDto视图对象，符合Layui规范
	 */
	public TableViewRspDto<BossOptLog> bossOptLogList(BossOptLogQueryFilter filter){
		log.debug("[bossOptLog]查询入参：{}",filter);
		TableViewRspDto<BossOptLog> pageResult = new TableViewRspDto<BossOptLog>();
		
		Page<BossOptLog> page = PageHelper.startPage(filter.getPage(), filter.getLimit());
		pageResult.setData(myBossOptLogMapper.bossOptLogFindList(filter));
		pageResult.setCount(page.getTotal());
		pageResult.setCode(0);
		pageResult.setMessage("查询成功");
		return pageResult;
	}
	
	/**
	 * 根据主键获取单条数据
	 * @param bossOptLog 使用bossOptLog对象的主键属性LogId类型是Long
	 */
	public BossOptLog bossOptLogGetByPrimaryKey(BossOptLog bossOptLog) {
		BossOptLog result = bossOptLogMapper.selectByPrimaryKey(bossOptLog.getLogId());
		return result;
	}
	
	/**
	 * 根据主键更新数据
	 * @param bossOptLog 被更新对象（请注意，只更新有值属性）
	 */
	public ResultBase bossOptLogUpdateByPrimaryKey(BossOptLogUpdateItemReq bossOptLogUpdateItemReq) {
		BossOptLog bossOptLog = new BossOptLog();
		BeanUtils.copyProperties(bossOptLogUpdateItemReq, bossOptLog);
		bossOptLogMapper.updateByPrimaryKeySelective(bossOptLog);
		return SimpleResultBuilder.success("更新成功");
	}
	
	/**
	 * 根据主键删除数据
	 * @param keys 被删除对象主键List集合
	 */
	@Transactional
	public ResultBase bossOptLogDeleteByPrimaryKey(List<Long> keys) {
		ResultBase resultBase = new ResultBase();
		try {
			keys.forEach((key)->{
				bossOptLogMapper.deleteByPrimaryKey(key);
			});
		} catch (Exception e) {
			resultBase.setMessage(e.getMessage());
			return resultBase;
		}
		resultBase.setSuccess(true);
		resultBase.setMessage("成功删除"+keys.size()+"条记录");
		return resultBase;
	}
	
	public ResultBase bossOptLogDeleteAll() {
		int all = bossOptLogMapper.deleteByExample(null);
		return SimpleResultBuilder.success("已删除"+all+"条数据");
	}
	
	@Async
	@EventListener
	public void bossOptLogListener(BossOptLog bossOptLog) {
		BossOptLogIgnoreExample bossOptLogIgnoreExample = new BossOptLogIgnoreExample();
		bossOptLogIgnoreExample.createCriteria().andEnableEqualTo(true);
		List<BossOptLogIgnore> ignoreList = bossOptLogIgnoreMapper.selectByExample(bossOptLogIgnoreExample);
		AntPathMatcher antPathMatcher = new AntPathMatcher();
//		if(!ignoreList.stream().anyMatch(item -> item.getOptItemCode().equalsIgnoreCase(bossOptLog.getOptItemCode()))) {
//			bossOptLogMapper.insertSelective(bossOptLog);
//		}
		String location = ipComponent.getIpLocation(bossOptLog.getIpAddr());
		bossOptLog.setLocation(location);
		if(!ignoreList.stream().anyMatch(item -> antPathMatcher.match(item.getOptItemCode(), bossOptLog.getOptItemCode()))) {
			bossOptLogMapper.insertSelective(bossOptLog);
		}
	}
}
