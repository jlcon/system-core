package com.system.boss.optlog.dto;

import java.util.Date;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class BossOptLogAddItemReq {
	/**
	 * (boss_opt_log)
	 */
	@Schema(description = "")
	private Long logId;
	/**
	 * 操作人称谓(boss_opt_log)
	 */
	@Schema(description = "操作人称谓")
	@Size(min = 0,max = 20,message ="optUserName字段超过限长（20）")
	private String optUserName;
	/**
	 * 操作人IP地址(boss_opt_log)
	 */
	@Schema(description = "操作人IP地址")
	@Size(min = 0,max = 10,message ="ipAddr字段超过限长（10）")
	private String ipAddr;
	/**
	 * 操作项(boss_opt_log)
	 */
	@Schema(description = "操作项")
	@Size(min = 0,max = 200,message ="optItemCode字段超过限长（200）")
	private String optItemCode;
	/**
	 * 备注(boss_opt_log)
	 */
	@Schema(description = "备注")
	@Size(min = 0,max = 200,message ="comment字段超过限长（200）")
	private String comment;
	/**
	 * 记录创建时间(boss_opt_log)
	 */
	@Schema(description = "记录创建时间")
	private Date createTime;
	/**
	 * 记录更新时间(boss_opt_log)
	 */
	@Schema(description = "记录更新时间")
	private Date updateTime;
}