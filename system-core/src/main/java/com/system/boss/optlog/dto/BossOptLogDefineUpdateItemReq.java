package com.system.boss.optlog.dto;

import java.util.Date;

import jakarta.validation.constraints.Size;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class BossOptLogDefineUpdateItemReq {
	/**
	 * 操作项定义(boss_opt_log_define)
	 */
	@Schema(description = "操作项定义")
	@Size(min = 0,max = 255,message ="codeDefine字段超过限长（255）")
	private String codeDefine;
	/**
	 * 记录创建时间(boss_opt_log_define)
	 */
	@Schema(description = "记录创建时间")
	private Date createTime;
	/**
	 * (boss_opt_log_define)
	 */
	@Schema(description = "")
	private Long defineId;
	/**
	 * 操作项(boss_opt_log_define)
	 */
	@Schema(description = "操作项")
	@Size(min = 0,max = 200,message ="optItemCode字段超过限长（200）")
	private String optItemCode;
	/**
	 * 记录更新时间(boss_opt_log_define)
	 */
	@Schema(description = "记录更新时间")
	private Date updateTime;
}