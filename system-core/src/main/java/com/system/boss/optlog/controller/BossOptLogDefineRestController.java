package com.system.boss.optlog.controller;

import java.util.List;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.system.boss.optlog.dto.BossOptLogDefineAddItemReq;
import com.system.boss.optlog.dto.BossOptLogDefineQueryFilter;
import com.system.boss.optlog.dto.BossOptLogDefineUpdateItemReq;
import com.system.boss.optlog.model.BossOptLogDefine;
import com.system.boss.optlog.service.BossOptLogDefineService;
import com.system.core.controller.ControllerBase;
import com.system.core.results.ResultBase;
import com.system.core.view.layui.table.TableViewRspDto;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("boss/opt-log-define")
@Tag(name = "操作日志")
@RequiredArgsConstructor
public class BossOptLogDefineRestController extends ControllerBase {
	
	private final BossOptLogDefineService bossOptLogDefineService;
	
	@PostMapping(value = "query-list")
	@Operation(summary ="日志定义列表")
	public TableViewRspDto<BossOptLogDefine> bossOptLogDefineQueryList(BossOptLogDefineQueryFilter filter) {
		TableViewRspDto<BossOptLogDefine> rsp = bossOptLogDefineService.bossOptLogDefineList(filter);
		return rsp;
	}
	
	@PostMapping(value = "get-item-by-key")
	@Operation(summary ="日志定义详情")
	public BossOptLogDefine bossOptLogDefineGetByPrimaryKey(BossOptLogDefine bossOptLogDefine){
		return bossOptLogDefineService.bossOptLogDefineGetByPrimaryKey(bossOptLogDefine);
	}
	
	@PostMapping(value = "add-item")
	@Operation(summary ="新增日志定义")
	public ResultBase bossOptLogDefineAddItem(@Validated BossOptLogDefineAddItemReq bossOptLogDefineAddItemReqDto) {
		return bossOptLogDefineService.bossOptLogDefineSave(bossOptLogDefineAddItemReqDto);
	}
	
	@PostMapping(value = "update_item")
	@Operation(summary ="更新日志定义")
	public ResultBase bossOptLogDefineUpdateItem(@Validated BossOptLogDefineUpdateItemReq bossOptLogDefineUpdateItemReqDto) {
		return bossOptLogDefineService.bossOptLogDefineUpdateByPrimaryKey(bossOptLogDefineUpdateItemReqDto);
	}
	
//	@PostMapping(value = "add_update_item")
//	@ResponseBody
//	public ResultBase bossOptLogDefineAddOrUpate(BossOptLogDefineAddItemReq bossOptLogDefineAddItemReq) {
//		return bossOptLogDefineService.bossOptLogDefineAddOrUpdateByItemCode(bossOptLogDefineAddItemReq);
//	}
	
	@PostMapping(value = "delete-_item")
	@Operation(summary ="删除日志定义")
	public ResultBase bossOptLogDefineDeleteItem(@RequestBody List<Long> keys) {
		return bossOptLogDefineService.bossOptLogDefineDeleteByPrimaryKey(keys);
	}
	
	@PostMapping(value = "select-by-logid")
	@Operation(summary ="日志定义详情")
	public BossOptLogDefine bossOptLogDefineSelectByLogId(Long logId) {
		return bossOptLogDefineService.bossOptLogDefineByLogId(logId);
	}
	
}
