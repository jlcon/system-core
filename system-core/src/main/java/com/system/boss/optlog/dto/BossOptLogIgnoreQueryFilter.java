package com.system.boss.optlog.dto;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.system.core.view.layui.table.TableViewReqDto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BossOptLogIgnoreQueryFilter extends TableViewReqDto{

	//记录创建时间
	@Schema(description = "记录创建时间")
	private Date createTime;
	//是否可用。0-不可用；1-可用；
	@Schema(description = "是否可用。0-不可用；1-可用；")
	private Boolean enable;
	//
	@Schema(description = "")
	private Long logIgnoreId;
	//忽略项code
	@Schema(description = "忽略项code")
	private String optItemCode;
	//记录更新时间
	@Schema(description = "记录更新时间")
	private Date updateTime;
	/**
	 * 记录创建时间
	 * jdbctype:timestamp
	 */
	private String createTimeStrStart;
	private String createTimeStrEnd;
	
	/**
	 * 记录更新时间
	 * jdbctype:timestamp
	 */
	private String updateTimeStrStart;
	private String updateTimeStrEnd;
	

	public String toQueryString() {
		StringBuilder param = new StringBuilder();
		/**
		 * 记录创建时间
		 * jdbcType:timestamp
		 */
		if(StringUtils.isNotBlank(getCreateTimeStrStart())) {
			param.append("&createTimeStrStart=");
			param.append(getCreateTimeStrStart());
		}
		if(StringUtils.isNotBlank(getCreateTimeStrEnd())) {
			param.append("&createTimeStrEnd=");
			param.append(getCreateTimeStrEnd());
		}
		/**
		 * 是否可用。0-不可用；1-可用；
		 * jdbcType:tinyint(1)
		 */
		if(getEnable() != null) {
			param.append("&enable=");
			param.append(getEnable());
		}
		/**
		 * 
		 * jdbcType:bigint
		 */
		if(getLogIgnoreId() != null) {
			param.append("&logIgnoreId=");
			param.append(getLogIgnoreId());
		}
		/**
		 * 忽略项code
		 * jdbcType:varchar(200)
		 */
		if(StringUtils.isNotBlank(getOptItemCode())) {
			param.append("&optItemCode=");
			param.append(getOptItemCode());
		}
		/**
		 * 记录更新时间
		 * jdbcType:timestamp
		 */
		if(StringUtils.isNotBlank(getUpdateTimeStrStart())) {
			param.append("&updateTimeStrStart=");
			param.append(getUpdateTimeStrStart());
		}
		if(StringUtils.isNotBlank(getUpdateTimeStrEnd())) {
			param.append("&updateTimeStrEnd=");
			param.append(getUpdateTimeStrEnd());
		}
//		if(param.length() > 0)
//			return "?"+param.toString().substring(1, param.length());
//		else
		return param.toString();
	}
	
	@Override
	public String toString() {
		return toQueryString();
	}
}
