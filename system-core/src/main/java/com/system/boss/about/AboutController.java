package com.system.boss.about;

import java.net.InetAddress;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootVersion;
import org.springframework.core.SpringVersion;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.system.boss.openssl.service.OpenSSLGenTool;
import com.system.core.controller.ControllerBase;
import com.system.core.utils.ProcessExecutor;


@Controller
@RequestMapping("boss/about")
public class AboutController extends ControllerBase {
	
	@Autowired
	private OpenSSLGenTool openSSLGenTool;
	
	
	@RequestMapping(value = "to_page",method = RequestMethod.GET)
	public String toPage() throws Exception {
		getModelMap().put("osname", System.getProperty("os.name"));
		getModelMap().put("osarch", System.getProperty("os.arch"));
		getModelMap().put("osversion", System.getProperty("os.version"));
		
		getModelMap().put("javaVersion", System.getProperty("java.version"));
		getModelMap().put("javaHome", System.getProperty("java.home"));
		getModelMap().put("javaVendor", System.getProperty("java.vm.vendor"));
		
		getModelMap().put("springBootVersion", SpringBootVersion.getVersion());
		getModelMap().put("springVersion", SpringVersion.getVersion());
		getModelMap().put("localhost", InetAddress.getLocalHost().getHostAddress());
		try {
			getModelMap().put("opensslVersion", openSSLGenTool.version());
		} catch (Exception e) {
			e.printStackTrace();
			getModelMap().put("opensslVersion", "未安装["+e.getMessage()+"]");
		}
		getModelMap().put("cpu", ProcessExecutor.executeOnce("cat /proc/cpuinfo|grep 'model name'|tail -1|perl -pe 's/.*:\\s*(.*)/\\1/g'","/"));
		getModelMap().put("mem_total", ProcessExecutor.executeOnce("free -h|grep -i mem|awk '{print $2}'","/"));
		getModelMap().put("mem_available", ProcessExecutor.executeOnce("free -h|grep -i mem|awk '{print $7}'","/"));
		getModelMap().put("disk_info", ProcessExecutor.executeOnce("df -h","/")[1].replace("\n", "<br>").replace(" ", "&nbsp;"));

		return "/boss/about/about";
	}
}