package com.system.boss.sqlstorage;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.system.boss.sqlstorage.mapper.SqlStorageMapper;

import jakarta.annotation.Resource;

@Service
public class SqlService {
	@Resource
	private SqlStorageMapper sqlStorageMapper;

	public List<Map<String, Object>> executeDynamicSql(int id, Map<String, Object> paramMap) {
		Map<String, String> sqlParts = sqlStorageMapper.getSqlParts(id);
		String selectFrom = sqlParts.get("select_from");
		String whereClause = sqlParts.get("where_clause");
		paramMap.put("select_from", selectFrom);
		paramMap.put("where_clause", whereClause);
		return sqlStorageMapper.executeDynamicSql(paramMap);
	}
}
