package com.system.boss.sqlstorage;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/boss/sql")
public class SqlController {

	@Autowired
	private SqlService sqlService;

	@GetMapping("/test")
	public List<Map<String, Object>> executeDynamicSql(@RequestParam int id,
			@RequestParam(required = false) String username, @RequestParam(required = false) Integer age) {
		Map<String, Object> paramMap = new HashMap<>();
		if (username != null) {
			paramMap.put("username", username);
		}
		if (age != null) {
			paramMap.put("age", age);
		}
		return sqlService.executeDynamicSql(id, paramMap);
	}
}
