package com.system.boss.sqlstorage.mapper;

import java.util.List;
import java.util.Map;

/*CREATE TABLE sys_sql_storage (
    sql_storage_id INT PRIMARY KEY,
    select_from VARCHAR(255),
    where_clause VARCHAR(255)
);*/
public interface SqlStorageMapper {
	
	Map<String, String> getSqlParts(int id);

	List<Map<String, Object>> executeDynamicSql(Map<String, Object> paramMap);
}
