package com.system.boss.monitor.service;

import java.io.BufferedReader;
import java.io.IOException;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import com.system.boss.monitor.service.dto.MonitorRsp;
import com.system.core.utils.ProcessExecutor;

@Service
public class MonitorService {
	
	public static StringBuilder sb = new StringBuilder();

	@PostConstruct
	public MonitorRsp test() {
		MonitorRsp monitorRsp = new MonitorRsp();
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				BufferedReader bufferedReader = ProcessExecutor.build("ping -t 127.0.0.1");
				String lineStr = null;
				try {
					while((lineStr = bufferedReader.readLine()) != null) {
						MonitorService.sb.append(lineStr);
						MonitorService.sb.append("<br>");
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
				
			}
		}).start();
		
		return monitorRsp;
	}
}
