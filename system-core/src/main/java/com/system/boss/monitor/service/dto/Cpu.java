package com.system.boss.monitor.service.dto;

import lombok.Data;

@Data
public class Cpu {

	private int processor;
	private String vendorId;
	private String cpuFamily;
	private String model;
	private String modelName;
	private String flags;
	private float cpuMHz;
	private String cacheSize;
	private String fpu;
	private String addressSizes;
}
