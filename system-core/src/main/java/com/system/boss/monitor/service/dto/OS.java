package com.system.boss.monitor.service.dto;

import lombok.Data;

@Data
public class OS {

	private String os;
	private String redHat;
}
