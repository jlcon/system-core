package com.system.boss.monitor.service.dto;

import lombok.Data;

@Data
public class JstatGc {
	
	// 第一个幸存区的大小
	private Float s0c;
	// 第二个幸存区的大小
	private Float s1c;
	// 第一个幸存区的使用大小
	private Float s0u;
	// 第二个幸存区的使用大小
	private Float s1u;
	// 伊甸园区的大小
	private Float ec;
	// 伊甸园区的使用大小
	private Float eu;
	// 老年代大小
	private Float oc;
	// 老年代使用大小
	private Float ou;
	// 方法区大小
	private Float mc;
	// 方法区使用大小
	private Float mu;
	// 压缩类空间大小
	private Float ccsc;
	// 压缩类空间使用大小
	private Float ccsu;
	// 年轻代垃圾回收次数
	private Integer ygc;
	// 年轻代垃圾回收消耗时间
	private Float ygct;
	// 老年代垃圾回收次数
	private Integer fgc;
	// 老年代垃圾回收消耗时间
	private Float fgct;
	// 垃圾回收消耗总时间
	private Float gct;
}
