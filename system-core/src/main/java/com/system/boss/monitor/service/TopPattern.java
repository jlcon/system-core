package com.system.boss.monitor.service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TopPattern {

	public static final String get(Pattern p,String str) {
		return get(p,str,1);
	}
	public static final String get(Pattern p,String str,int group) {
		Matcher matcher = p.matcher(str);
		if(matcher.find()) {
			return matcher.group(group);
		} else {
			return null;
		}
	}
	
	public static final Integer getInteger(String propertyName,String str) {
		try {
			return Integer.parseInt(get(propertyName,str));
		} catch (Exception e) {
			return 0;
		}
	}
	
	public static final Float getFloat(String propertyName,String str) {
		try {
			return Float.parseFloat(get(propertyName,str));
		} catch (Exception e) {
			return 0f;
		}
	}
	
	public static final Long getLong(String propertyName,String str) {
		try {
			return Long.parseLong(get(propertyName,str));
		} catch (Exception e) {
			return 0l;
		}
	}
	
	public static final String get(String propertyName,String str) {
		Pattern pattern = Pattern.compile("\\s*(\\d+\\.\\d+||\\d+)\\s*"+propertyName);
		Matcher matcher = pattern.matcher(str);
		if(matcher.find()) {
			return matcher.group(1);
		} else {
			return null;
		}
	}
}
