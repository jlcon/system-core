package com.system.boss.monitor.service.dto;

import lombok.Data;

@Data
public class TopUsed {

	private Float cpuUsed;
	private Float memUsed;
	private Float swapUsed;
}
