package com.system.boss.monitor.service.dto;

import lombok.Data;

@Data
public class Memory {
	
	private Integer memFree;
	private Integer memTotal;
	private Integer swapTotal;
	private Integer swapFree;

}
