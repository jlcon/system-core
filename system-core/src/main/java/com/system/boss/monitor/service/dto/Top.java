package com.system.boss.monitor.service.dto;

import java.util.List;

import lombok.Data;

@Data
public class Top {

	// task信息
	private Integer tasksTotal;
	private Integer tasksRunning;
	private Integer tasksSleeping;
	private Integer tasksStopped;
	private Integer tasksZombie;
	// CPU信息
	// 用户空间占用CPU百分比
	private Float cpuUser;
	// 内核空间占用CPU百分比
	private Float cpuSystem;
	// 用户进程空间内改变过优先级的进程占用CPU百分比
	private Float cpuNice;
	// 空闲CPU百分比
	private Float cpuIdle;
	// 等待输入输出的CPU时间百分比
	private Float cpuIOWait;
	// 硬件中断
	private Float cpuHardwareIRQ;
	// 软件中断
	private Float cpuSoftwareIRQ;
	// 用于有虚拟cpu的情况，用来指示被虚拟机偷掉的cpu时间
	private Float cpuStealTime;
	
	// 内存信息
	private Long memTotal;
	private Long memFree;
	private Long memUsed;
	private Long memBuffCache;
	
	// swap信息
	private Long swapTotal;
	private Long swapFree;
	private Long swapUsed;
	private Long swapAvailMem;
	
	private List<TopTask> topTask;
}
