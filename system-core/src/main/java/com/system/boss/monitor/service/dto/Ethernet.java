package com.system.boss.monitor.service.dto;

import org.apache.commons.lang3.StringUtils;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
public class Ethernet {

	private String name;
	private String inet;
	private String netmask;
	private String broadcast;
	private Integer mtu;
	private String inet6;
	private Long rxpackets;
	//接收字节数（合计）
	private Long rxbytes;
	private Long txpackets;
	private Integer costTime;
	//发送字节数（合计）
	private Long txbytes;
	private Long upPeerSecond;
	private Long downPeerSecond;
	private final Long timestamp = System.currentTimeMillis();
	
	//###网卡信息
	private String speed;
	private Integer upSpeed;
	private Integer downSpeed;
	/*
	 * 网卡接口支持的类型，
	 * FIBRE:光纤
	 * TP:双绞线，就是普通的RJ45网口 100Base-TX 
	 * AUI:粗缆接口 10Base-5 
	 * BNC:细缆接口 10Base-2
	 */
	private String supportedPorts;
	// 网卡的物理标识，如果两个device的PHYAD相同，表示在一块物理网卡上
	private String phyad;
	// 接口类型,Twisted Pair是双绞线,FIBRE是光纤
	private String port;
	// 工作模式，Full为全双工
	private String duplex;
	
	public float getSpeedValue() {
		if(StringUtils.isNotBlank(this.speed)) {
			return Float.parseFloat(StringUtils.remove(this.speed, "Mb/s").trim());
		} else {
			return 1000;
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ethernet other = (Ethernet) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	
}
