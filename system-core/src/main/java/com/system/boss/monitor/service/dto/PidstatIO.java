package com.system.boss.monitor.service.dto;

import java.time.LocalDateTime;

import lombok.Data;

@Data
public class PidstatIO {

	private Long uid;
	private Long pid;
	private Float read;
	private Float write;
	private Float ccwr;
	private String command;
	private LocalDateTime time;
}
