package com.system.boss.monitor.controller;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.system.boss.monitor.service.SystemResourceMonitor;
import com.system.boss.monitor.service.dto.Ethernet;
import com.system.boss.monitor.service.dto.ProcessDetail;
import com.system.boss.monitor.service.dto.Top;
import com.system.boss.monitor.service.dto.TopPageResponse;
import com.system.boss.monitor.service.dto.TopUsed;
import com.system.core.controller.ControllerBase;

@Controller
@RequestMapping("/boss/linux")
public class LinuxController extends ControllerBase{
	
	@Autowired
	private SystemResourceMonitor systemResourceMonitor;

	@RequestMapping("/ethernet")
	@ResponseBody
	public List<Ethernet> ethernet() {
		return systemResourceMonitor.getEthernet();
	}
	
	@RequestMapping("/eth-speed")
	@ResponseBody
	public List<Ethernet> ethSpeed(){
		return SystemResourceMonitor.ethCahce;
	}
	
	@RequestMapping("/processDetail")
	@ResponseBody
	public ProcessDetail getProcessDetail(Integer pid) throws Exception{
		return systemResourceMonitor.getProcessDetail(pid);
	}
	
	@RequestMapping("/top")
	@ResponseBody
	public TopPageResponse getTop() throws Exception {
		Top top = systemResourceMonitor.getTop();
		TopPageResponse page = new TopPageResponse();
		BeanUtils.copyProperties(top, page);
		page.setData(top.getTopTask());
		page.setSuccess(true);
		page.setCode(0);
		return page;
	}
	
	@RequestMapping("/topUsed")
	@ResponseBody
	public TopUsed getTopUsed() throws Exception{
		return systemResourceMonitor.getTopUsed();
	}
	
	@RequestMapping("/")
	public String homepage() {
		return "/system/linux";
	}
	
	@RequestMapping("/infos")
	public String systemInfo() {
		getModelMap().put("cpus", systemResourceMonitor.getCpu());
		getModelMap().put("mem", systemResourceMonitor.getMemory());
		getModelMap().put("disk", systemResourceMonitor.getDisk());
		getModelMap().put("eths", systemResourceMonitor.getEthernet());
		getModelMap().put("os", systemResourceMonitor.getOS());
		return "/system/infos";
	}
	
	@RequestMapping("/monitor")
	public String monitor() {
		return "/system/monitor";
	}
}
