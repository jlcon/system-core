package com.system.boss.monitor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.system.boss.monitor.service.JstatService;
import com.system.boss.monitor.service.dto.JstatGc;
import com.system.core.controller.ControllerBase;

@Controller
@RequestMapping("/boss/linux")
public class JvmController extends ControllerBase {

	@Autowired
	private JstatService jstatService;
	
	@RequestMapping("/jstat/gc")
	@ResponseBody
	public JstatGc jstatgc(Integer pid) throws Exception {
		return jstatService.gc(pid);
	}
}
