package com.system.boss.monitor.service.dto;

import lombok.Data;

@Data
public class TopTask {

	private Long pid;
	private String user;
	// 优先级
	private String pr;
	// nice值,负值表示高优先级，正值表示低优先级
	private Integer ni;
	
	// 进程使用的虚拟内存总量，单位kb。VIRT=SWAP+RES
	private String virt;
	// 常驻内存
	private String res;
	// 共享内存
	private String shr;
	
	// 进程状态。（D=不可中断的睡眠状态，R=运行，S=睡眠，T=跟踪/停止，Z=僵尸进程）
	private String s;
	// 上次更新到现在的CPU时间占用百分比
	private float cpu;
	// 进程使用的物理内存百分比
	private float mem;
	// 进程使用的CPU时间总计，单位1/100秒
	private String time;
	// 命令名/命令行
	private String command;
	
	// io信息
	private Float read;
	private Float write;
}
