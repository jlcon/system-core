package com.system.boss.monitor.service.dto;

import java.util.LinkedList;

import lombok.Data;

@Data
public class MonitorRsp {

	private LinkedList<Top> topHistory = new LinkedList<Top>();
	private LinkedList<Ethernet> netHistory = new LinkedList<Ethernet>();
}
