package com.system.boss.monitor.service.dto;

import lombok.Data;

@Data
public class ProcessDetail {

	private String user;
	private int pid;
	// CPU使用百分比
	private float cpu;
	// 内存使用百分比
	private float mem;
	// 进程的虚拟大小
	private int vsz;
	// 驻留集的大小，可以理解为内存中页的数量
	private int rss;
	// 控制终端的ID
	private String tty;
	// 进程启动的时间
	private String start;
	/**
	 *  D 无法中断的休眠状态（通常 IO 的进程）
	 *	R 正在运行可中在队列中可过行的
	 *	S 处于休眠状态
	 *	T 停止或被追踪
	 *	W 进入内存交换
	 *	X 死掉的进程
	 *	Z 僵尸进程
	 */
	private String stat;
	// 进程已经消耗的CPU时间，注意是消耗CPU的时间
	private String time;
	private String command;
}
