package com.system.boss.monitor.service.dto;

import lombok.Data;

@Data
public class Disk {

	private String fileSystem;
	private String blocks;
	private String used;
	private String available;
	private String use;
	private String mountedOn;
}
