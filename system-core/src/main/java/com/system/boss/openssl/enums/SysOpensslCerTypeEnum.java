package com.system.boss.openssl.enums;

public enum SysOpensslCerTypeEnum {

	ROOTCA("ROOTCA", "ROOTCA"),
	SUBCA("SUBCA", "SUBCA"),
	CRL("CRL", "CRL"),
	USER("USER", "USER");
	
	private String code;
	private String message;

	public static SysOpensslCerTypeEnum getEnumByMessage(String message) {
		if (message == null)
			return null;
		for (SysOpensslCerTypeEnum _enum : values()) {
			if (_enum.getMessage().equalsIgnoreCase(message)) {
				return _enum;
			}
		}
		return null;
	}

	/**
	 * 获取全部枚举
	 * 
	 * @return List
	 */
	public static java.util.List<SysOpensslCerTypeEnum> getAllEnum() {
		java.util.List<SysOpensslCerTypeEnum> list = new java.util.ArrayList<SysOpensslCerTypeEnum>(values().length);
		for (SysOpensslCerTypeEnum _enum : values()) {
			list.add(_enum);
		}
		return list;
	}

	/**
	 * 获取全部枚举值
	 * 
	 * @return List
	 */
	public static java.util.List<String> getAllEnumCode() {
		java.util.List<String> list = new java.util.ArrayList<String>(values().length);
		for (SysOpensslCerTypeEnum _enum : values()) {
			list.add(_enum.getCode());
		}
		return list;
	}

	private SysOpensslCerTypeEnum(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
