package com.system.boss.openssl.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import jakarta.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.system.boss.openssl.service.OpenSSLBackupService;
import com.system.core.controller.ControllerBase;
import com.system.core.results.ResultBase;
import com.system.core.results.SimpleResultBuilder;


@Controller
@RequestMapping("boss/openssl/backup")
public class OpenSSLBackupController extends ControllerBase {
	
	@Autowired
	private OpenSSLBackupService openSSLBackupService;
	
	@RequestMapping(value = "backup-all",method = RequestMethod.GET)
	public void opensslBackup(HttpServletResponse response) {
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd.HHmmss");
		
		response.setCharacterEncoding("utf-8");
		response.setHeader("Content-Disposition", "attchement;filename=openssl-crts-all-"+format.format(Calendar.getInstance().getTime())+".zip");
		response.setContentType("multipart/form-data");
		
		try {
			openSSLBackupService.opensslBackup(response.getOutputStream());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "restore-backup-all",method = RequestMethod.POST)
	@ResponseBody
	public ResultBase opensslRestoreBackup(@RequestParam("file") MultipartFile file) throws Exception {
		
		openSSLBackupService.opensslRestoreBackup(file.getInputStream());
		return SimpleResultBuilder.success("恢复成功");
	}
	
}