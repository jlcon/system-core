package com.system.boss.openssl.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.system.boss.openssl.service.OpenSSLConfigService;
import com.system.core.controller.ControllerBase;
import com.system.core.results.ResultBase;
import com.system.core.results.SimpleResultBuilder;


@Controller
@RequestMapping("boss/openssl/config")
public class OpenSSLConfigController extends ControllerBase {
	
	@Autowired
	private OpenSSLConfigService openSSLConfigService;
	
	@RequestMapping(value = "get",method = RequestMethod.POST)
	@ResponseBody
	public ResultBase readConfig() throws Exception {
		return SimpleResultBuilder.success(openSSLConfigService.readConfig());
	}
	
}