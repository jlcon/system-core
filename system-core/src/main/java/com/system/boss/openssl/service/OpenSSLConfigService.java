package com.system.boss.openssl.service;

import java.io.File;
import java.io.FileInputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class OpenSSLConfigService {

	@Autowired
	private OpenSSLGenTool openSSLGenTool;
	
	Pattern pattern = Pattern.compile("OPENSSLDIR:\\s*\"(.*?)\"",Pattern.CASE_INSENSITIVE);
	
	public String readConfig() throws Exception {
		String tmp = openSSLGenTool.version();
		log.debug(tmp);
		Matcher m = pattern.matcher(tmp);
		if (m.find()) {
			String path = m.group(1);
			File configFile = new File(path,"openssl.cnf");
			if(!configFile.exists()) {
				log.error("未找到文件：{}",configFile.getAbsolutePath());
				return null;
			} else {
				return IOUtils.toString(new FileInputStream(configFile),"utf-8");
			}
		} else {
			log.error("未找到匹配内容:{}",tmp);
			return null;
		}
	}
	
	public static void main(String[] args) throws Exception {
		OpenSSLConfigService openSSLConfigService = new OpenSSLConfigService();
		openSSLConfigService.readConfig();
	}
}
