package com.system.boss.openssl.enums;

public enum SysOpensslCerStatusEnum {

	NORMAL("NORMAL", "NORMAL"),
	INVOKED("INVOKED", "INVOKED"),
	GENFAIL("GENFAIL", "GENFAIL");
	
	private String code;
	private String message;

	public static SysOpensslCerStatusEnum getEnumByMessage(String message) {
		if (message == null)
			return null;
		for (SysOpensslCerStatusEnum _enum : values()) {
			if (_enum.getMessage().equalsIgnoreCase(message)) {
				return _enum;
			}
		}
		return null;
	}

	/**
	 * 获取全部枚举
	 * 
	 * @return List
	 */
	public static java.util.List<SysOpensslCerStatusEnum> getAllEnum() {
		java.util.List<SysOpensslCerStatusEnum> list = new java.util.ArrayList<SysOpensslCerStatusEnum>(values().length);
		for (SysOpensslCerStatusEnum _enum : values()) {
			list.add(_enum);
		}
		return list;
	}

	/**
	 * 获取全部枚举值
	 * 
	 * @return List
	 */
	public static java.util.List<String> getAllEnumCode() {
		java.util.List<String> list = new java.util.ArrayList<String>(values().length);
		for (SysOpensslCerStatusEnum _enum : values()) {
			list.add(_enum.getCode());
		}
		return list;
	}

	private SysOpensslCerStatusEnum(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
