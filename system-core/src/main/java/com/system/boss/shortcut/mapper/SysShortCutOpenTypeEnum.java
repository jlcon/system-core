package com.system.boss.shortcut.mapper;

public enum SysShortCutOpenTypeEnum {

	system("system", "系统链接"),
	linker("linker", "外部链接");
	
	private String code;
	private String message;

	public static SysShortCutOpenTypeEnum getEnumByMessage(String message) {
		if (message == null)
			return null;
		for (SysShortCutOpenTypeEnum _enum : values()) {
			if (_enum.getMessage().equalsIgnoreCase(message)) {
				return _enum;
			}
		}
		return null;
	}

	/**
	 * 获取全部枚举
	 * 
	 * @return List
	 */
	public static java.util.List<SysShortCutOpenTypeEnum> getAllEnum() {
		java.util.List<SysShortCutOpenTypeEnum> list = new java.util.ArrayList<SysShortCutOpenTypeEnum>(values().length);
		for (SysShortCutOpenTypeEnum _enum : values()) {
			list.add(_enum);
		}
		return list;
	}

	/**
	 * 获取全部枚举值
	 * 
	 * @return List
	 */
	public static java.util.List<String> getAllEnumCode() {
		java.util.List<String> list = new java.util.ArrayList<String>(values().length);
		for (SysShortCutOpenTypeEnum _enum : values()) {
			list.add(_enum.getCode());
		}
		return list;
	}

	private SysShortCutOpenTypeEnum(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
