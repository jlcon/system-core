package com.system.boss.shortcut.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface SysShortCutMapper {
	
	@Select("SELECT * FROM sys_short_cut order by create_time desc")
	public List<SysShortCut> selectByExample();
	
	@Select("SELECT" + 
			"	sys_short_cut.`name`," + 
			"	sys_short_cut.url," + 
			"	sys_short_cut.`enable`," + 
			"	sys_short_cut.role_code," + 
			"	sys_short_cut.owner_type," + 
			"	sys_short_cut.open_type," + 
			"	sys_short_cut.ower_id," + 
			"	sys_short_cut.icon," + 
			"	sys_short_cut.sort " + 
			"FROM" + 
			"	sys_short_cut " + 
			"WHERE" + 
			"	sys_short_cut.`enable` = 1 " + 
			"ORDER BY sys_short_cut.sort DESC")
	public List<SysShortCut> actived();
	
	@Select("SELECT * FROM sys_short_cut WHERE sys_short_cut.short_cut_id=#{shortCutId}")
	public SysShortCut getShortCutByKey(Long shortCutId);
	
	@Delete("DELETE FROM sys_short_cut WHERE sys_short_cut.short_cut_id=#{shortCutId}")
	public int shortCutDeleteByKey(Long shortCutId);
	
	@Update({"<script>","update sys_short_cut",
		"    <set>" + 
		"      <if test=\"name != null\">" + 
		"        name = #{name,jdbcType=VARCHAR}," + 
		"      </if>" + 
		"      <if test=\"url != null\">" + 
		"        url = #{url,jdbcType=VARCHAR}," + 
		"      </if>" + 
		"      <if test=\"enable != null\">" + 
		"        enable = #{enable,jdbcType=BIT}," + 
		"      </if>" + 
		"      <if test=\"roleCode != null\">" + 
		"        role_code = #{roleCode,jdbcType=BIT}," + 
		"      </if>" + 
		"      <if test=\"ownerType != null\">" + 
		"        owner_type = #{ownerType,jdbcType=CHAR}," + 
		"      </if>" + 
		"      <if test=\"openType != null\">" + 
		"        open_type = #{openType,jdbcType=CHAR}," + 
		"      </if>" + 
		"      <if test=\"owerId != null\">" + 
		"        ower_id = #{owerId,jdbcType=BIGINT}," + 
		"      </if>" + 
		"      <if test=\"icon != null\">" + 
		"        icon = #{icon,jdbcType=VARCHAR}," + 
		"      </if>" + 
		"      <if test=\"sort != null\">" + 
		"        sort = #{sort,jdbcType=INTEGER}," + 
		"      </if>" + 
		"      <if test=\"createTime != null\">" + 
		"        create_time = #{createTime,jdbcType=TIMESTAMP}," + 
		"      </if>" + 
		"      <if test=\"updateTime != null\">" + 
		"        update_time = #{updateTime,jdbcType=TIMESTAMP}," + 
		"      </if>" + 
		"    </set>",
		"<where>"
		+ "short_cut_id=#{shortCutId}"
		+ "</where>",
		"</script>"})
	public int shortCutUpdate(SysShortCut sysShortCut);
	
	@Insert({"<script>",
		"insert into sys_short_cut",
		"    <trim prefix=\"(\" suffix=\")\" suffixOverrides=\",\">" + 
		"      <if test=\"name != null\">" + 
		"        name," + 
		"      </if>" + 
		"      <if test=\"url != null\">" + 
		"        url," + 
		"      </if>" + 
		"      <if test=\"enable != null\">" + 
		"        enable," + 
		"      </if>" + 
		"      <if test=\"roleCode != null\">" + 
		"        role_code," + 
		"      </if>" + 
		"      <if test=\"ownerType != null\">" + 
		"        owner_type," + 
		"      </if>" + 
		"      <if test=\"openType != null\">" + 
		"        open_type," + 
		"      </if>" + 
		"      <if test=\"owerId != null\">" + 
		"        ower_id," + 
		"      </if>" + 
		"      <if test=\"icon != null and icon!=''\">" + 
		"        icon," + 
		"      </if>" + 
		"      <if test=\"sort != null\">" + 
		"        sort," + 
		"      </if>" + 
		"      <if test=\"createTime != null\">" + 
		"        create_time," + 
		"      </if>" + 
		"      <if test=\"updateTime != null\">" + 
		"        update_time," + 
		"      </if>" + 
		"    </trim>" + 
		"    <trim prefix=\"values (\" suffix=\")\" suffixOverrides=\",\">" + 
		"      <if test=\"name != null\">" + 
		"        #{name,jdbcType=VARCHAR}," + 
		"      </if>" + 
		"      <if test=\"url != null\">" + 
		"        #{url,jdbcType=VARCHAR}," + 
		"      </if>" + 
		"      <if test=\"enable != null\">" + 
		"        #{enable,jdbcType=BIT}," + 
		"      </if>" + 
		"      <if test=\"roleCode != null\">" + 
		"        #{roleCode,jdbcType=BIT}," + 
		"      </if>" + 
		"      <if test=\"ownerType != null\">" + 
		"        #{ownerType,jdbcType=CHAR}," + 
		"      </if>" + 
		"      <if test=\"openType != null\">" + 
		"        #{openType,jdbcType=CHAR}," + 
		"      </if>" + 
		"      <if test=\"owerId != null\">" + 
		"        #{owerId,jdbcType=BIGINT}," + 
		"      </if>" + 
		"      <if test=\"icon != null and icon!=''\">" + 
		"        #{icon,jdbcType=VARCHAR}," + 
		"      </if>" + 
		"      <if test=\"sort != null\">" + 
		"        #{sort,jdbcType=INTEGER}," + 
		"      </if>" + 
		"    </trim>","</script>"})
	public Long shortCutAdd(SysShortCut sysShortCut);
}