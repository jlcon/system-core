package com.system.boss.shortcut.mapper;

public enum SysShortCutOwnerTypeEnum {

	system("system", "系统"),
	user("user", "用户");
	
	private String code;
	private String message;

	public static SysShortCutOwnerTypeEnum getEnumByMessage(String message) {
		if (message == null)
			return null;
		for (SysShortCutOwnerTypeEnum _enum : values()) {
			if (_enum.getMessage().equalsIgnoreCase(message)) {
				return _enum;
			}
		}
		return null;
	}

	/**
	 * 获取全部枚举
	 * 
	 * @return List
	 */
	public static java.util.List<SysShortCutOwnerTypeEnum> getAllEnum() {
		java.util.List<SysShortCutOwnerTypeEnum> list = new java.util.ArrayList<SysShortCutOwnerTypeEnum>(values().length);
		for (SysShortCutOwnerTypeEnum _enum : values()) {
			list.add(_enum);
		}
		return list;
	}

	/**
	 * 获取全部枚举值
	 * 
	 * @return List
	 */
	public static java.util.List<String> getAllEnumCode() {
		java.util.List<String> list = new java.util.ArrayList<String>(values().length);
		for (SysShortCutOwnerTypeEnum _enum : values()) {
			list.add(_enum.getCode());
		}
		return list;
	}

	private SysShortCutOwnerTypeEnum(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
