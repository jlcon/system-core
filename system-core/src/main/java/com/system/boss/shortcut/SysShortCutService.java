package com.system.boss.shortcut;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.system.boss.shortcut.mapper.SysShortCut;
import com.system.boss.shortcut.mapper.SysShortCutMapper;
import com.system.core.results.ResultBase;
import com.system.core.view.layui.table.TableViewReqDto;
import com.system.core.view.layui.table.TableViewRspDto;

@Service
public class SysShortCutService {

	@Autowired
	private SysShortCutMapper sysShortCutMapper;
	
	public List<SysShortCut> actived(){
		return sysShortCutMapper.actived();
	}
	
	public SysShortCut getShortCutByKey(Long shortCutId) {
		return sysShortCutMapper.getShortCutByKey(shortCutId);
	}
	public ResultBase shortCutAdd(SysShortCut sysShortCut) {
		ResultBase result = new ResultBase();
		sysShortCutMapper.shortCutAdd(sysShortCut);
		result.setSuccess(true);
		result.setMessage("快捷方式新增成功");
		return result;
	}
	
	public ResultBase shortCutDeleteByKey(List<Long> keys) {
		ResultBase result = new ResultBase();
		try {
			for(Long id:keys) {
				sysShortCutMapper.shortCutDeleteByKey(id);
			}
			result.setSuccess(true);
			result.setMessage("删除成功");
			return result;
		} catch (Exception e) {
			result.setMessage(e.getMessage());
			return result;
		}
	}
	
	public TableViewRspDto<SysShortCut> shortCutList(SysShortCut filter,TableViewReqDto page){
		TableViewRspDto<SysShortCut> pageResult = new TableViewRspDto<SysShortCut>();
		Page<SysShortCut> pager = PageHelper.startPage(page.getPage(), page.getLimit());
		List<SysShortCut> list = sysShortCutMapper.selectByExample();
		pageResult.setCount(pager.getTotal());
		pageResult.setCode(0);
		pageResult.setMessage("查询成功");
		pageResult.setData(list);
		return pageResult;
	}
	
	@Transactional
	public ResultBase shortCutUpdate(SysShortCut sysShortCut) {
		ResultBase result = new ResultBase();
		int updated = sysShortCutMapper.shortCutUpdate(sysShortCut);
		if(updated > 0) {
			result.setSuccess(true);
			result.setMessage("更新成功"+updated+"条记录");
		} else {
			result.setMessage("没有记录被更新~");
		}
		return result;
	}
}
