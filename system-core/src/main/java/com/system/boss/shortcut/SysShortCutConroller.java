package com.system.boss.shortcut;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.system.boss.shortcut.mapper.SysShortCut;
import com.system.boss.shortcut.mapper.SysShortCutOpenTypeEnum;
import com.system.boss.shortcut.mapper.SysShortCutOwnerTypeEnum;
import com.system.core.controller.ControllerBase;
import com.system.core.results.ResultBase;
import com.system.core.view.layui.table.TableViewReqDto;
import com.system.core.view.layui.table.TableViewRspDto;

@Controller
@RequestMapping("/boss/shortcut")
public class SysShortCutConroller extends ControllerBase {
	
	@Autowired
	private SysShortCutService sysShortCutService;

	@RequestMapping("/topage")
	public String toPage() {
		getModelMap().put("sysShortCutOwnerTypeEnum", SysShortCutOwnerTypeEnum.getAllEnum());
		getModelMap().put("sysShortCutOpenTypeEnum", SysShortCutOpenTypeEnum.getAllEnum());
		return "/boss/shortcut/shortcut-list";
	}
	
	@RequestMapping("/query-list")
	@ResponseBody
	public TableViewRspDto<SysShortCut> queryList(SysShortCut filter,TableViewReqDto page) {
		TableViewRspDto<SysShortCut> result = sysShortCutService.shortCutList(filter,page);
		return result;
	}
	
	@RequestMapping("/add-item")
	@ResponseBody
	public ResultBase shortCutAdd(SysShortCut sysShortCut) {
		return sysShortCutService.shortCutAdd(sysShortCut);
	}
	
	@RequestMapping("/update-item")
	@ResponseBody
	public ResultBase shortCutUpdate(SysShortCut sysShortCut) {
		return sysShortCutService.shortCutUpdate(sysShortCut);
	}
	
	@RequestMapping("/get-item-bykey")
	@ResponseBody
	public SysShortCut getShortCutByKey(Long shortCutId) {
		return sysShortCutService.getShortCutByKey(shortCutId);
	}
	@RequestMapping("/delete-item-bykey")
	@ResponseBody
	public ResultBase shortCutDeleteByKey(@RequestBody List<Long> keys) {
		return sysShortCutService.shortCutDeleteByKey(keys);
	}
}
