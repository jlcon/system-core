package com.system.base.files.controller;

import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dingtalk.api.response.OapiRobotSendResponse;
import com.system.core.dingtalk.DingTalkService;
import com.system.core.view.common.response.CommonDataResponse;

import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("api/dingtalk")
@Tag(name = "系统基础")
@RequiredArgsConstructor
public class DingTalkController {

	private final DingTalkService dingTalkService;
	
	@GetMapping(value = "msg-text")
	@Operation(summary = "钉钉消息(文本)")
	public CommonDataResponse<OapiRobotSendResponse> talkText(String configName,String text) throws Exception {
		Assert.notNull(configName, "配置名称不能为空");
		Assert.notNull(text, "发送文本不能为空");
		return CommonDataResponse.buildSuccessResult(dingTalkService.talkText(configName, text));
	}
}
