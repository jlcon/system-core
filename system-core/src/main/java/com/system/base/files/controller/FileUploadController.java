package com.system.base.files.controller;

import java.io.File;
import java.io.OutputStream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.system.base.files.service.BaseFileService;
import com.system.core.conf.SystemConfig;
import com.system.core.controller.ControllerBase;
import com.system.core.file.FileService;
import com.system.core.file.dto.FileManageResultKindEditor;
import com.system.core.results.FileUploadResult;
import com.system.core.utils.ImageSize;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;

@Controller
@RequestMapping("api/files")
@Tag(name = "系统基础")
@RequiredArgsConstructor
public class FileUploadController extends ControllerBase {

	private final FileService fileService;

	private final SystemConfig systemConfig;
	private final BaseFileService baseFileService;
	@Value("${app.system.uploadPath}")
	private String prefix;

	@GetMapping(value = "file-manage")
	@ResponseBody
	@Operation(summary = "kindEditor文件管理")
	public FileManageResultKindEditor fileManage(String path) {
		FileManageResultKindEditor result = fileService.fileManage(systemConfig.getUploadPath()+File.separator+path);
		result.setCurrentDirPath(path);
		result.setMoveupDirPath("");
		result.setCurrentUrl("/upload/"+path);
		return result;
	}

	@PostMapping(value = "upload")
	@ResponseBody
	@Operation(summary = "文件上传")
	public FileUploadResult upload(@RequestPart("file") MultipartFile file, HttpServletRequest request) {
		FileUploadResult result = baseFileService.upload(file);
		result.setRequestPath(request.getRequestURI());
		return result;
	}
	@PostMapping(value = "upload-thumb")
	@ResponseBody
	@Operation(summary = "文件上传(缩略图)")
	public FileUploadResult uploadThumb(@RequestPart("file") MultipartFile file, HttpServletRequest request) {
		FileUploadResult result = baseFileService.uploadThumb(file);
		result.setRequestPath(request.getRequestURI());
		return result;
	}

	@GetMapping(value = "view")
	@Schema(name = "按自定义尺寸展示图片")
	public void view(@RequestParam(required = true) @Parameter(name = "服务端文件地址") String filePath, 
			@RequestParam(required = true) @Parameter(name = "图片展示高度") int height, 
			@RequestParam(required = true) @Parameter(name = "图片展示宽度") int width,
			@RequestParam(defaultValue = "false") @Parameter(name = "是否强制比例展示") boolean force) throws Exception {
		getResponse().setContentType("image/png;");
		OutputStream out = getResponse().getOutputStream();
		ImageSize.readImage(out, prefix,filePath, width, height,force,"jpg");
		out.flush();
		out.close();
	}
}
