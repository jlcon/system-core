package com.system.base.files.service;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.system.core.conf.SystemConfig;
import com.system.core.file.FileService;
import com.system.core.results.FileUploadResult;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class BaseFileService {
	
	private final SystemConfig systemConfig;
	private final FileService fileService;

    /**
     * 上传文件
     *
     * @param file
     * @return
     */
    public FileUploadResult upload(MultipartFile file) {
        return fileService.uploadByFileName(file, systemConfig.getUploadPath(), fileService.getFileName(),systemConfig.getLimitKbSize());
    }
    /**
     * 上传文件
     *
     * @param file
     * @return
     */
    public FileUploadResult uploadThumb(MultipartFile file) {
    	return fileService.uploadByFileNameThumb(file, systemConfig.getUploadPath(), fileService.getFileName(),systemConfig.getLimitKbSize());
    }
}