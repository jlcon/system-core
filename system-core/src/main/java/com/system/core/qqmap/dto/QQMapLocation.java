package com.system.core.qqmap.dto;

public class QQMapLocation {

	private double lngx;
	private double laty;
	private String msg;
	public double getLaty() {
		return laty;
	}
	public double getLngx() {
		return lngx;
	}
	public String getMsg() {
		return msg;
	}
	public void setLaty(double laty) {
		this.laty = laty;
	}
	public void setLngx(double lngx) {
		this.lngx = lngx;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	@Override
	public String toString() {
		return "QQMapLocation [lngx=" + lngx + ", laty=" + laty + ", msg=" + msg + "]";
	}

}
