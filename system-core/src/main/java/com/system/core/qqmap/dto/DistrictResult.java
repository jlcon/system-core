package com.system.core.qqmap.dto;

import java.util.List;

public class DistrictResult {

	private int status;
	private String message;
	private String dataVersion;
	private List<List<District>> result;
	public String getDataVersion() {
		return dataVersion;
	}
	public String getMessage() {
		return message;
	}
	public List<List<District>> getResult() {
		return result;
	}
	public int getStatus() {
		return status;
	}
	public void setDataVersion(String dataVersion) {
		this.dataVersion = dataVersion;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public void setResult(List<List<District>> result) {
		this.result = result;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "DistrictResult [status=" + status + ", message=" + message + ", dataVersion=" + dataVersion
				+ ", result=" + result + "]";
	}
	
	
}
