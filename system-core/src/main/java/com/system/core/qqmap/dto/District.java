package com.system.core.qqmap.dto;

import java.util.Arrays;

public class District {

	private Long id;
	private String name;
	private String fullname;
	private String[] pinyin;
	private Location location;
	private Integer[] cidx;
	
	public Integer[] getCidx() {
		return cidx;
	}
	public String getFullname() {
		return fullname;
	}
	public Long getId() {
		return id;
	}
	public Location getLocation() {
		return location;
	}
	public String getName() {
		return name;
	}
	public String[] getPinyin() {
		return pinyin;
	}
	public void setCidx(Integer[] cidx) {
		this.cidx = cidx;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setPinyin(String[] pinyin) {
		this.pinyin = pinyin;
	}
	@Override
	public String toString() {
		return "District [id=" + id + ", name=" + name + ", fullname=" + fullname + ", pinyin="
				+ Arrays.toString(pinyin) + ", location=" + location + ", cidx=" + Arrays.toString(cidx) + "]";
	}
}
