package com.system.core.qqmap;

import java.util.TreeMap;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.system.core.qqmap.dto.DistrictResult;

@Service
public class DistrictService extends QQMapBase{

	private final String listApiURL = "/ws/district/v1/list";
	private final String getchildrenApiURL = "/ws/district/v1/getchildren";
	private final String searchApiURL = "/ws/district/v1/search";
	
	
	private TreeMap<String, Object> params = new TreeMap<>();
	
	public DistrictResult getchildren(Long id) {
		params.clear();
		params.put("id", id);
		DistrictResult result = JSONObject.parseObject(getService(getchildrenApiURL,params), DistrictResult.class);
		return result;
	}
	
	public DistrictResult list() {
		// 调用接口
		DistrictResult result = JSONObject.parseObject(getService(listApiURL,params), DistrictResult.class);
		return result;
	}
	
	
	public DistrictResult search(String keyword) {
		params.clear();
		params.put("keyword", keyword);
		return JSONObject.parseObject(getService(searchApiURL,params), DistrictResult.class);
	}
}
