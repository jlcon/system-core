package com.system.core.qqmap;

import java.util.TreeMap;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.system.core.qqmap.dto.QQMapLocation;

@Service
public class TranslateService extends QQMapBase{

	private final String translateApi = "/ws/coord/v1/translate";
	
	private TreeMap<String, Object> params = new TreeMap<>();
	
	/**
	 * 
	 * 输入的locations的坐标类型<br>
	 * 可选值为[1,6]之间的整数，每个数字代表的类型说明：<br>
	 * 1 GPS坐标<br>
	 * 2 sogou经纬度<br>
	 * 3 baidu经纬度<br>
	 * 4 mapbar经纬度<br>
	 * 5 [默认]腾讯、google、高德坐标<br>
	 * 6 sogou墨卡托<br>
	 */
	public QQMapLocation translate(double baidux,double baiduy,int type) {
		params.clear();
		params.put("locations", baiduy+","+baidux);
		params.put("type", type);
		
		// 调用接口
		String result = getService(translateApi,params);
		
		QQMapLocation location = new QQMapLocation();
		JSONObject json = JSONObject.parseObject(result);
		location.setMsg(json.getString("message"));
		if(json.getIntValue("status") == 0) {
			JSONObject json2 = json.getJSONArray("locations").getJSONObject(0);
			location.setLaty(json2.getDoubleValue("lat"));
			location.setLngx(json2.getDoubleValue("lng"));
		}
		return location;
	}
}
