package com.system.core.enums;

public enum BooleanEnum {

	TRUE("TRUE", "TRUE"),
	FALSE("FALSE","FALSE");

	private String code;
	private String message;

	public static BooleanEnum getEnumByMessage(String message) {
		if (message == null)
			return null;
		for (BooleanEnum _enum : values()) {
			if (_enum.getMessage().equalsIgnoreCase(message)) {
				return _enum;
			}
		}
		return null;
	}

	/**
	 * 获取全部枚举
	 * 
	 * @return List
	 */
	public static java.util.List<BooleanEnum> getAllEnum() {
		java.util.List<BooleanEnum> list = new java.util.ArrayList<BooleanEnum>(values().length);
		for (BooleanEnum _enum : values()) {
			list.add(_enum);
		}
		return list;
	}

	/**
	 * 获取全部枚举值
	 * 
	 * @return List
	 */
	public static java.util.List<String> getAllEnumCode() {
		java.util.List<String> list = new java.util.ArrayList<String>(values().length);
		for (BooleanEnum _enum : values()) {
			list.add(_enum.getCode());
		}
		return list;
	}

	private BooleanEnum(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
