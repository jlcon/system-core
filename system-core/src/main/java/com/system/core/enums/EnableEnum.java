package com.system.core.enums;

public enum EnableEnum {

	ENABLE("ENABLE", "ENABLE"),
	DISABLE("DISABLE", "DISABLE");

	private String code;
	private String message;

	public static EnableEnum getEnumByMessage(String message) {
		if (message == null)
			return null;
		for (EnableEnum _enum : values()) {
			if (_enum.getMessage().equalsIgnoreCase(message)) {
				return _enum;
			}
		}
		return null;
	}

	/**
	 * 获取全部枚举
	 * 
	 * @return List
	 */
	public static java.util.List<EnableEnum> getAllEnum() {
		java.util.List<EnableEnum> list = new java.util.ArrayList<EnableEnum>(values().length);
		for (EnableEnum _enum : values()) {
			list.add(_enum);
		}
		return list;
	}

	/**
	 * 获取全部枚举值
	 * 
	 * @return List
	 */
	public static java.util.List<String> getAllEnumCode() {
		java.util.List<String> list = new java.util.ArrayList<String>(values().length);
		for (EnableEnum _enum : values()) {
			list.add(_enum.getCode());
		}
		return list;
	}

	private EnableEnum(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
