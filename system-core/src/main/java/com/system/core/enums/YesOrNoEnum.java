package com.system.core.enums;

public enum YesOrNoEnum {

	YES("YES", "YES"),
	NO("NO", "NO");

	private String code;
	private String message;

	public static YesOrNoEnum getEnumByMessage(String message) {
		if (message == null)
			return null;
		for (YesOrNoEnum _enum : values()) {
			if (_enum.getMessage().equalsIgnoreCase(message)) {
				return _enum;
			}
		}
		return null;
	}

	/**
	 * 获取全部枚举
	 * 
	 * @return List
	 */
	public static java.util.List<YesOrNoEnum> getAllEnum() {
		java.util.List<YesOrNoEnum> list = new java.util.ArrayList<YesOrNoEnum>(values().length);
		for (YesOrNoEnum _enum : values()) {
			list.add(_enum);
		}
		return list;
	}

	/**
	 * 获取全部枚举值
	 * 
	 * @return List
	 */
	public static java.util.List<String> getAllEnumCode() {
		java.util.List<String> list = new java.util.ArrayList<String>(values().length);
		for (YesOrNoEnum _enum : values()) {
			list.add(_enum.getCode());
		}
		return list;
	}

	private YesOrNoEnum(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
