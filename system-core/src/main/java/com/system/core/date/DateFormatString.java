package com.system.core.date;

public class DateFormatString {

	public final static String dateString = "yyyy-MM-dd";
	public final static String dateTimeString = "yyyy-MM-dd HH:mm:ss";
	public final static String timeString = "HH:mm:ss";
}
