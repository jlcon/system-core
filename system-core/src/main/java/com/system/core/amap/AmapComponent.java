package com.system.core.amap;

import org.springframework.stereotype.Component;

import com.system.core.amap.dto.AmapConfig;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class AmapComponent {

	private final AmapConfig amapConfig;
}
