package com.system.core.controller;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

import lombok.Data;

@Data
public class RequestInfo {

	private String ip;
	private String requestURI;
	private Long addTime;
	private Long updateTime;
	private DateTimeFormatter formater = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
	
	public String getAddTimeFormat() {
		LocalDateTime dateTime =LocalDateTime.ofEpochSecond(this.addTime,0, ZoneOffset.ofHours(8));
		return dateTime.format(formater);
	}
	
	public String getUpdateTimeFormat() {
		LocalDateTime dateTime =LocalDateTime.ofEpochSecond(this.updateTime,0, ZoneOffset.ofHours(8));
		return dateTime.format(formater);
	}
}
