package com.system.core.shiro.cache.redis;

import java.util.Collection;
import java.util.Set;

import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class RedisCache<K, V> implements Cache<K, V> {

	private String cacheName;
	private RedisService redisService;

	@SuppressWarnings("unchecked")
	@Override
	public V get(K k) throws CacheException {
		return (V) redisService.getHashValueByKey(cacheName, k.toString());
	}

	@Override
	public V put(K k, V v) throws CacheException {
		redisService.putHash(cacheName, k.toString(), v);
		return v;
	}

	@Override
	public V remove(K k) throws CacheException {
		@SuppressWarnings("unchecked")
		V value = (V) redisService.getHashValueByKey(cacheName, k.toString());
		redisService.removeHashKey(cacheName, k.toString());
		return value;
	}

	@Override
	public void clear() throws CacheException {
		redisService.removeHash(cacheName);
	}

	@Override
	public int size() {
		return redisService.sizeHash(cacheName);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<K> keys() {
		return (Set<K>) redisService.keysHash(cacheName);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<V> values() {
		return (Collection<V>) redisService.valuesHash(cacheName);
	}

}
