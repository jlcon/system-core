package com.system.core.shiro.cache.redis;

import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.cache.CacheManager;
import org.springframework.stereotype.Component;

import jakarta.annotation.Resource;

@Component
public class RedisCacheManager implements CacheManager {

	@Resource
	private RedisCache<Object, Object> redisCache;

	@SuppressWarnings("unchecked")
	@Override
	public <K, V> Cache<K, V> getCache(String name) throws CacheException {
		redisCache.setCacheName(name);
		return (Cache<K, V>) redisCache;
	}
}
