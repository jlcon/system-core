package com.system.core.shiro.cache.redis;

import java.util.Collection;
import java.util.Set;

import org.springframework.context.annotation.Lazy;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import jakarta.annotation.Resource;

@Service
@Lazy
public class RedisService {
	
	@Resource
	private RedisTemplate<String, Object> redisTemplate;

	public void putHash(String hashName, String key, Object value) {
		redisTemplate.opsForHash().put(hashName, key, value);
	}

	public Object getHashValueByKey(String hashName, String key) {
		return redisTemplate.opsForHash().get(hashName, key);
	}

	public void removeHashKey(String hashName, String key) {
		redisTemplate.opsForHash().delete(hashName, key);
	}

	public void removeHash(String hashName) {
		redisTemplate.delete(hashName);
	}

	public int sizeHash(String hashName) {
		return redisTemplate.opsForHash().size(hashName).intValue();
	}

	public Set<Object> keysHash(String hashName) {
		return redisTemplate.opsForHash().keys(hashName);
	}

	public Collection<Object> valuesHash(String hashName) {
		return redisTemplate.opsForHash().values(hashName);
	}
}
