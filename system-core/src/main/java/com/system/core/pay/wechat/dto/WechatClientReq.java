package com.system.core.pay.wechat.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Builder
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class WechatClientReq {

	private String privateKey;
	private String serialNumber;
	private String merchantId;
	private String apiV3Key;
	private String[] merchantPublicCert;
	private boolean autoUpdate;
}
