package com.system.core.pay.wechat.dto;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.ToString;

@lombok.Data
@ToString
public class NativeRsp {
	
	@Schema(description = "code")
	@JSONField(name = "code")
	@JsonProperty("code")
	private String code;
	@Schema(description = "codeUrl")
	@JSONField(name = "code_url")
	@JsonProperty("code_url")
	private String codeUrl;
	
	@Schema(description = "detail")
	@JSONField(name = "detail")
	@JsonProperty("detail")
	private Detail detail;
	
	@Schema(description = "message")
	@JSONField(name = "message")
	@JsonProperty("message")
	private String message;
	
	
	@lombok.Data
	@ToString
	public static class Detail{
		
		@Schema(description = "location")
		@JSONField(name = "location")
		@JsonProperty("location")
		private String location;
		
		@Schema(description = "value")
		@JSONField(name = "value")
		@JsonProperty("value")
		private String value;
	}
}