package com.system.core.pay.wechat.dto;

import java.util.List;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.v3.oas.annotations.media.Schema;

@lombok.Data
public class NativeReq implements PreOrderReq{
	
	@Schema(description = "交易结束时间")
	@JSONField(name = "time_expire")
	@JsonProperty("time_expire")
	private String timeExpire;
	
	@Schema(description = "订单金额")
	@JSONField(name = "amount")
	@JsonProperty("amount")
	private CommReqAmountInfo amount;
	
	@Schema(description = "直连商户号")
	@JSONField(name = "mchid")
	@JsonProperty("mchid")
	private String mchid;
	
	@Schema(description = "结算信息")
	@JSONField(name = "settle_info")
	@JsonProperty("settle_info")
	private SettleInfo settleInfo;
	
	@Schema(description = "商品描述")
	@JSONField(name = "description")
	@JsonProperty("description")
	private String description;
	
	@Schema(description = "通知地址")
	@JSONField(name = "notify_url")
	@JsonProperty("notify_url")
	private String notifyUrl;
	
	@Schema(description = "商户订单号")
	@JSONField(name = "out_trade_no")
	@JsonProperty("out_trade_no")
	private String outTradeNo;
	
	@Schema(description = "订单优惠标记")
	@JSONField(name = "goods_tag")
	@JsonProperty("goods_tag")
	private String goodsTag;
	
	@Schema(description = "公众号ID")
	@JSONField(name = "appid")
	@JsonProperty("appid")
	private String appid;
	
	@Schema(description = "附加数据")
	@JSONField(name = "attach")
	@JsonProperty("attach")
	private String attach;
	
	@Schema(description = "优惠功能")
	@JSONField(name = "detail")
	@JsonProperty("detail")
	private OrderDetail detail;
	
	@Schema(description = "电子发票入口开放标识")
	@JSONField(name = "support_fapiao")
	@JsonProperty("support_fapiao")
	private java.lang.Boolean supportFapiao;
	
	@Schema(description = "场景信息")
	@JSONField(name = "scene_info")
	@JsonProperty("scene_info")
	private SceneInfo sceneInfo;
	
	
	@lombok.Data
	public static class CommReqAmountInfo{
		
		@Schema(description = "总金额")
		@JSONField(name = "total")
		@JsonProperty("total")
		private Integer total;
		
		@Schema(description = "货币类型")
		@JSONField(name = "currency")
		@JsonProperty("currency")
		private String currency;
	}
	
	@lombok.Data
	public static class SettleInfo{
		
		@Schema(description = "是否指定分账")
		@JSONField(name = "profit_sharing")
		@JsonProperty("profit_sharing")
		private Boolean profitSharing;
	}
	
	@lombok.Data
	public static class OrderDetail{
		
		@Schema(description = "商品小票ID")
		@JSONField(name = "invoice_id")
		@JsonProperty("invoice_id")
		private String invoiceId;
		
		@Schema(description = "单品列表")
		@JSONField(name = "goods_detail")
		@JsonProperty("goods_detail")
		private List<GoodsDetail> goodsDetail;
		
		@Schema(description = "订单原价")
		@JSONField(name = "cost_price")
		@JsonProperty("cost_price")
		private Integer costPrice;
	}
	
	@lombok.Data
	public static class GoodsDetail{
		
		@Schema(description = "商品名称")
		@JSONField(name = "goods_name")
		@JsonProperty("goods_name")
		private String goodsName;
		
		@Schema(description = "微信支付商品编码")
		@JSONField(name = "wechatpay_goods_id")
		@JsonProperty("wechatpay_goods_id")
		private String wechatpayGoodsId;
		
		@Schema(description = "商品数量")
		@JSONField(name = "quantity")
		@JsonProperty("quantity")
		private Integer quantity;
		
		@Schema(description = "商户侧商品编码")
		@JSONField(name = "merchant_goods_id")
		@JsonProperty("merchant_goods_id")
		private String merchantGoodsId;
		
		@Schema(description = "商品单价")
		@JSONField(name = "unit_price")
		@JsonProperty("unit_price")
		private Integer unitPrice;
	}
	
	@lombok.Data
	public static class SceneInfo{
		
		@Schema(description = "商户门店信息")
		@JSONField(name = "store_info")
		@JsonProperty("store_info")
		private StoreInfo storeInfo;
		
		@Schema(description = "商户端设备号（门店号或收银设备ID）")
		@JSONField(name = "device_id")
		@JsonProperty("device_id")
		private String deviceId;
		
		@Schema(description = "用户的客户端IP，支持IPv4和IPv6两种格式的IP地址")
		@JSONField(name = "payer_client_ip")
		@JsonProperty("payer_client_ip")
		private String payerClientIp;
	}
	
	@lombok.Data
	public static class StoreInfo{
		
		@Schema(description = "详细的商户门店地址")
		@JSONField(name = "address")
		@JsonProperty("address")
		private String address;
		
		@Schema(description = "<a href='https://pay.weixin.qq.com/docs/merchant/development/chart/provincial-code.html'>地区编码</a>")
		@JSONField(name = "area_code")
		@JsonProperty("area_code")
		private String areaCode;
		
		@Schema(description = "商户侧门店名称")
		@JSONField(name = "name")
		@JsonProperty("name")
		private String name;
		
		@Schema(description = "商户侧门店编号")
		@JSONField(name = "id")
		@JsonProperty("id")
		private String id;
	}
}