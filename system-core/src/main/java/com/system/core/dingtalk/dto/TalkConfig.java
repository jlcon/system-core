package com.system.core.dingtalk.dto;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@EnableConfigurationProperties
@Component
@ConfigurationProperties(prefix = "app.dingtalk")
@Data
public class TalkConfig {

	List<DingTalkDto> configs;
	
	public DingTalkDto getConfigByName(String name) {
		return configs.stream().filter(item->item.getName().equalsIgnoreCase(name)).findAny().get();
	}
}
