package com.system.core.dingtalk.dto;

import lombok.Data;

@Data
public class DingTalkDto {

	private String name;
	private String customRobotToken;
	private String secret;
}
