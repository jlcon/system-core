package com.system.core.view.common.response;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.beans.BeanUtils;

import com.system.core.results.ResultBase;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.Setter;

/**
 * @Description: 通用响应结果
 * @Author: kaxiaofei
 * @Date: 2021/5/17
 * @Version: v1.0
 */
@Setter
@Getter
@Tag(name= "通用响应结果")
public class CommonResponse extends ResultBase {

	private static final long serialVersionUID = -7522584570985683566L;
	protected final static int SUCCESS_CODE = 0;
    protected final static int FAILURE_CODE = 500;

    @Schema(description = "-1：需要登录，500失败，200成功,支持扩展其它 ")
    private int code = SUCCESS_CODE;

    @Schema(description = "时间戳 ")
    private long timestamp = System.currentTimeMillis();

    public CommonResponse() {
    }

    public CommonResponse(int code, String message) {
        this.code = code;
        super.setMessage(message);
        super.setSuccess(code == SUCCESS_CODE);
    }

    /**
     * 创建结果为成功的CommonResponse对象
     *
     * @return 结果为成功的CommonResponse对象
     */
    public static CommonResponse buildSuccessResult() {
        return buildSuccessResult("OK");
    }

    /**
     * 创建结果为成功的CommonResponse对象
     *
     * @param message
     * @return 结果为成功的CommonResponse对象
     */
    public static CommonResponse buildSuccessResult(String message) {
        return new CommonResponse(SUCCESS_CODE, message);
    }

    /**
     * 创建结果为成功的CommonResponse对象
     *
     * @param clazz
     * @return 结果为成功的CommonResponse对象
     */
    public static <T extends CommonResponse> T buildSuccessResult(Class<T> clazz) {
        return buildSuccessResult("OK", clazz);
    }

    /**
     * 创建结果为成功的CommonResponse对象
     *
     * @param message
     * @param clazz
     * @return 结果为成功的CommonResponse对象
     */
    public static <T extends CommonResponse> T buildSuccessResult(String message, Class<T> clazz) {
        T result = BeanUtils.instantiateClass(clazz);
        result.setCode(SUCCESS_CODE);
        result.setMessage(message);
        result.setSuccess(true);
        return result;
    }

    /**
     * 创建结果为失败的CommonResponse对象
     *
     * @param message
     * @return 结果为失败的CommonResponse对象
     */
    public static CommonResponse buildFailureResult(String message) {
        return buildFailureResult(message, CommonResponse.class);
    }

    /**
     * 创建结果为失败的CommonResponse对象
     *
     * @param ex
     * @return 结果为失败的CommonResponse对象
     */
    public static CommonResponse buildFailureResult(Exception ex) {
        return buildFailureResult(ex, CommonResponse.class);
    }

    /**
     * 创建结果为失败的CommonResponse对象
     *
     * @param message
     * @param clazz
     * @return 结果为失败的CommonResponse对象
     */
    public static <T extends CommonResponse> T buildFailureResult(String message, Class<T> clazz) {
        T result = BeanUtils.instantiateClass(clazz);
        result.setCode(FAILURE_CODE);
        result.setMessage(message);
        result.setSuccess(false);
        return result;
    }

    /**
     * 创建结果为失败的CommonResponse对象
     *
     * @param ex
     * @param clazz
     * @return 结果为失败的CommonResponse对象
     */
    public static <T extends CommonResponse> T buildFailureResult(Exception ex, Class<T> clazz) {
        String msg = ex.getMessage();
        if (msg == null) {
            msg = "系统繁忙，请稍候再试!";
        }
        return buildFailureResult(msg, clazz);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
