package com.system.core.view.common.response;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.Setter;

/**
 * @Description: 通用数据集合响应结果
 * @Author: kaxiaofei
 * @Date: 2021/5/17
 * @Version: v1.0
 */
@Setter
@Getter
@Tag(name = "分页数据响应结果")
public class CommonPageResponse<T> extends CommonResponse {

	private static final long serialVersionUID = 2076086177834935842L;

	@Schema(description = "符合条件的总记录数")
    private long total;
    
    private Map<String, String> totalRow = null;

    @Schema(description = "当前页数据列表")
    private List<T> data = new ArrayList<>(0);
    
    private String encryptData;

    public CommonPageResponse() {
        super(SUCCESS_CODE, "OK");
    }

    public CommonPageResponse(long total, List<T> data) {
        this();
        this.total = total;
        this.data = data;
    }

    /**
     * 创建结果为SUCCESS的CommonPageResponse对象
     *
     * @param total
     * @param data
     * @return 结果为SUCCESS的CommonPageResponse对象
     */
    public static <T> CommonPageResponse<T> buildSuccessResult(long total, List<T> data) {
        return buildSuccessResult("OK", total, data);
    }
    
    public static <T> CommonPageResponse<T> buildEncryptSuccessResult(long total, String encrypt) {
    	CommonPageResponse<T> result = CommonResponse.buildSuccessResult("OK", CommonPageResponse.class);
    	result.setTotal(total);
    	result.setEncryptData(encrypt);
    	return result;
    }

    /**
     * 创建结果为SUCCESS的CommonPageResponse对象
     *
     * @param message
     * @param total
     * @param data
     * @return 结果为SUCCESS的CommonPageResponse对象
     */
    public static <T> CommonPageResponse<T> buildSuccessResult(String message, long total, List<T> data) {
        CommonPageResponse<T> result = CommonResponse.buildSuccessResult(message, CommonPageResponse.class);
        result.setTotal(total);
        result.setData(data);
        return result;
    }

    /**
     * 创建结果为SUCCESS的CommonPageResponse对象
     *
     * @param message
     * @param total
     * @param data
     * @param clazz
     * @return 结果为SUCCESS的CommonPageResponse对象
     */
    public static <R extends CommonPageResponse<T>, T> R buildSuccessResult(String message, long total, List<T> data, Class<R> clazz) {
        R result = CommonResponse.buildSuccessResult(message, clazz);
        result.setTotal(total);
        result.setData(data);
        return result;
    }

    /**
     * 创建结果为SUCCESS的CommonPageResponse对象
     *
     * @param total
     * @param data
     * @param clazz
     * @return 结果为SUCCESS的CommonPageResponse对象
     */
    public static <R extends CommonPageResponse<T>, T> R buildSuccessResult(long total, List<T> data, Class<R> clazz) {
        return buildSuccessResult("OK", total, data, clazz);
    }
    
    public static <T> CommonPageResponse<T> buildPageFailureResult(Exception message) {
    	CommonPageResponse<T> result = buildSuccessResult(0,null);
    	result.setSuccess(false);
    	result.setMessage(message.getMessage());
    	return result;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
