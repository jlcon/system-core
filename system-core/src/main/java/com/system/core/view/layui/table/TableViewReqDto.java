package com.system.core.view.layui.table;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class TableViewReqDto {

    @Schema(description = "请求页码")
    private Integer page = 1;
    @Schema(description = "每页分页长度")
    private Integer limit = 20;
    @Schema(description = "排序字段")
    private String orderField;
    @Schema(description = "排序类型，升序(ASC)或者降序(DESC)")
    private String orderType;

}
