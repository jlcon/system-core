package com.system.core.view.layui.table;

import java.util.List;

import com.github.pagehelper.Page;

import lombok.experimental.UtilityClass;

@UtilityClass
public class TableViewUtil {

	public <T> TableViewRspDto<T> listCoverter(List<T> list,Page<T> page){
		
		TableViewRspDto<T> result = new TableViewRspDto<T>();
		result.setCount((long)page.getPageNum());
		result.setData(list);
		result.setCode(0);
		result.setSuccess(true);
		
		return result;
	}
}
