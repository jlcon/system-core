package com.system.core.view.common.response;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.Setter;

/**
 * @Description: 通用数据响应结果
 * @Author: kaxiaofei
 * @Date: 2021/5/17
 * @Version: v1.0
 */
@Setter
@Getter
@Tag(name="数据响应结果")
public class CommonDataResponse<T> extends CommonResponse {

	private static final long serialVersionUID = -9131700040458706671L;
	
	@Schema(description = "响应结果数据Object类型")
    private T data;

    public CommonDataResponse() {
        super(SUCCESS_CODE, "OK");
    }

    public CommonDataResponse(T data) {
        this();
        this.data = data;
    }

    /**
     * 创建结果为SUCCESS的CommonDataResponse对象
     *
     * @param data
     * @return 结果为SUCCESS的CommonDataResponse对象
     */
    public static <T> CommonDataResponse<T> buildSuccessResult(T data) {
        return buildSuccessResult("OK", data);
    }

    /**
     * 创建结果为SUCCESS的CommonDataResponse对象
     *
     * @param message
     * @param data
     * @return 结果为SUCCESS的CommonDataResponse对象
     */
    public static <T> CommonDataResponse<T> buildSuccessResult(String message, T data) {
        CommonDataResponse<T> result = CommonResponse.buildSuccessResult(message, CommonDataResponse.class);
        result.setData(data);
        return result;
    }

    /**
     * 创建结果为SUCCESS的CommonDataResponse对象
     *
     * @param data
     * @param clazz
     * @return 结果为SUCCESS的CommonDataResponse对象
     */
    public static <R extends CommonDataResponse<T>, T> R buildSuccessResult(T data, Class<R> clazz) {
        return buildSuccessResult("OK", data, clazz);
    }

    /**
     * 创建结果为SUCCESS的CommonDataResponse对象
     *
     * @param message
     * @param data
     * @param clazz
     * @return 结果为SUCCESS的CommonDataResponse对象
     */
    public static <R extends CommonDataResponse<T>, T> R buildSuccessResult(String message, T data, Class<R> clazz) {
        R result = CommonResponse.buildSuccessResult(message, clazz);
        result.setData(data);
        return result;
    }
    
    public static <T> CommonDataResponse<T> buildFailureResult(String message, T data) {
    	CommonDataResponse<T> result = CommonResponse.buildFailureResult(message, CommonDataResponse.class);
    	result.setData(null);
    	return result;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
    
}
