package com.system.core.view.layui.tree;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class TreeNode {
	
	private int id;
	private int pid;
	private String title;
	private String field;
	private String href;
	private boolean checked;
	private boolean spread;
	private boolean disabled;
	private List<TreeNode> children = new ArrayList<TreeNode>();

}
