package com.system.core.view.layui.table;

import java.util.List;
import java.util.Map;

import com.system.core.results.ResultBase;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class TableViewRspDto<T> extends ResultBase{

	//成功的状态码，默认：0
	@Schema(description = "响应编码")
	private Integer code = -1;
	//数据总数
	
	@Schema(description = "查询到数据总数")
	private Long count;
	//数据列表
	@Schema(description = "数据列表")
	private List<T> data;
	
	private Map<String, String> totalRow = null;
}
