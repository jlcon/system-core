package com.system.core.file.dto;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@EnableConfigurationProperties
@Component
@ConfigurationProperties(prefix = "app.oss")
@Data
public class OSSConfig {
	
	String endpoint="oss-cn-chengdu.aliyuncs.com";
	String accessKeyId="unknown";
	String accessKeySecret="unknown";
	
}
