package com.system.core.file.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class FileManageResultKindEditor {
	
	@JsonProperty(value = "moveup_dir_path")
	private String moveupDirPath;
	@JsonProperty(value = "current_dir_path")
	private String currentDirPath;
	@JsonProperty(value = "current_url")
	private String currentUrl;
	@JsonProperty(value = "total_count")
	private int totalCount;
	@JsonProperty(value = "file_list")
	List<FileKindEditor> fileList = new ArrayList<FileKindEditor>();
}
