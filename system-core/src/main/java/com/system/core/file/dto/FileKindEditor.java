package com.system.core.file.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class FileKindEditor {

	@JsonProperty(value = "is_dir")
	private boolean isDir;
	@JsonProperty(value = "has_file")
	private boolean hasFile;
	@JsonProperty(value = "filesize")
	private long filesize;
	@JsonProperty(value = "is_photo")
	private boolean isPhoto;
	private String filetype;
	private String filename;
	private String datetime;
	
}
