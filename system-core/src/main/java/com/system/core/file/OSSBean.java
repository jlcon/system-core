package com.system.core.file;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.aliyun.oss.ClientConfiguration;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.common.auth.DefaultCredentialProvider;
import com.system.core.file.dto.OSSConfig;

import lombok.RequiredArgsConstructor;

@Configuration
@RequiredArgsConstructor
public class OSSBean {

	private final OSSConfig ossConfig;
	
	@Bean
	public OSSClient ossClient() {
		DefaultCredentialProvider defaultCredentialProvider = new DefaultCredentialProvider(ossConfig.getAccessKeyId(), ossConfig.getAccessKeySecret());
		ClientConfiguration clientConfiguration = new ClientConfiguration();
		OSSClient ossClient = new OSSClient(ossConfig.getEndpoint(),defaultCredentialProvider,clientConfiguration);
		return ossClient;
	}
}
