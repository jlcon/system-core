package com.system.core.wechat.dto;

import lombok.Data;

@Data
public class WeChatUser {

	private String openid;
	private String nickname;
	private Integer sex;
	private String province;
	private String city;
	private String country;
	private String headimgurl;
	private String[] privilege;
	private String unionid;
	
	private Integer errcode;
	private String errmsg;
}
