package com.system.core.wechat.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Code2SessionRsp {

	private String openid;
//	private String sessionKey;
	private String unionid;
	private int errcode;
	private String errmsg;
}
