package com.system.core.wechat.dto;

import lombok.Data;

@Data
public class UserPhoneNumberReq {

	private String accessToken;
	
	private String code;
}
