package com.system.core.wechat.dto;

import lombok.Data;

@Data
public class JsapiReq {

	private String noncestr;
	private String jsapiTicket;
	private long timestamp;
	private String url;
}
