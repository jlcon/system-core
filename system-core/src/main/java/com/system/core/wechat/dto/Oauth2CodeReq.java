package com.system.core.wechat.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Oauth2CodeReq {

	private String appid;
	private String redirectUri;
	@Builder.Default
	private String scope = "snsapi_login";
	/**
	 * 用于保持请求和回调的状态，授权请求后原样带回给第三方。
	 * 该参数可用于防止csrf攻击（跨站请求伪造攻击），建议第三方带上该参数，
	 * 可设置为简单的随机数加session进行校验
	 */
	private String state;
}
