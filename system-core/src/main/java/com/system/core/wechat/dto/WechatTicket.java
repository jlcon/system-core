package com.system.core.wechat.dto;

import com.alibaba.fastjson.annotation.JSONField;

import lombok.Data;

@Data
public class WechatTicket{
	private int errcode;
	private String errmsg;
	private String ticket;
	@JSONField(name = "expires_in")
	private int expiresIn;
}
