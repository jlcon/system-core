package com.system.core.wechat.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class JsSDKSignRsp {

	private Long timestamp;
	private String nonceStr;
	private String signature;
	private String jsapiTicket;
	private String url;
}
