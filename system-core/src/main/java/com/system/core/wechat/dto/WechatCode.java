package com.system.core.wechat.dto;

import com.alibaba.fastjson.annotation.JSONField;

import lombok.Data;

@Data
public class WechatCode{
	@JSONField(name = "access_token")
	private String accessToken;
	@JSONField(name = "expires_in")
	private int expiresIn;
	private String errcode;
	private String errmsg;
}
