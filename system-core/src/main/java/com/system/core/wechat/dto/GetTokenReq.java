package com.system.core.wechat.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GetTokenReq {

	private String appid;
	private String secret;
}
