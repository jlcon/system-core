package com.system.core.wechat.example;

import org.junit.Test;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.system.core.wechat.SecurityUtil;
import com.system.core.wechat.dto.WechatTicket;

public class SecurityUtilExample {
	
	@Test
	public void getJsApiTicket() {
		WechatTicket ticket = SecurityUtil.getJsApiTicket("36_hoSF8A0_ahx_ruc3z9CdPPgGDRr7cbWcVa08kBB-nV_6DsM3LEiGSFf8rj1pussCxcZhKLz6kpik42mhfBX1Y8rkIwRoa8i0JXri3kzFLkpSSWwFeX-dr1EoiVsIdlpIjHV_zu5GGQyfMqDWQTKgAIATWN");
		System.out.println(ticket.toString());
	}
	
	
	@Test
	public void prettyJson() {
		String src = "{\"access_token\":\"36_cy6nn2-R8yDrWYSBNS_tSZylLzmjX6XPnXNLkvi3q3wUOJPvY4ugzXra8YfNqfTpyEO8rrGwA33KJFuHNQUmHHEmBND_-837C5UBAmkpajLvDN5QfiikUvyA18RSIwxbBH6DCCjDQzw6iOAKPXLfADAJRF\",\"expires_in\":7200}";
		System.out.println(JSONObject.parseObject(src).toString(SerializerFeature.PrettyFormat));
	}

}
