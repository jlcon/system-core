package com.system.core.sms.dto.smstemplate;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class SmsCodeBizReq {

	private String code;
	private String bizName;
	
	@Override
	public String toString() {
		return "{\"code\":\"" + code + "\", \"bizName\":\"" + bizName + "\"}";
	}
	
}
