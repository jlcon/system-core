package com.system.core.sms.dto.sendcode;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum SendCodeBizTypeEnum {

	LOGINSMSCODE(1, "登录"),
	FINDPASSWORD(2, "找回密码"),
	BINDPHONE(3, "绑定手机"),
	CHANGEPHONE(4, "修改手机"),
	CHANGEPASSWD(5, "修改密码");

	private Integer code;
	private String message;

	public static SendCodeBizTypeEnum getEnumByMessage(String message) {
		if (message == null)
			return null;
		for (SendCodeBizTypeEnum _enum : values()) {
			if (_enum.getMessage().equalsIgnoreCase(message)) {
				return _enum;
			}
		}
		return null;
	}
	
	public static SendCodeBizTypeEnum getEnumByCode(Integer code) {
		if (code == null)
			return null;
		for (SendCodeBizTypeEnum _enum : values()) {
			if (_enum.getCode()==code) {
				return _enum;
			}
		}
		return null;
	}

	/**
	 * Get all enums
	 * 
	 * @return List<SendCodeBizTypeEnum>
	 */
	public static List<SendCodeBizTypeEnum> getAllEnum() {
		List<SendCodeBizTypeEnum> list = new java.util.ArrayList<SendCodeBizTypeEnum>(values().length);
		for (SendCodeBizTypeEnum _enum : values()) {
			list.add(_enum);
		}
		return list;
	}

	/**
	 * Get all enums code
	 * 
	 * @return List<String>
	 */
	public static List<Integer> getAllEnumCode() {
		List<Integer> list = new java.util.ArrayList<Integer>(values().length);
		for (SendCodeBizTypeEnum _enum : values()) {
			list.add(_enum.getCode());
		}
		return list;
	}
}