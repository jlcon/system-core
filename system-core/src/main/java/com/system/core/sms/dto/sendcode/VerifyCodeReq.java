package com.system.core.sms.dto.sendcode;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class VerifyCodeReq {

	@NotBlank(message = "phone必传")
	private String phone;
	@NotBlank(message = "smsCode必传")
	private String smsCode;
	@NotNull(message = "bizTypeName必传")
	private SendCodeBizTypeEnum bizTypeName;
}
