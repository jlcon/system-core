package com.system.core.sms.dto.sendcode;

import java.util.Objects;

import lombok.Data;

@Data
public class UserCode {

	private SendCodeBizTypeEnum bizTypeName;
	private Integer sendTimes = 1;
	private String smsCode;
	private boolean used = false;
	private Long lastUpdateTimeMillis;
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserCode other = (UserCode) obj;
		return bizTypeName == other.bizTypeName;
	}
	@Override
	public int hashCode() {
		return Objects.hash(bizTypeName);
	}
	
}
