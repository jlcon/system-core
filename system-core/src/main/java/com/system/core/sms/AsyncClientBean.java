package com.system.core.sms;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.aliyun.auth.credentials.Credential;
import com.aliyun.auth.credentials.provider.StaticCredentialProvider;
import com.aliyun.sdk.service.dysmsapi20170525.AsyncClient;
import com.system.core.sms.dto.config.AliyunSmsConfig;

import darabonba.core.client.ClientOverrideConfiguration;
import lombok.AllArgsConstructor;

@Configuration
@AllArgsConstructor
public class AsyncClientBean {

	private final AliyunSmsConfig aliyunSmsConfig;

    @Bean
    AsyncClient asyncClient() {
		StaticCredentialProvider provider = StaticCredentialProvider.create(Credential.builder()
				.accessKeyId(aliyunSmsConfig.getAccessKeyId())
				.accessKeySecret(aliyunSmsConfig.getAccessKeySecret())
				.build());
		AsyncClient client = AsyncClient.builder().region(aliyunSmsConfig.getRegion())
				.credentialsProvider(provider).overrideConfiguration(
						ClientOverrideConfiguration.create().setEndpointOverride("dysmsapi.aliyuncs.com")
				)
				.build();
		return client;
	}
}
