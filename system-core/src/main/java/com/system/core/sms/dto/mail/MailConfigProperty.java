package com.system.core.sms.dto.mail;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@EnableConfigurationProperties
@Component
@ConfigurationProperties(prefix = "app.mail")
@Data
public class MailConfigProperty {

	private String host;
	private String port;
	private String auth;
	private String tls;
	private String userName;
	private String passwd;
}
