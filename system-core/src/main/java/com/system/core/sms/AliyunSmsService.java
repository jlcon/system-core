package com.system.core.sms;

import java.util.concurrent.CompletableFuture;

import org.springframework.stereotype.Component;

import com.aliyun.sdk.service.dysmsapi20170525.AsyncClient;
import com.aliyun.sdk.service.dysmsapi20170525.models.SendSmsRequest;
import com.aliyun.sdk.service.dysmsapi20170525.models.SendSmsResponse;
import com.aliyun.sdk.service.dysmsapi20170525.models.SendSmsResponseBody;
import com.system.core.sms.dto.config.AliyunSmsConfig;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class AliyunSmsService {
	
	private final AsyncClient client;
	private final AliyunSmsConfig aliyunSmsConfig;
	
	/*
	 * 例子：
	 * SmsCodeReq smsCodeReq = SmsCodeReq.builder().code(smsCodeComponent.getWord()).build(); SendSmsRequest
	 * sendSmsRequest = SendSmsRequest.builder()
	 * .signName("探程")
	 * .templateCode("SMS_278035060")
	 * .phoneNumbers("15923325427")
	 * .templateParam(smsCodeReq.toString())
	 * .outId(idGeneratorService.generatorId("TC"))
	 * .build();
	 * SendSmsResponseBody result = aliyunSmsService.send(sendSmsRequest);
	 */
	public SendSmsResponseBody send(SendSmsRequest sendSmsRequest) throws Exception {
		SendSmsResponseBody body;
		if(aliyunSmsConfig.isRealSend()) {
			CompletableFuture<SendSmsResponse> response = client.sendSms(sendSmsRequest);
			SendSmsResponse sendSmsResponse = response.get();
			body = sendSmsResponse.getBody();
		} else {
			body = SendSmsResponseBody.builder()
					.code("OK")
					.message("OK")
					.bizId("434411790533902189^0")
					.requestId("13B1C1CE-BE5C-50C8-A9B1-18BC46F816BE")
					.build();
		}
		return body;
	}
}
