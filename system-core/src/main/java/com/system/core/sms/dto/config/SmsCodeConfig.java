package com.system.core.sms.dto.config;

import java.time.Duration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@EnableConfigurationProperties
@Component
@ConfigurationProperties(prefix = "app.sms.code")
@Data
public class SmsCodeConfig {

	private String defaultCode = "888888";
	private Integer codeLength = 6;
	private Boolean useDefaultCode = false;
	private Integer timesPeerDay = 1;
	private String signName;
	private String smsCodeTemplateName;
	private Duration expire = Duration.ofSeconds(10);
}
