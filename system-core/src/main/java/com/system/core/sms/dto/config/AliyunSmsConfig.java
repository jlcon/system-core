package com.system.core.sms.dto.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@EnableConfigurationProperties
@Component
@ConfigurationProperties(prefix = "app.sms.aliyun")
@Data
public class AliyunSmsConfig {
	
	private String accessKeyId;
	private String accessKeySecret;
	private boolean realSend = false;
	private String region = "cn-chengdu";
}
