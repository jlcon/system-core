package com.system.core.sms.dto.smstemplate;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class SmsCodeReq {

	private String code;

	@Override
	public String toString() {
		return "{\"code\":\"" + code + "\"}";
	}

}
