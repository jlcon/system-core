package com.system.core.sms.dto.mail;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MailSendReq {

	private String[] receiverAddresses;
	private String subject;
	private String content;
}
