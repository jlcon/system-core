package com.system.core.tiandimap.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Builder
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class GeocoderReq {

    private Double lon;
    private Double lat;
    private Integer ver;
}