package com.system.core.tiandimap.dto;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.v3.oas.annotations.media.Schema;

@lombok.Data
public class GeocoderRsp {
	
	@Schema(description = "formattedAddress")
	@JSONField(name = "formatted_address")
	@JsonProperty("formatted_address")
	private String formattedAddress;
	
	@Schema(description = "location")
	@JSONField(name = "location")
	@JsonProperty("location")
	private Location location;
	
	@Schema(description = "addressComponent")
	@JSONField(name = "addressComponent")
	@JsonProperty("addressComponent")
	private AddressComponent addressComponent;
	
	
	@lombok.Data
	public static class Location{
		
		@Schema(description = "lon")
		@JSONField(name = "lon")
		@JsonProperty("lon")
		private java.math.BigDecimal lon;
		
		@Schema(description = "lat")
		@JSONField(name = "lat")
		@JsonProperty("lat")
		private java.math.BigDecimal lat;
	}
	
	@lombok.Data
	public static class AddressComponent{
		
		@Schema(description = "address")
		@JSONField(name = "address")
		@JsonProperty("address")
		private String address;
		
		@Schema(description = "town")
		@JSONField(name = "town")
		@JsonProperty("town")
		private String town;
		
		@Schema(description = "nation")
		@JSONField(name = "nation")
		@JsonProperty("nation")
		private String nation;
		
		@Schema(description = "city")
		@JSONField(name = "city")
		@JsonProperty("city")
		private String city;
		
		@Schema(description = "countyCode")
		@JSONField(name = "county_code")
		@JsonProperty("county_code")
		private String countyCode;
		
		@Schema(description = "poiPosition")
		@JSONField(name = "poi_position")
		@JsonProperty("poi_position")
		private String poiPosition;
		
		@Schema(description = "county")
		@JSONField(name = "county")
		@JsonProperty("county")
		private String county;
		
		@Schema(description = "cityCode")
		@JSONField(name = "city_code")
		@JsonProperty("city_code")
		private String cityCode;
		
		@Schema(description = "addressPosition")
		@JSONField(name = "address_position")
		@JsonProperty("address_position")
		private String addressPosition;
		
		@Schema(description = "poi")
		@JSONField(name = "poi")
		@JsonProperty("poi")
		private String poi;
		
		@Schema(description = "provinceCode")
		@JSONField(name = "province_code")
		@JsonProperty("province_code")
		private String provinceCode;
		
		@Schema(description = "townCode")
		@JSONField(name = "town_code")
		@JsonProperty("town_code")
		private String townCode;
		
		@Schema(description = "province")
		@JSONField(name = "province")
		@JsonProperty("province")
		private String province;
		
		@Schema(description = "road")
		@JSONField(name = "road")
		@JsonProperty("road")
		private String road;
		
		@Schema(description = "roadDistance")
		@JSONField(name = "road_distance")
		@JsonProperty("road_distance")
		private Integer roadDistance;
		
		@Schema(description = "addressDistance")
		@JSONField(name = "address_distance")
		@JsonProperty("address_distance")
		private Integer addressDistance;
		
		@Schema(description = "poiDistance")
		@JSONField(name = "poi_distance")
		@JsonProperty("poi_distance")
		private Integer poiDistance;
	}
}