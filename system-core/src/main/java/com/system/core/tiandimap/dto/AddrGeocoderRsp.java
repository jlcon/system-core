package com.system.core.tiandimap.dto;

import lombok.Data;

@Data
public class AddrGeocoderRsp {
	
	private String msg;
    private Location location;
    private String searchVersion;
    private String status;

    @Data
    public static class Location {
        private int score;
        private String level;
        private String lon;
        private String lat;
        private String keyWord;
    }
}
