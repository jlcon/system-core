package com.system.core.tiandimap.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Builder
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class TiandiBaseRsp<T> {

    private String msg;
    private String status;
    private T result;
}