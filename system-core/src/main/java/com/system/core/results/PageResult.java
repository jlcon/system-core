package com.system.core.results;

import java.util.List;

import com.system.core.orders.PageOrder;

/**
 * 页面对象，例如：
 * <p><pre class="code">
 * public PageResult&lt;MovieBase&gt; movielist(PageOrder&lt;MovieBase&gt; filter){
 * 
    PageResult&lt;MovieBase&gt; pageResult = new PageResult&lt;MovieBase&gt;(filter);
    Page&lt;MovieBase&gt; page = PageHelper.startPage(filter.getPageNo(), filter.getPageSize());
    
    pageResult.setData(movieBaseMapper.selectByExample(example));
    pageResult.setCount(page.getTotal());
    pageResult.setPageSize(page.getPageSize());
    pageResult.setSuccess(true);
    return pageResult;
}
 * </pre>
 * 
 * @author jlcon
 *
 * @param <T>
 */
public class PageResult<T> extends ResultBase{

	private int pageSize;
	private long count;
	private List<T> data;
	private String msg;
	private int code = 0;
	private PageOrder<?> pageOrder;
	
	public PageResult() {
		super();
	}
	
	public PageResult(PageOrder<?> pageOrder) {
		this.pageOrder = pageOrder;
	}
	
	public int getPageSize() {
		return pageSize == 0?50:pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getPageNo() {
		return pageOrder.getPageNo();
	}
	
	public List<T> getData() {
		return data;
	}
	public void setData(List<T> data) {
		this.data = data;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	@Override
	public String toString() {
		return "{pageSize:\"" + pageSize + "\", count:\"" + count + "\", currentPageNo:\"" + getPageNo() + "\", data:\"" + data + "\", msg:\"" + msg + "\", code:\"" + code + "}";
	}
	public void setPageOrder(PageOrder<?> pageOrder) {
		this.pageOrder = pageOrder;
	}
	public PageOrder<?> getPageOrder() {
		return pageOrder;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}
	
	
}
