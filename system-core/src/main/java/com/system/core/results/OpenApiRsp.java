package com.system.core.results;

import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class OpenApiRsp<T> extends ResultBase implements Serializable {

	private static final long serialVersionUID = 1183338168805421488L;
	private String code;
	private T data;
	
}
