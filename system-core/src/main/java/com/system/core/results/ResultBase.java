package com.system.core.results;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class ResultBase implements Serializable{

	private static final long serialVersionUID = 9122445885876282390L;
	@Schema(description = "true-成功，false-失败")
	private boolean success = false;
	@Schema(description = "返回消息")
	private String message = StringUtils.EMPTY;
	@Schema(description = "请求地址")
	private String requestPath;
	
	public ResultBase() {
		super();
	}
	
	public ResultBase(String message) {
		super();
		this.message = message;
	}
	
	public ResultBase(boolean success, String message) {
		this.success = success;
		this.message = message;
	}
	
	public ResultBase(boolean success, String message, String requestPath) {
		super();
		this.success = success;
		this.message = message;
		this.requestPath = requestPath;
	}

	@Override
	public String toString() {
		return "ResultBase [success=" + success + ", message=" + message + ", requestPath=" + requestPath + "]";
	}
}
