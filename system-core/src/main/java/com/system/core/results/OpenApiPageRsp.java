package com.system.core.results;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class OpenApiPageRsp<T> extends ResultBase {

	private String code;
	private Long count;
	private List<T> data;
}
