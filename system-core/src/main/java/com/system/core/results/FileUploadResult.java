package com.system.core.results;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FileUploadResult extends ResultBase {

	private static final long serialVersionUID = -8660457985406607405L;
	
	private String uploadSavePath;
	private String url;
	private String uri;
	private String thumbUri;
	private String thumbUrl;
	private int error;
	// 单位字节
	private Long fileSize;
	private String originalFilename;
	private String fileExt;
	private Long takeTime;
}
