package com.system.core.results;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class GenericBase extends ResultBase{

	private static final long serialVersionUID = -5042004450287539343L;
	private String code;

}
