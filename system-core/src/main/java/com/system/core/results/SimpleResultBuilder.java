package com.system.core.results;

import lombok.experimental.UtilityClass;

@UtilityClass
public class SimpleResultBuilder {

	public ResultBase success() {
		ResultBase result = new ResultBase();
		result.setSuccess(true);
		result.setMessage("成功");
		return result;
	}
	
	public ResultBase success(String msg) {
		ResultBase result = new ResultBase();
		result.setSuccess(true);
		result.setMessage(msg);
		return result;
	}
	
	public ResultBase fail() {
		ResultBase result = new ResultBase();
		result.setMessage("失败");
		return result;
	}
	
	public ResultBase fail(String msg) {
		ResultBase result = new ResultBase();
		result.setMessage(msg);
		return result;
	}
	
}
