package com.system.core.interceptor.jwt;

import java.time.Duration;

import com.system.core.security.jwt.dto.UserJWT;

/**
 * @Description: jwt 存储策略
 * @Author: kaxiaofei
 * @Date: 2021/5/24
 * @Version: v1.0
 */
public interface JwtTokenStoreStrategy<T extends UserJWT> {

    /**
     * 存储Token
     * @param user
     * @param token
     * @param expireDay
     */
    void store(T user, String token, Duration expireDay);

    /**
     * 存储Token
     * @param tokenType
     * @param user
     * @param token
     * @param expireDay
     */
    void store(T user, String token, Duration expireDay, int tokenType);

    /**
     * 获取Token
     *
     * @param user
     * @return
     */
    String get(T user);


    /**
     * 获取Token
     *
     * @param user
     * @param tokenType
     * @return
     */
    String get(T user,int tokenType);

    /**
     * 获取对象
     * @return
     */
    Class<T> getUserJwtClass();
}
