package com.system.core.interceptor.jwt;

import java.time.Duration;

import com.system.core.security.jwt.dto.UserJWT;

/**
 * @Description: 抽象的TokenStore类
 * @Author: kaxiaofei
 * @Date: 2021/5/24
 * @Version: v1.0
 */
public abstract class AbstractJwtTokenStoreStrategy<T extends UserJWT> implements JwtTokenStoreStrategy<T> {

    @Override
    public void store(T user, String token, Duration expireDay) {
        this.store(user, token, expireDay, 0);
    }

    @Override
    public String get(T user) {
        return this.get(user,0);
    }
}
