package com.system.core.interceptor.jwt;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Description: jwt 配置参数
 * @Author: kaxiaofei
 * @Date: 2021/5/24
 * @Version: v1.0
 */

@EnableConfigurationProperties
@Component
@ConfigurationProperties(prefix = "jwt")
@Data
public class JwtConfigProperties {

    /**
     * 加密密钥
     */
    private String secret;

    /**
     * token有效天数
     */
    private long expireday;

    /**
     * header 名称
     */
    private String header;

    /**
     * userId UID
     */
    private String userId;
}
