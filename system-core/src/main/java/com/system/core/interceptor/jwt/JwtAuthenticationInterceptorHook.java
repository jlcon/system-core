package com.system.core.interceptor.jwt;

import com.system.core.security.jwt.annotation.JwtTokenNeed;
import com.system.core.security.jwt.dto.UserJWT;

import jakarta.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;

/**
 * @Description: 认证授权的勾子函数
 * @Author: kaxiaofei
 * @Date: 2021/5/24
 * @Version: v1.0
 */
public interface JwtAuthenticationInterceptorHook {

    /**
     * 调用勾子
     *
     * @param jwtTokenNeed
     * @param userInfo
     * @throws Exception
     */
    void invoke(JwtTokenNeed jwtTokenNeed, UserJWT userInfo, Method method, HttpServletRequest request) throws Exception;
}
