package com.system.core.interceptor.jwt;

import java.time.Duration;

import org.springframework.stereotype.Component;

import com.system.core.security.jwt.dto.UserJWT;

import lombok.extern.slf4j.Slf4j;

/**
 * @Description: 默认的空存储实现
 * @Author: kaxiaofei
 * @Date: 2021/5/24
 * @Version: v1.0
 */
@Slf4j
@Component
public class DefaultJwtTokenStoreStrategyImpl extends AbstractJwtTokenStoreStrategy<UserJWT> {

    @Override
    public void store(UserJWT user, String token, Duration expireDay, int tokenType) {
        log.warn("缺少jwt对象保存方法..");
    }

    @Override
    public String get(UserJWT user, int tokenType) {
        log.warn("缺少jwt对象获取方法..");
        return null;
    }

    @Override
    public Class<UserJWT> getUserJwtClass() {
        return UserJWT.class;
    }
}

