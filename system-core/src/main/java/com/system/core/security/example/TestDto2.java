package com.system.core.security.example;

import com.system.core.security.annotations.SignAllField;

import lombok.Data;

@Data
@SignAllField
public class TestDto2 {

	private int processor;
	private String vendorId;
	private String cpuFamily;
	private String model;
	private String modelName;
	private String flags;
	private float cpuMHz;
	private String cacheSize;
	private String fpu;
	private String addressSizes;
}
