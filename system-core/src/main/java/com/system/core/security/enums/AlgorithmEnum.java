package com.system.core.security.enums;

public enum AlgorithmEnum {

	RSA("RSA", "RSA"),
	NONEwithRSA("NONEwithRSA", "NONEwithRSA"),
	MD2withRSA("MD2withRSA", "MD2withRSA"),
	MD5withRSA("MD5withRSA", "MD5withRSA"),
	SHA256withRSA("SHA256withRSA", "SHA256withRSA"),
	SHA1withRSA("SHA1withRSA", "SHA1withRSA"),
	SHA256("SHA-256", "SHA-256"),
	SHA512("SHA-512", "SHA-512"),
	MD5("MD5", "MD5");

	/**
	 * 获取全部枚举
	 * 
	 * @return List<AlgorithmEnum>
	 */
	public static java.util.List<AlgorithmEnum> getAllEnum() {
		java.util.List<AlgorithmEnum> list = new java.util.ArrayList<AlgorithmEnum>(values().length);
		for (AlgorithmEnum _enum : values()) {
			list.add(_enum);
		}
		return list;
	}
	/**
	 * 获取全部枚举值
	 * 
	 * @return List<String>
	 */
	public static java.util.List<String> getAllEnumCode() {
		java.util.List<String> list = new java.util.ArrayList<String>(values().length);
		for (AlgorithmEnum _enum : values()) {
			list.add(_enum.getCode());
		}
		return list;
	}

	private String code;

	private String message;

	private AlgorithmEnum(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
