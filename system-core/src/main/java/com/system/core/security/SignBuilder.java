package com.system.core.security;

import com.system.core.security.SignUtil.SignParam;

public interface SignBuilder {

	/**
	 * 进行签名操作
	 */
	public String buildSign(String source);
	
	/**
	 * 准备签名字符串
	 */
	public String buildSignSrouce(SignParam signParam);
}
