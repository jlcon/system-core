package com.system.core.security.example;

import com.system.core.security.annotations.SignAllField;

import lombok.Data;

@Data
@SignAllField
public class WechatDto {

	private String appid;
	private String mchId;
	private String deviceInfo;
	private String body;
	private String nonceStr;
}
