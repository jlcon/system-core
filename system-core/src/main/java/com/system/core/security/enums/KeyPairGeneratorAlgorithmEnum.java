package com.system.core.security.enums;

public enum KeyPairGeneratorAlgorithmEnum {

	EC("EC", "EC"),
	DiffieHellman("DiffieHellman", "DiffieHellman"),
	ECGOST3410("ECGOST3410", "DiffieHellman"),
	ECDHC("ECDHC", "ECDHC"),
	ECMQV("ECMQV", "ECMQV"),
	ECIES("ECIES", "ECIES"),
	ECDSA("ECDSA", "ECDSA"),
	ECDH("ECDH", "ECDH"),
	DH("DH", "DH"),
	ELGAMAL("ELGAMAL", "ELGAMAL"),
	GOST3410("GOST3410", "GOST3410"),
	RSA("RSA", "RSA"),
	DSA("DSA", "DSA");

	/**
	 * 获取全部枚举
	 * 
	 * @return List<KeyPairGeneratorAlgorithmEnum>
	 */
	public static java.util.List<KeyPairGeneratorAlgorithmEnum> getAllEnum() {
		java.util.List<KeyPairGeneratorAlgorithmEnum> list = new java.util.ArrayList<KeyPairGeneratorAlgorithmEnum>(
				values().length);
		for (KeyPairGeneratorAlgorithmEnum _enum : values()) {
			list.add(_enum);
		}
		return list;
	}
	/**
	 * 获取全部枚举值
	 * 
	 * @return List<String>
	 */
	public static java.util.List<String> getAllEnumCode() {
		java.util.List<String> list = new java.util.ArrayList<String>(values().length);
		for (KeyPairGeneratorAlgorithmEnum _enum : values()) {
			list.add(_enum.getCode());
		}
		return list;
	}

	public static KeyPairGeneratorAlgorithmEnum getEnumByMessage(String message) {
		if (message == null) {
			return null;
		}
		for (KeyPairGeneratorAlgorithmEnum _enum : values()) {
			if (_enum.getMessage().equalsIgnoreCase(message)) {
				return _enum;
			}
		}
		return null;
	}

	private String code;

	private String message;

	private KeyPairGeneratorAlgorithmEnum(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
