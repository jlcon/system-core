package com.system.core.security;

import java.lang.reflect.Field;
import java.util.TreeMap;

import org.apache.commons.beanutils.PropertyUtils;

import com.system.core.security.annotations.SignAllField;
import com.system.core.security.annotations.SignField;

import lombok.Data;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

@UtilityClass
@Slf4j
public class SignUtil {

	public String getSign(Object srcObject,SignBuilder builder) {
		
		TreeMap<String,Object> tempTreeMap = new TreeMap<String,Object>();
		
		for (Field field : srcObject.getClass().getDeclaredFields()) {
			String fieldString = field.getName();
			if (fieldString.equalsIgnoreCase("class")) {
				continue;
			}
			try {
				if(
					field.isAnnotationPresent(SignField.class)
						||srcObject.getClass().isAnnotationPresent(SignAllField.class))
				{
					tempTreeMap.put(fieldString, PropertyUtils.getProperty(srcObject, fieldString));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		SignParam signParam = new SignParam();
		signParam.setDataMap(tempTreeMap);
		String signSourceString = builder.buildSignSrouce(signParam);
		log.debug("待签名字符串是：{}",signSourceString);
		
		return builder.buildSign(signSourceString);
	}
	
	@Data
	public static class SignParam{
		
		private TreeMap<String,Object> dataMap;
		
	}
}
