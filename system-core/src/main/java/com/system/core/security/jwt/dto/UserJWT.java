package com.system.core.security.jwt.dto;

import java.io.Serializable;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class UserJWT implements Serializable {
	
	private static final long serialVersionUID = 2179762598654553321L;

	public static final String USERINFOKEY = "userInfo";
	
	private String subject;

	private String userId;
	private String userName;
	private String nick;
	private String deviceId;
	private String wechatOpenID;
}
