package com.system.core.security.enums;

public enum MessageDigestAlgorithmEnum {

	RIPEMD320("RIPEMD320","RIPEMD320"),
	SHA1("SHA-1","SHA-1"),
	SHA384("SHA-384","SHA-384"),
	RIPEMD160("RIPEMD160","RIPEMD160"),
	SHA("SHA","SHA"),
	WHIRLPOOL("WHIRLPOOL","WHIRLPOOL"),
	GOST3411("GOST3411","GOST3411"),
	SHA224("SHA-224","SHA-224"),
	SHA256("SHA-256","SHA-256"),
	MD2("MD2","MD2"),
	RIPEMD128("RIPEMD128","RIPEMD128"),
	MD4("MD4","MD4"),
	SHA512("SHA-512","SHA-512"),
	RIPEMD256("RIPEMD256","RIPEMD256"),
	TIGER("TIGER","TIGER"),
	MD5("MD5","MD5");

	/**
	 * 获取全部枚举
	 * 
	 * @return List<MessageDigestAlgorithmEnum>
	 */
	public static java.util.List<MessageDigestAlgorithmEnum> getAllEnum() {
		java.util.List<MessageDigestAlgorithmEnum> list = new java.util.ArrayList<MessageDigestAlgorithmEnum>(
				values().length);
		for (MessageDigestAlgorithmEnum _enum : values()) {
			list.add(_enum);
		}
		return list;
	}
	/**
	 * 获取全部枚举值
	 * 
	 * @return List<String>
	 */
	public static java.util.List<String> getAllEnumCode() {
		java.util.List<String> list = new java.util.ArrayList<String>(values().length);
		for (MessageDigestAlgorithmEnum _enum : values()) {
			list.add(_enum.getCode());
		}
		return list;
	}

	public static MessageDigestAlgorithmEnum getEnumByMessage(String message) {
		if (message == null) {
			return null;
		}
		for (MessageDigestAlgorithmEnum _enum : values()) {
			if (_enum.getMessage().equalsIgnoreCase(message)) {
				return _enum;
			}
		}
		return null;
	}

	private String code;

	private String message;

	private MessageDigestAlgorithmEnum(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
