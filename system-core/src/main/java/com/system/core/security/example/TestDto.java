package com.system.core.security.example;

import com.system.core.security.annotations.SignField;

import lombok.Data;

@Data
public class TestDto {

	@SignField
	private int processor;
	private String vendorId;
	private String cpuFamily;
	@SignField
	private String model;
	private String modelName;
	@SignField
	private String flags;
	private float cpuMHz;
	private String cacheSize;
	private String fpu;
	private String addressSizes;
}
