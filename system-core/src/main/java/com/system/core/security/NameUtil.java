package com.system.core.security;

import org.apache.commons.lang3.StringUtils;

import lombok.experimental.UtilityClass;

@UtilityClass
public class NameUtil {
	
	public String javaClassName(String anyName) {
		String javaWorld = _2JavaName(anyName);
		return StringUtils.capitalize(javaWorld);
	}
	
	public String instanceName(String anyName) {
		String javaWorld = _2JavaName(anyName);
		return StringUtils.uncapitalize(javaWorld);
	}

	public String _2JavaName(String name){
		char[] tmp1 = name.toCharArray();
		StringBuffer sb = new StringBuffer();
		int j = -2;
		for (int i = 0; i < tmp1.length; i++) {
			char c = tmp1[i];
			if(i == 0) {
				c = Character.toLowerCase(c);
			}
				
			if(j == (i-1)) {
				c = Character.toUpperCase(c);
			}
			if(c == '_') {
				j = i;
				continue;
			}
			sb.append(c);
		}
		return sb.toString();
	}
	
	public String getClassName(String code) {
		char[] chars = code.toCharArray();
		StringBuilder tmp = new StringBuilder();
		for(int index = 0,remenberIndex = 0;index < chars.length; index++) {
			char mychar = chars[index];
			if(mychar == '_' || mychar == '-') {
				remenberIndex = index;
				continue;
			}
			if(remenberIndex > 0 && index == remenberIndex+1) {
				tmp.append(Character.toUpperCase(mychar));
			} else {
				tmp.append(mychar);
			}
		}
		return StringUtils.capitalize(tmp.toString());
	}
	
	
	public String javaName2_(String src) {
		char[] chars = src.toCharArray();
		StringBuilder sb = new StringBuilder();
		int index = 0;
		for(char c:chars) {
			if(Character.isUpperCase(c)&&index != 0) {
				sb.append("_");
				sb.append(Character.toLowerCase(c));
			} else {
				sb.append(Character.toLowerCase(c));
			}
			index++;
		}
		return sb.toString();
	}
	
	public String getFileExt(String fileName) {
		if (StringUtils.isBlank(fileName)) {
			return null;
		}
		String[] names = fileName.split("\\.");
		if (names.length < 2) {
			return null;
		}
		return names[names.length - 1];
	}
	public String getFileNameNoExt(String fileName) {
		if (StringUtils.isBlank(fileName)) {
			return null;
		}
		String[] names = fileName.split("\\.");
		if (names.length < 2) {
			return fileName;
		} else {
			return StringUtils.removeEnd(fileName, "."+getFileExt(fileName));
		}
	}
}
