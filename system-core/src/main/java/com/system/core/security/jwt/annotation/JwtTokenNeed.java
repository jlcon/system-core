package com.system.core.security.jwt.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * <p>JWT验证注解</p>
 * <p>该注解在方法上，如果有该注解则调用方法需要进行token验证</p>
 * @author 江路
 * @version : JwtTokenNeed.java, v1.0 2020.09.03 09:59:08
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface JwtTokenNeed {

	/**
	 * 是否必需的
	 * @return
	 */
	boolean required() default true;

	/**
	 * 需要Token的类型，required=true时，才有作用
	 * @return
	 */
	int tokenType() default 0;
	
	/**
	 * 仅验证客户端token有效性
	 * @return
	 */
	boolean tokenVerifyOnly() default false;
}
