package com.system.core.security.impl;

import org.apache.commons.codec.digest.DigestUtils;

import com.system.core.security.NameUtil;
import com.system.core.security.SignBuilder;
import com.system.core.security.SignUtil.SignParam;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class WechatMd5Builder implements SignBuilder{
	
	private String key;

	@Override
	public String buildSign(String source) {
		String stringSignTemp = DigestUtils.md5Hex(source).toUpperCase();
		return stringSignTemp;
	}

	@Override
	public String buildSignSrouce(SignParam signParam) {
		StringBuffer stringBuffer = new StringBuffer();
		signParam.getDataMap().forEach((key,value)->{
			stringBuffer.append("&"+NameUtil.javaName2_(key)+"="+value);
		});
		stringBuffer.append("&key="+key);
		stringBuffer.delete(0, 1);
		return stringBuffer.toString();
	}
	

}
