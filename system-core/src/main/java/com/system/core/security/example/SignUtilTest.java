package com.system.core.security.example;

import com.system.core.security.SignUtil;
import com.system.core.security.impl.WechatHMACSHA256Builder;
import com.system.core.security.impl.WechatMd5Builder;

public class SignUtilTest {

	public static void main(String[] args) {
		TestDto testDto = new TestDto();
		testDto.setAddressSizes("a");
		testDto.setCacheSize("b");
		testDto.setCpuFamily("c");
		testDto.setCpuMHz(12.43f);
		testDto.setFlags("intel");
		testDto.setModel("family");
		testDto.setModelName("funk");
		testDto.setProcessor(1);
		testDto.setVendorId("vendors");
		
		String sign = SignUtil.getSign(testDto,new WechatMd5Builder("192006250b4c09247ec02edce69f6a2d"));
		System.out.println("md5 sign -> "+sign);
		sign = SignUtil.getSign(testDto,new WechatHMACSHA256Builder("192006250b4c09247ec02edce69f6a2d"));
		System.out.println("HMAC-SHA256 sign -> "+sign);
		
		WechatDto wechatDto = new WechatDto();
		wechatDto.setAppid("wxd930ea5d5a258f4f");
		wechatDto.setBody("test");
		wechatDto.setMchId("10000100");
		wechatDto.setDeviceInfo("1000");
		wechatDto.setNonceStr("ibuaiVcKdpRxkhJA");
		
		sign = SignUtil.getSign(wechatDto,new WechatMd5Builder("192006250b4c09247ec02edce69f6a2d"));
		System.out.println("md5 sign -> "+sign);
		sign = SignUtil.getSign(wechatDto,new WechatHMACSHA256Builder("192006250b4c09247ec02edce69f6a2d"));
		System.out.println("HMAC-SHA256 sign -> "+sign);
	}
}
