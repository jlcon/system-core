package com.system.core.utils;

public class FileTools {

	public static String getExtension(String fileName) {
		if(fileName.lastIndexOf(".") >= 0) {
			return fileName.substring(fileName.lastIndexOf(".")+1,fileName.length()).toLowerCase();
		} else {
			return null;
		}
	}
	public static String getRealName(String fileName) {
		if(fileName.lastIndexOf(".") >= 0) {
			return fileName.substring(0,fileName.lastIndexOf(".")).toLowerCase();
		} else {
			return fileName;
		}
	}
}
