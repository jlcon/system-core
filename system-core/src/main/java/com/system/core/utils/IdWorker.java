package com.system.core.utils;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Random;

/**
 * @Description:
 * @Author: kaxiaofei
 * @Date: 2020/7/23
 * @Version: v1.0
 */
public class IdWorker {

    private static final long workerIdBits = 5L;  //32 影响ID的长度
    private static final long dataCenterIdBits = 5L; //32 影响ID的长度
    private static final long maxWorkerId = -1L ^ (-1L << workerIdBits); //每个数据中心最大工作节点数
    private static final long maxDataCenterId = -1L ^ (-1L << dataCenterIdBits); //最大数据中心数
    private static final long sequenceBits = 12L; //4096
    private static final long workerIdShift = sequenceBits;
    private static final long dataCenterIdShift = sequenceBits + workerIdBits;
    private static final long timestampLeftShift = sequenceBits + workerIdBits + dataCenterIdBits;
    private static final long sequenceMask = -1L ^ (-1L << sequenceBits);//4096 每毫秒内最大序列值
    private static final Random r = new Random();
    private final long workerId;  //工作节点ID
    private final long dataCenterId; //数据中心ID
    private final long idEpoch; //id纪元（必须小于当前时间）
    private long lastTimestamp = -1L;
    private long sequence;

    public IdWorker() {
        this(1419242747142L);
    }

    public IdWorker(long idEpoch) {
        this(r.nextInt((int) maxDataCenterId), r.nextInt((int) maxWorkerId), 0, idEpoch);
    }

    public IdWorker(long dataCenterId, long workerId) {
        this(dataCenterId, workerId, 1419242747142L);
    }

    public IdWorker(long dataCenterId, long workerId, long idEpoch) {
        this(dataCenterId, workerId, 0, idEpoch);
    }

    public IdWorker(long dataCenterId, long workerId, long sequence, long idEpoch) {
        this.dataCenterId = dataCenterId;
        this.workerId = workerId;
        this.sequence = sequence;
        this.idEpoch = idEpoch;
        if (workerId < 0 || workerId > maxWorkerId) {
            throw new IllegalArgumentException("workerId is illegal: " + workerId);
        }
        if (dataCenterId < 0 || dataCenterId > maxDataCenterId) {
            throw new IllegalArgumentException("dataCenterId is illegal: " + workerId);
        }
        if (idEpoch >= System.currentTimeMillis()) {
            throw new IllegalArgumentException("idEpoch is illegal: " + idEpoch);
        }
    }

    private synchronized long nextId() {
        long timestamp = timeGen();
        if (timestamp < lastTimestamp) {
            throw new IllegalStateException("Clock moved backwards.");
        }
        if (lastTimestamp == timestamp) {
            sequence = (sequence + 1) & sequenceMask;
            if (sequence == 0) {
                timestamp = tilNextMillis(lastTimestamp);
            }
        } else {
            sequence = 0;
        }
        lastTimestamp = timestamp;
        long id = ((timestamp - idEpoch) << timestampLeftShift)//
                | (dataCenterId << dataCenterIdShift)//
                | (workerId << workerIdShift)//
                | sequence;
        return id;
    }

    private long tilNextMillis(long lastTimestamp) {
        long timestamp = timeGen();
        while (timestamp <= lastTimestamp) {
            timestamp = timeGen();
        }
        return timestamp;
    }

    private long timeGen() {
        return System.currentTimeMillis();
    }

    /**
     * 获取一个ID
     */
    public long getId() {
        long id = nextId();
        return id;
    }

    /**
     * 批量获取多个ID
     */
    public long[] getIds(int count) {
        if (count > 1000) {
            throw new IllegalArgumentException("get up to 1000 at most each time");
        }
        long[] ids = new long[count];
        for (int i = 0; i < count; i++) {
            ids[i] = nextId();
        }
        return ids;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("dataCenterId", dataCenterId)
                .append("workerId", workerId)
                .append("idEpoch", idEpoch)
                .append("lastTimestamp", lastTimestamp)
                .append("sequence", sequence)
                .append("sequenceMask", sequenceMask)
                .toString();
    }
}
