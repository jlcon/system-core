package com.system.core.utils;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

import jakarta.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.v3.oas.annotations.media.Schema;

public class IpUtil {
	
	public static String getUserAddrByRequest(HttpServletRequest request) {
		String ip = getUserIpAddr(request);
		String addr = getUserAddrByIPNoCache(ip);
		return addr;
	}
	
	public static String getUserAddrByIPNoCache(String ipaddr) {
		String baidu = "http://opendata.baidu.com/api.php?query=%s&resource_id=6006&oe=utf8";
		String result;
		try {
			result = HttpUtils.get(String.format(baidu, ipaddr));
		} catch (IOException e) {
			e.printStackTrace();
			return StringUtils.EMPTY;
		}
		IPAddr ipaddobject = JSONObject.parseObject(result, new TypeReference<IPAddr>(){});
		if(ipaddobject.getData().size()>0) {
			return ipaddobject.getData().get(0).getLocation();
		} else {
			return StringUtils.EMPTY;
		}
	}
	
	public static String getUserIpAddr(HttpServletRequest request) {
		String ipAddress = null;
		try {
			ipAddress = request.getHeader("x-forwarded-for");
			if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
				ipAddress = request.getHeader("Proxy-Client-IP");
			}
			if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
				ipAddress = request.getHeader("WL-Proxy-Client-IP");
			}
			if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
				ipAddress = request.getRemoteAddr();
				if (ipAddress.equals("127.0.0.1")) {
					// 根据网卡取本机配置的IP
					InetAddress inet = null;
					try {
						inet = InetAddress.getLocalHost();
					} catch (UnknownHostException e) {
						e.printStackTrace();
					}
					ipAddress = inet.getHostAddress();
				}
			}
			// 对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
			if (ipAddress != null && ipAddress.length() > 15) { // "***.***.***.***".length()
				// = 15
				if (ipAddress.indexOf(",") > 0) {
					ipAddress = ipAddress.substring(0, ipAddress.indexOf(","));
				}
			}
		} catch (Exception e) {
			ipAddress = "";
		}
		// ipAddress = this.getRequest().getRemoteAddr();

		return ipAddress;
	}
	
	@lombok.Data
	public static class IPAddr {
		
		@Schema(description = "t")
		@JSONField(name = "t")
		@JsonProperty("t")
		private String t;
		
		@Schema(description = "data")
		@JSONField(name = "data")
		@JsonProperty("data")
		private List<Data> data;
		
		@Schema(description = "setCacheTime")
		@JSONField(name = "set_cache_time")
		@JsonProperty("set_cache_time")
		private String setCacheTime;
		
		@Schema(description = "status")
		@JSONField(name = "status")
		@JsonProperty("status")
		private String status;
		
		
		@lombok.Data
		public static class Data{
			
			@Schema(description = "resourceid")
			@JSONField(name = "resourceid")
			@JsonProperty("resourceid")
			private String resourceid;
			
			@Schema(description = "originQuery")
			@JSONField(name = "OriginQuery")
			@JsonProperty("OriginQuery")
			private String originQuery;
			
			@Schema(description = "dispType")
			@JSONField(name = "disp_type")
			@JsonProperty("disp_type")
			private Integer dispType;
			
			@Schema(description = "origipquery")
			@JSONField(name = "origipquery")
			@JsonProperty("origipquery")
			private String origipquery;
			
			@Schema(description = "fetchkey")
			@JSONField(name = "fetchkey")
			@JsonProperty("fetchkey")
			private String fetchkey;
			
			@Schema(description = "shareImage")
			@JSONField(name = "shareImage")
			@JsonProperty("shareImage")
			private Integer shareImage;
			
			@Schema(description = "origip")
			@JSONField(name = "origip")
			@JsonProperty("origip")
			private String origip;
			
			@Schema(description = "extendedLocation")
			@JSONField(name = "ExtendedLocation")
			@JsonProperty("ExtendedLocation")
			private String extendedLocation;
			
			@Schema(description = "showlamp")
			@JSONField(name = "showlamp")
			@JsonProperty("showlamp")
			private String showlamp;
			
			@Schema(description = "tplt")
			@JSONField(name = "tplt")
			@JsonProperty("tplt")
			private String tplt;
			
			@Schema(description = "titlecont")
			@JSONField(name = "titlecont")
			@JsonProperty("titlecont")
			private String titlecont;
			
			@Schema(description = "roleId")
			@JSONField(name = "role_id")
			@JsonProperty("role_id")
			private Integer roleId;
			
			@Schema(description = "appinfo")
			@JSONField(name = "appinfo")
			@JsonProperty("appinfo")
			private String appinfo;
			
			@Schema(description = "location")
			@JSONField(name = "location")
			@JsonProperty("location")
			private String location;
			
			@Schema(description = "showLikeShare")
			@JSONField(name = "showLikeShare")
			@JsonProperty("showLikeShare")
			private Integer showLikeShare;
		}
	}
}
