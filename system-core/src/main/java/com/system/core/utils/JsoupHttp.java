package com.system.core.utils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

public class JsoupHttp {
	
	public static Map<String,String> getCookieMapFromStr(String str){
		if(StringUtils.isBlank(str)) return null;
		Map<String,String> cookieMap = new HashMap<String, String>();
		String[] strlist = str.split(";");
		for(String tmp:strlist) {
			String[] tmp1 = tmp.split("=");
			cookieMap.put(tmp1[0], tmp1[1]);
		}
		return cookieMap;
	}
	
	public static Document getDocument(String url, Map<String, String> cookies) {
		try {
			Connection con = Jsoup.connect(url);
			if (cookies != null) {
				con = con.cookies(cookies);
			}
			Document document = con.userAgent(
					"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.87 Safari/537.36")
					.timeout(30000).ignoreContentType(true).ignoreHttpErrors(true).get();
			return document;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static JSONArray getJsonArray(String url, Map<String, String> cookies) {
		String tmp = getJsonString(url, cookies);
		Pattern pattern = Pattern.compile("\\[.*\\]");
		Matcher m = pattern.matcher(tmp);
		if (m.find()) {
			try {
				tmp = m.group(0);
				if (StringUtils.isNotBlank(tmp)) {
					JSONArray json = JSONArray.parseArray(tmp);
					return json;
				} else {
					return null;
				}
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		} else {
			return null;
		}
	}

	public static JSONObject getJsonObject(String url, Map<String, String> cookies) {
		String tmp = getJsonString(url, cookies);
		Pattern pattern = Pattern.compile("\\{.*\\}");
		Matcher m = pattern.matcher(tmp);
		if (m.find()) {
			try {
				tmp = m.group(0);
				if (StringUtils.isNotBlank(tmp)) {
					JSONObject json = JSONObject.parseObject(tmp);
					return json;
				} else {
					return null;
				}
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		} else {
			return null;
		}
	}

	public static String getJsonString(String url, Map<String, String> cookies) {
		try {
			return HttpUtils.get(url,cookies);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static LinkedList<String> getJsonString(String url, Map<String, String> cookies, String cssQuery) {
		LinkedList<String> list = new LinkedList<String>();
		try {
			Connection con = Jsoup.connect(url);
			if (cookies != null) {
				con = con.cookies(cookies);
			}
			Document document = con.userAgent(
					"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.87 Safari/537.36")
					.timeout(30000).ignoreContentType(true).ignoreHttpErrors(true).get();
			Elements elements = document.select(cssQuery);
			for (Iterator<Element> itor = elements.iterator(); itor.hasNext();) {
				Element ele = itor.next();
				list.add(ele.text());
			}
			return list;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
}
