package com.system.core.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringTools {

	public static List<String> getByRegexs(String src,String regex) {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(src);
		List<String> result = new ArrayList<String>();
		while(matcher.find()) {
			result.add(matcher.group(0));
		}
		return result;
	}
	
	public static String getByRegex(String src,String regex) {
		List<String> result = getByRegexs(src, regex);
		if(result.size() > 0) {
			return result.get(0);
		} else {
			return null;
		}
	}
}
