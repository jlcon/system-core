package com.system.core.utils.random;

import java.util.UUID;

/**
 * @Description:
 * @Author: kaxiaofei
 * @Date: 2021/6/16
 * @Version: v1.0
 */
public class ShortUUIDUtils {

    /**
     * 定义值的列表
     */
    private static String[] chars = new String[]{"a", "b", "c", "d", "e", "f",
            "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s",
            "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5",
            "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I",
            "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
            "W", "X", "Y", "Z"};


    /**
     * 生成唯一标识值(8位)
     *
     * @return
     */
    public static String generateShortUUID() {
        StringBuffer buffer = new StringBuffer();
        String uuid = UUID.randomUUID().toString().replace("-", "");
        String str;
        for (int i = 0; i < 8; i++) {
            str = uuid.substring(i * 4, i * 4 + 4);
            int x = Integer.parseInt(str, 16);
            buffer.append(chars[x % 0x3E]);
        }
        return buffer.toString();
    }
}
