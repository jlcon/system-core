package com.system.core.utils;

import org.springframework.core.env.Environment;

/**
 * @Description:
 * @Author: tzw
 * @Date: 2021/10/06
 * @Version: v1.0
 */
public class EnvironmentUtils {

    public static final String DEV = "dev";
    public static final String TEST = "test";
    public static final String PROD = "prod";
    public static final String UAT = "uat";

    private static final Environment env = SpringContextUtil.getBean("environment", Environment.class);

    public static Boolean isDev() {
        return checkEnv(DEV);
    }

    public static Boolean isTest() {
        return checkEnv(TEST);
    }

    public static Boolean isProd() {
        return checkEnv(PROD);
    }

    public static Boolean isAws() {
        return checkEnv(UAT);
    }

    public static Boolean isOnline() {
        return isProd() || isAws();
    }

    public static Boolean isNotOnline() {
        return !isOnline();
    }
    
    public static boolean isJUnitTest() {
        for (StackTraceElement element : Thread.currentThread().getStackTrace()) {
            if (element.getClassName().startsWith("org.junit")) {
                return true;
            }
        }
        return false;
    }

    /**
     * 包含其中一个则返回true
     *
     * @param envs
     * @return
     */
    public static Boolean checkEnv(String... envs) {
        if (envs == null)
            return false;
        for (int i = 0; i < envs.length; i++) {
            if (env.getActiveProfiles()[0].equalsIgnoreCase(envs[i])) {
                return true;
            }
        }
        return false;
    }

}
