package com.system.core.tree;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.system.core.tree.dto.TreeConfig;
import com.system.core.view.layui.tree.TreeNode;

@Component
public class TreeService {

    public <T> List<TreeNode> getTree(TreeConfig config, List<T> data) {
        List<TreeNode> result = new ArrayList<>();
        // 遍历数据集合，将每条数据加入对应的父节点下
        for (Object item : data) {
            int id = getIntValue(item, config.getIdField());
            int parentId = getIntValue(item, config.getPidField());
            String nodeName = getNodeNameValue(item, config.getNodeNameField());
            String href = getHrefValue(item, config.getHrefField());

            if (parentId == config.getRootpidValue()) {
                TreeNode rootNode = createNode(id,parentId, nodeName, href); // 创建根节点
                result.add(rootNode); // 将根节点加入结果集合
            }

            TreeNode node = createNode(id,parentId, nodeName, href);
            addNodeToParent(parentId, node, result);
        }
        return result;
    }

    // 递归方法：将节点加入对应的父节点下
    private void addNodeToParent(int parentId, TreeNode node, List<TreeNode> nodes) {
        for (TreeNode item : nodes) {
            if (item.getId() == parentId) {
                item.getChildren().add(node);
                return;
            } else {
                addNodeToParent(parentId, node, item.getChildren());
            }
        }
    }

    // 创建节点对象
    private TreeNode createNode(int id,int pid, String nodeName, String href) {
        TreeNode node = new TreeNode();
        node.setId(id);
        node.setPid(pid);
        node.setTitle(nodeName);
        node.setHref(href);
        node.setChecked(false);
        node.setSpread(false);
        node.setDisabled(false);
        node.setChildren(new ArrayList<TreeNode>());
        return node;
    }

    // 获取指定字段的值
    private int getIntValue(Object item, String fieldName) {
        try {
            Field field = item.getClass().getDeclaredField(fieldName);
            field.setAccessible(true); // 设置为可访问
            Object value = field.get(item);
            if (value instanceof Long) {
                return ((Long) value).intValue();
            } else if (value instanceof Integer) {
                return (Integer) value;
            } else {
                throw new IllegalArgumentException("Field type is not supported");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0; // 如果获取失败，则返回默认值
    }

    private String getNodeNameValue(Object item, String fieldName) {
        try {
            Field field = item.getClass().getDeclaredField(fieldName);
            field.setAccessible(true); // 设置为可访问
            return (String) field.get(item);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null; // 如果获取失败，则返回默认值
    }

    private String getHrefValue(Object item, String fieldName) {
        try {
            Field field = item.getClass().getDeclaredField(fieldName);
            field.setAccessible(true); // 设置为可访问
            return (String) field.get(item);
        } catch (Exception e) {
            // e.printStackTrace();
        }
        return null; // 如果获取失败，则返回默认值
    }
}