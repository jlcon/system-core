package com.system.core.tree.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class TreeConfig {

	private String idField;
	private String pidField;
	private String nodeNameField;
	private String hrefField;
	private Long rootpidValue;
}
