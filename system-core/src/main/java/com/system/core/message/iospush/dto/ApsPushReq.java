package com.system.core.message.iospush.dto;

import lombok.Data;

@Data
public class ApsPushReq {

	private String walletAddress;
	private String alert;
	private String notifyType;
	private String myExtend;
}
