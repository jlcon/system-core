package com.system.core.message.iospush.enums;

import java.util.List;

public enum MessagePushSendStatusEnum {

	UNSEND(0, "未发送"),
	SUCCESS(10, "发送成功");

	private Integer code;
	private String message;

	public static MessagePushSendStatusEnum getEnumByMessage(String message) {
		if (message == null)
			return null;
		for (MessagePushSendStatusEnum _enum : values()) {
			if (_enum.getMessage().equalsIgnoreCase(message)) {
				return _enum;
			}
		}
		return null;
	}

	/**
	 * Get all enums
	 * 
	 * @return List<MessagePushSendStatusEnum>
	 */
	public static List<MessagePushSendStatusEnum> getAllEnum() {
		List<MessagePushSendStatusEnum> list = new java.util.ArrayList<MessagePushSendStatusEnum>(values().length);
		for (MessagePushSendStatusEnum _enum : values()) {
			list.add(_enum);
		}
		return list;
	}

	/**
	 * Get all enums code
	 * 
	 * @return List<String>
	 */
	public static List<Integer> getAllEnumCode() {
		List<Integer> list = new java.util.ArrayList<Integer>(values().length);
		for (MessagePushSendStatusEnum _enum : values()) {
			list.add(_enum.getCode());
		}
		return list;
	}

	private MessagePushSendStatusEnum(Integer code, String message) {
		this.code = code;
		this.message = message;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}