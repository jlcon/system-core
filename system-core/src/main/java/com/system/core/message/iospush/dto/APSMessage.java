package com.system.core.message.iospush.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class APSMessage {

	private Aps aps;
	private String notifyType;
	private String myExtend;
}