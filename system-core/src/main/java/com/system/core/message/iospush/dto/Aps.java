package com.system.core.message.iospush.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Aps {
	
	private String alert;
}
