package com.system.core.message;

public class GenericException extends Exception{

	private static final long serialVersionUID = -656162914402039668L;

	public static final String APP_LOGOUT_CODE = "APP_USER_LOGOUT";
	public static final String APP_EXPIRED_CODE = "APP_EXPIRED_CODE";

    private String code;
    private String message;

    public GenericException(String code, String message){
        this.code = code;
        this.message = message;
    }

    public String getCode(){
        return this.code;
    }
    public void setCode(String code){
        this.code = code;
    }
    public String getMessage(){
        return this.message;
    }
    public void setMassage(String message){
        this.message = message;
    }

}
