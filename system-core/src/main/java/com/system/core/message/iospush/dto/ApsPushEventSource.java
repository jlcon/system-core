package com.system.core.message.iospush.dto;


import com.system.core.message.iospush.enums.ApnsMsgType;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ApsPushEventSource {

	private String walletAddress;
	private ApnsMsgType type;
}
