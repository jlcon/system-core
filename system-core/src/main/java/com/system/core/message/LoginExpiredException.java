package com.system.core.message;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper=false)
public class LoginExpiredException extends Exception{

	private static final long serialVersionUID = -4411547463903860356L;
	
    private String message;

}
