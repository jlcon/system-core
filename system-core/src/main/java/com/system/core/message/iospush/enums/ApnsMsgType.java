package com.system.core.message.iospush.enums;

import java.util.List;

public enum ApnsMsgType {

	NFT铸造成功通知("1", "NFT Casting Notice"),
	NFT出售成功通知("2", "NFT Successful Sale Notice"),
	NFT购买成功通知("3", "NFT purchase success notification"),
	拍卖出价成功通知("4", "Notice of successful auction bid"),
	拍卖结果通知("5", "Notice of auction results"),
	商品购买成功通知("6", "Notification of successful purchase"),
	离线广告收入通知("7", "Offline advertising revenue notification"),
	AR店铺入驻年费通知("8", "Notice of annual fee for AR shop"),
	商品发货通知("9", "Commodity delivery notice"),
	商品退货通知("10", "Commodity Return Notice"),
	即将自动确认到货通知("11", "The arrival notice will be automatically confirmed soon"),
	续费成功通知针对品牌商("12", "You have a new order, please handle it in time"),
	AR店铺不续费下架通知针对品牌商("13", "Your store has left the AR landmark"),
	广告投放被投诉针对广告商("15", "You have been complained about advertising content"),
	广告投诉裁决通知针对广告商("16", "Adjudication results of advertising complaints"),
	广告投放到期结束针对广告商("17", "Your advertisement is due"),
	同意广告位投放针对广告商("18", "The advertising space you applied for has taken effect"),
	不同意广告位投放针对广告商("19", "The advertising space you applied for failed to be released");

	private String code;
	private String message;

	public static ApnsMsgType getEnumByMessage(String message) {
		if (message == null)
			return null;
		for (ApnsMsgType _enum : values()) {
			if (_enum.getMessage().equalsIgnoreCase(message)) {
				return _enum;
			}
		}
		return null;
	}

	/**
	 * Get all enums
	 * 
	 * @return List<ApnsMsgType>
	 */
	public static List<ApnsMsgType> getAllEnum() {
		List<ApnsMsgType> list = new java.util.ArrayList<ApnsMsgType>(values().length);
		for (ApnsMsgType _enum : values()) {
			list.add(_enum);
		}
		return list;
	}

	/**
	 * Get all enums code
	 * 
	 * @return List<String>
	 */
	public static List<String> getAllEnumCode() {
		List<String> list = new java.util.ArrayList<String>(values().length);
		for (ApnsMsgType _enum : values()) {
			list.add(_enum.getCode());
		}
		return list;
	}

	private ApnsMsgType(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}