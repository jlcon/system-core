package com.system.core.message.iospush;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@EnableConfigurationProperties
@Component
@ConfigurationProperties(prefix = "push.apns")
@Data
public class ApnsConfig {

	private String sandboxServer="api.sandbox.push.apple.com:443";
	private String productServer="api.push.apple.com:443";
	private String appid;
	private String teamid;
	private String keyid;
	private String secret;
}
