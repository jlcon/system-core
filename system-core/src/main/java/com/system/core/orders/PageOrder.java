package com.system.core.orders;

/**
 * Order入参基础类
 * @author jlcon
 * @param <T> 返回页面的BEAN对象
 * 
 */
public class PageOrder<T> {

	private int pageNo = 1;
	private int pageSize = 20;
	private T baseOrder;
	
	public int getPageNo() {
		return pageNo;
	}
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(baseOrder.toString());
		builder.append("&pageNo=");
		builder.append(pageNo);
		builder.append("&pageSize=");
		builder.append(pageSize);
		return builder.toString();
	}
	public T getBaseOrder() {
		return baseOrder;
	}
	public void setBaseOrder(T baseOrder) {
		this.baseOrder = baseOrder;
	}
}
