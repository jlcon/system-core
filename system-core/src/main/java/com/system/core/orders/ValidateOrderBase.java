package com.system.core.orders;

import org.springframework.util.Assert;

/**
 * Order验证基础类，提供多种验证bean属性的方法
 * @author jlcon
 */
public abstract class ValidateOrderBase {
	/**
	 * 
	 * <font size="3" color="red">验证文本是否为空</font><br>
	 * @param validateField 验证字段
	 * @param fieldDes 字段名称
	 */
	protected void validateHasText(String validateField, String fieldDes) {
		Assert.hasText(validateField, fieldDes + "不能为空!");
	}
	
	protected void validateHasZore(long value, String fieldDes) {
		Assert.isTrue(value > 0, fieldDes + "不能为空!");
	}
	
	protected void validateNotNull(Object validateField, String fieldDes) {
		Assert.notNull(validateField, fieldDes + "不能为空!");
	}
	
	protected void validateGreaterThan(Number validateField, String fieldDes) {
		Assert.isTrue(validateField.doubleValue() > 0, fieldDes + "必须大于零!");
	}
	
	protected void validateGreaterThanEqualZero(Number validateField, String fieldDes) {
		Assert.isTrue(validateField.doubleValue() >= 0, fieldDes + "必须大于等于零!");
	}
	public abstract void check();
}
