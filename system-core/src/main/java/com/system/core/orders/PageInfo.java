package com.system.core.orders;

/**
 * 页面分页对象
 * @author jlcon
 */
public class PageInfo {

	private int pageNo = 1;
	private int pageSize = 20;
	
	public int getPageNo() {
		return pageNo;
	}
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
}
