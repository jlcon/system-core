package com.system.core.orders;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class OpenApiReqBase {

	@Schema(description = "平台ID")
	@NotBlank(message = "platformId（平台ID）不能为空")
	private String platformId;
	@Schema(description = "API地址")
	@NotBlank(message = "apiUrl不能为空")
	private String apiUrl;
	@Schema(description = "发送请求时间戳")
	@Min(value = 0,message = "timestamp（发送请求时间戳）不能为空")
	private Long timestamp;
	@Schema(description = "签名")
	@NotBlank(message = "sign（签名）不能为空")
	private String sign;
}
