package com.system.core.conf;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@EnableConfigurationProperties
@Component
@ConfigurationProperties(prefix = "app.system")
@Data
public class SystemConfig {

	private String uploadPath;
	private String filePrefix;
	private String fileHostName;
	private Long limitKbSize;
}