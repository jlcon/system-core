package com.system.core.conf.dto;

import com.system.core.results.ResultBase;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ExceptionCommonRsp extends ResultBase {

	private static final long serialVersionUID = 7544102180456442560L;

	private String causeErrorType;

	private String code;

	private String causeParams;

	@Builder
	public ExceptionCommonRsp(String causeErrorType, String code, String causeParams,
			boolean success,String message, String requestPath) {
		super(success, message, requestPath);
		this.causeErrorType = causeErrorType;
		this.code = code;
		this.causeParams = causeParams;
	}
}