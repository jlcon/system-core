package com.system.core.baidumap.dto;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@EnableConfigurationProperties
@Component
@ConfigurationProperties(prefix = "app.baidumap")
@Data
public class BaiduMapConfig {

	private String ak;
	private String sk;
}