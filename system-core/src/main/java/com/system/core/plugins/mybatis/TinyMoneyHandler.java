package com.system.core.plugins.mybatis;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;

import com.system.core.utils.TinyMoney;

@MappedTypes(TinyMoney.class)
@MappedJdbcTypes(JdbcType.DECIMAL)
public class TinyMoneyHandler extends BaseTypeHandler<TinyMoney> {

    @Override
	public void setNonNullParameter(PreparedStatement ps, int i, TinyMoney parameter, JdbcType jdbcType) throws SQLException {
        ps.setBigDecimal(i, parameter.getCent());
    }

    @Override
    public TinyMoney getNullableResult(ResultSet rs, String columnName) throws SQLException {
        BigDecimal result = rs.getBigDecimal(columnName);
        TinyMoney money = TinyMoney.cent(result);
        return money;
    }

    @Override
    public TinyMoney getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        BigDecimal result = rs.getBigDecimal(columnIndex);
        TinyMoney money = TinyMoney.cent(result);
        return money;
    }

    @Override
    public TinyMoney getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        BigDecimal result = cs.getBigDecimal(columnIndex);
        TinyMoney money = TinyMoney.cent(result);
        return money;
    }


}
