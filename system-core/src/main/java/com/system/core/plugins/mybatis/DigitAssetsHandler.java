package com.system.core.plugins.mybatis;

import com.system.core.utils.DigitAssets;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@MappedTypes(DigitAssets.class)
@MappedJdbcTypes(JdbcType.DECIMAL)
public class DigitAssetsHandler extends BaseTypeHandler<DigitAssets> {

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, DigitAssets parameter, JdbcType jdbcType) throws SQLException {
        ps.setBigDecimal(i, parameter == null ? DigitAssets.zero().getNum() : parameter.getNum());
    }

    @Override
    public DigitAssets getNullableResult(ResultSet rs, String columnName) throws SQLException {
        BigDecimal result = rs.getBigDecimal(columnName);
        DigitAssets money = DigitAssets.num(result);
        return money;
    }

    @Override
    public DigitAssets getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        BigDecimal result = rs.getBigDecimal(columnIndex);
        DigitAssets money = DigitAssets.num(result);
        return money;
    }

    @Override
    public DigitAssets getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        BigDecimal result = cs.getBigDecimal(columnIndex);
        DigitAssets money = DigitAssets.num(result);
        return money;
    }


}
