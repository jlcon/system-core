package com.system.core.plugins.mybatis;

import java.util.List;
import java.util.function.Predicate;

import org.apache.commons.lang3.StringUtils;
import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.PluginAdapter;
import org.mybatis.generator.api.dom.java.Field;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.api.dom.java.Interface;
import org.mybatis.generator.api.dom.java.JavaVisibility;
import org.mybatis.generator.api.dom.java.Method;
import org.mybatis.generator.api.dom.java.Parameter;
import org.mybatis.generator.api.dom.java.TopLevelClass;
import org.mybatis.generator.api.dom.xml.Attribute;
import org.mybatis.generator.api.dom.xml.Document;
import org.mybatis.generator.api.dom.xml.TextElement;
import org.mybatis.generator.api.dom.xml.VisitableElement;
import org.mybatis.generator.api.dom.xml.XmlElement;

public class CustomModelPlugin extends PluginAdapter{
	
	@Override
	public boolean modelBaseRecordClassGenerated(TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
		topLevelClass.addImportedType("io.swagger.v3.oas.annotations.media.Schema");
		topLevelClass.addImportedType("jakarta.validation.constraints.Size");
		
//		topLevelClass.addImportedType("lombok.Builder");
		topLevelClass.addImportedType("lombok.Data");
		
//		topLevelClass.addAnnotation("@Builder");
		topLevelClass.addAnnotation("@Data");
		
		List<String> docs = topLevelClass.getJavaDocLines();
		docs.clear();
		docs.add("/**");
		docs.add(" *");
		docs.add(" * "+introspectedTable.getRemarks());
		docs.add(" *");
		docs.add(" * @author 江路");
		docs.add(" **/");
		return true;
	}

	@Override
	public boolean modelFieldGenerated(Field field,
			TopLevelClass topLevelClass, IntrospectedColumn introspectedColumn,
			IntrospectedTable introspectedTable, ModelClassType modelClassType) {
		
		List<String> doclist = field.getJavaDocLines();
		doclist.clear();
		StringBuilder apiModelProperty = new StringBuilder("@Schema(description = \"");
		if(StringUtils.isBlank(introspectedColumn.getRemarks()) && introspectedColumn.isIdentity()) {
			apiModelProperty.append("主键\"");
		} else if(StringUtils.isBlank(introspectedColumn.getRemarks())) {
			apiModelProperty.append(introspectedColumn.getJavaProperty()+"\"");
		} else {
			apiModelProperty.append(introspectedColumn.getRemarks()+"\"");
		}
		if(!introspectedColumn.isNullable()) {
			apiModelProperty.append(")");
		} else {
			apiModelProperty.append(")");
		}
//		System.out.println(">name:"+introspectedColumn.getJavaProperty()+",jdbctype:"+introspectedColumn.getJdbcTypeName()+",length:"+introspectedColumn.getLength());
		doclist.add(apiModelProperty.toString());
		
		if (introspectedColumn.getJdbcTypeName().equalsIgnoreCase("date")) {
			topLevelClass.addImportedType("com.fasterxml.jackson.annotation.JsonFormat");
			StringBuilder jsonFormat = new StringBuilder("@JsonFormat(pattern = \"");
			jsonFormat.append("yyyy-MM-dd");
			jsonFormat.append("\",timezone = \"GMT+8\")");
			doclist.add(jsonFormat.toString());
		}
		
		StringBuilder validate = new StringBuilder();
		if(introspectedColumn.getJdbcTypeName().equalsIgnoreCase("BIGINT")
				||introspectedColumn.getJdbcTypeName().equalsIgnoreCase("INTEGER")) {
			if(!introspectedColumn.isNullable()) {
				topLevelClass.addImportedType("jakarta.validation.constraints.Min");
				topLevelClass.addImportedType("jakarta.validation.constraints.NotNull");
				validate.append("@Min(value = 0,message = \""+introspectedColumn.getJavaProperty()+"必须大于0\")\n\t");
				validate.append("@NotNull(message = \""+introspectedColumn.getJavaProperty()+"字段必填\")");
				doclist.add(validate.toString());
			}
		}
		if(introspectedColumn.getJdbcTypeName().equalsIgnoreCase("varchar")
				||introspectedColumn.getJdbcTypeName().equalsIgnoreCase("char")) {
			validate.append("@Size(max = "+introspectedColumn.getLength()+",message = \""+introspectedColumn.getJavaProperty()+"字段超过字符最大长度限制，最大长度为："+introspectedColumn.getLength()+"\")");
			if(!introspectedColumn.isNullable()) {
				topLevelClass.addImportedType("jakarta.validation.constraints.NotBlank");
				validate.append("\n\t@NotBlank(message = \""+introspectedColumn.getJavaProperty()+"-"+introspectedColumn.getRemarks()+"字段必填\")");
				doclist.add(validate.toString());
			}
		}
		// 条件匹配，非空数字
		else if (!introspectedColumn.isStringColumn() && !introspectedColumn.isNullable()) {
			System.out.println(">非空数字：["+introspectedColumn.getJavaProperty()+":type-"+introspectedColumn.getJdbcTypeName()+"]暂未处理");
		}
		return true;
	}


	@Override
	public boolean modelGetterMethodGenerated(Method method,
			TopLevelClass topLevelClass, IntrospectedColumn introspectedColumn,
			IntrospectedTable introspectedTable, ModelClassType modelClassType) {
		
		List<String> doclist = method.getJavaDocLines();
		doclist.clear();
		doclist.add("/**");
		if(StringUtils.isBlank(introspectedColumn.getRemarks())) {
			doclist.add(" * 获取"+introspectedColumn.getJavaProperty());
			doclist.add(" * @return "+introspectedColumn.getJavaProperty());
		} else {
			doclist.add(" * 获取"+introspectedColumn.getRemarks());
			doclist.add(" * @return "+introspectedColumn.getRemarks());
		}
//		doclist.add(" * @mbggenerated");
		doclist.add(" */");
		return false;
	}

	@Override
	public boolean modelSetterMethodGenerated(Method method,
			TopLevelClass topLevelClass, IntrospectedColumn introspectedColumn,
			IntrospectedTable introspectedTable, ModelClassType modelClassType) {
		
		List<String> doclist = method.getJavaDocLines();
		
		String paranName = method.getParameters().get(0).getName();
		
		doclist.clear();
		doclist.add("/**");
		if(StringUtils.isBlank(introspectedColumn.getRemarks())) {
			doclist.add(" * 设置"+introspectedColumn.getJavaProperty());
		} else {
			doclist.add(" * 设置"+introspectedColumn.getRemarks());
		}
		doclist.add(" * @param "+paranName+" "+introspectedColumn.getRemarks());
//		doclist.add(" * @mbggenerated");
		doclist.add(" */");
		
		return false;
	}

	@Override
	public boolean sqlMapUpdateByPrimaryKeySelectiveElementGenerated(XmlElement element, IntrospectedTable introspectedTable) {
		for(VisitableElement ele:element.getElements()) {
			if(ele instanceof XmlElement) {
				XmlElement setElement = (XmlElement)ele;
				setElement.getElements().removeIf(new Predicate<VisitableElement>() {

					@Override
					public boolean test(VisitableElement t) {
						XmlElement ifElement = (XmlElement)t;
						for(Attribute attr:ifElement.getAttributes()) {
							if(attr.getValue().equalsIgnoreCase("updateTime != null")) {
//								System.out.println("忽略更新updateTime");
								return true;
							}
						}
						return false;
					}
				});
			}
		}
		return super.sqlMapUpdateByPrimaryKeySelectiveElementGenerated(element, introspectedTable);
	}
	
	
	
	@Override
	public boolean validate(List<String> arg0) {
		return true;
	}

	@Override
	public boolean sqlMapDocumentGenerated(Document document, IntrospectedTable introspectedTable) {
		insertBatch(document, introspectedTable);
		updateBatch(document, introspectedTable);
		updateBatch1(document, introspectedTable);
		return super.sqlMapDocumentGenerated(document, introspectedTable);
	}
	
	protected void updateBatch1(Document document, IntrospectedTable introspectedTable) {
		IntrospectedColumn keyColumns = null;
		if(introspectedTable.getPrimaryKeyColumns().size()>0)
			keyColumns = introspectedTable.getPrimaryKeyColumns().get(0);
		if(keyColumns == null)
			return;
		XmlElement batchUpdateEle = new XmlElement("update");
		batchUpdateEle.addAttribute(new Attribute("id", "updateBatchByPrimaryKey"));
		batchUpdateEle.addAttribute(new Attribute("parameterType", "java.util.List"));
		TextElement zhushi = new TextElement("<!--\r\n"
				+ "      WARNING - @mbg.generated\r\n"
				+ "      This element is automatically generated by MyBatis Generator, do not modify.\r\n"
				+ "    -->");
		XmlElement foreachEle = new XmlElement("foreach");
		foreachEle.addAttribute(new Attribute("collection", "list"));
		foreachEle.addAttribute(new Attribute("item", "item"));
		foreachEle.addAttribute(new Attribute("separator", ";"));
		foreachEle.addAttribute(new Attribute("open", ""));
		foreachEle.addAttribute(new Attribute("close", ""));
		foreachEle.addAttribute(new Attribute("index", "index"));
		TextElement updateTableEle = new TextElement("update "+introspectedTable.getAliasedFullyQualifiedTableNameAtRuntime());
		XmlElement setEle = new XmlElement("set");
		List<IntrospectedColumn> columns = introspectedTable.getAllColumns();
		for(IntrospectedColumn column:columns) {
			TextElement ifValue = new TextElement(column.getActualColumnName()+" = #{item."+column.getJavaProperty()+",jdbcType="+column.getJdbcTypeName()+"},");
			setEle.addElement(ifValue);
		}
		foreachEle.addElement(updateTableEle);
		foreachEle.addElement(setEle);
		
		StringBuilder wherestr = new StringBuilder("where ");
		wherestr.append(keyColumns.getActualColumnName()+" = #{");
		wherestr.append("item."+keyColumns.getJavaProperty()+",jdbcType=");
		wherestr.append(keyColumns.getJdbcTypeName()+"}");
		TextElement whereEle = new TextElement(wherestr.toString());
		foreachEle.addElement(whereEle);
		batchUpdateEle.addElement(zhushi);
		batchUpdateEle.addElement(foreachEle);
		document.getRootElement().addElement(batchUpdateEle);
	}

	protected void updateBatch(Document document, IntrospectedTable introspectedTable) {
		IntrospectedColumn keyColumns = null;
		if(introspectedTable.getPrimaryKeyColumns().size()>0)
			keyColumns = introspectedTable.getPrimaryKeyColumns().get(0);
		if(keyColumns == null)
			return;
		XmlElement batchUpdateEle = new XmlElement("update");
		batchUpdateEle.addAttribute(new Attribute("id", "updateBatchByPrimaryKeySelective"));
		batchUpdateEle.addAttribute(new Attribute("parameterType", "java.util.List"));
		TextElement zhushi = new TextElement("<!--\r\n"
				+ "      WARNING - @mbg.generated\r\n"
				+ "      This element is automatically generated by MyBatis Generator, do not modify.\r\n"
				+ "    -->");
		XmlElement foreachEle = new XmlElement("foreach");
		foreachEle.addAttribute(new Attribute("collection", "list"));
		foreachEle.addAttribute(new Attribute("item", "item"));
		foreachEle.addAttribute(new Attribute("separator", ";"));
		foreachEle.addAttribute(new Attribute("open", ""));
		foreachEle.addAttribute(new Attribute("close", ""));
		foreachEle.addAttribute(new Attribute("index", "index"));
		TextElement updateTableEle = new TextElement("update "+introspectedTable.getAliasedFullyQualifiedTableNameAtRuntime());
		XmlElement setEle = new XmlElement("set");
		List<IntrospectedColumn> columns = introspectedTable.getAllColumns();
		for(IntrospectedColumn column:columns) {
			XmlElement ifEle = new XmlElement("if");
			ifEle.addAttribute(new Attribute("test", "item."+column.getJavaProperty()+" != null"));
			
			TextElement ifValue = new TextElement(column.getActualColumnName()+" = #{item."+column.getJavaProperty()+",jdbcType="+column.getJdbcTypeName()+"},");
			ifEle.addElement(ifValue);
			setEle.addElement(ifEle);
		}
		foreachEle.addElement(updateTableEle);
		foreachEle.addElement(setEle);
		
		StringBuilder wherestr = new StringBuilder("where ");
		wherestr.append(keyColumns.getActualColumnName()+" = #{");
		wherestr.append("item."+keyColumns.getJavaProperty()+",jdbcType=");
		wherestr.append(keyColumns.getJdbcTypeName()+"}");
		TextElement whereEle = new TextElement(wherestr.toString());
		foreachEle.addElement(whereEle);
		batchUpdateEle.addElement(zhushi);
		batchUpdateEle.addElement(foreachEle);
		document.getRootElement().addElement(batchUpdateEle);
	}

	protected void insertBatch(Document document, IntrospectedTable introspectedTable) {
		String keyName= "";
		if(introspectedTable.getPrimaryKeyColumns().size()>0)
			keyName = introspectedTable.getPrimaryKeyColumns().get(0).getJavaProperty();
		XmlElement batchInsertEle = new XmlElement("insert");
		batchInsertEle.addAttribute(new Attribute("id", "insertBatch"));
		batchInsertEle.addAttribute(new Attribute("parameterType", "java.util.List"));
		batchInsertEle.addAttribute(new Attribute("useGeneratedKeys", "true"));
		if(StringUtils.isNotBlank(keyName))
			batchInsertEle.addAttribute(new Attribute("keyProperty", keyName));
		TextElement zhushi = new TextElement("<!--\r\n"
				+ "      WARNING - @mbg.generated\r\n"
				+ "      This element is automatically generated by MyBatis Generator, do not modify.\r\n"
				+ "    -->");
		StringBuilder insertsql = new StringBuilder("insert into ");
		insertsql.append(introspectedTable.getAliasedFullyQualifiedTableNameAtRuntime()+" (");
		int i = 1;
		List<IntrospectedColumn> columns = introspectedTable.getAllColumns();
		columns.removeIf(new Predicate<IntrospectedColumn>() {

			@Override
			public boolean test(IntrospectedColumn column) {
				if(column.isIdentity() 
						|| column.getActualColumnName().equalsIgnoreCase("create_time")
						|| column.getActualColumnName().equalsIgnoreCase("update_time"))
					return true;
				else
					return false;
			}
			
		});
		for(IntrospectedColumn column:columns) {
			if(column.isIdentity() 
					|| column.getActualColumnName().equalsIgnoreCase("create_time")
					|| column.getActualColumnName().equalsIgnoreCase("update_time"))
				continue;
			insertsql.append(column.getActualColumnName());
			if(i < columns.size()){
				insertsql.append(",");
			}
			if(i%2==0) {
				insertsql.append("\n\t\t");
			}
			i++;
		}
		insertsql.append(") VALUES");
		TextElement insert1 = new TextElement(insertsql.toString());
		batchInsertEle.addElement(zhushi);
		batchInsertEle.addElement(insert1);
		
		XmlElement foreachEle = new XmlElement("foreach");
		foreachEle.addAttribute(new Attribute("collection", "list"));
		foreachEle.addAttribute(new Attribute("item", "item"));
		foreachEle.addAttribute(new Attribute("separator", ","));
		
		batchInsertEle.addElement(foreachEle);
		StringBuilder valuesql = new StringBuilder("(");
		i = 1;
		for(IntrospectedColumn column:columns) {
			valuesql.append("#{item."+column.getJavaProperty()+",jdbcType="+column.getJdbcTypeName()+"}");
			if(i<columns.size()){
				valuesql.append(",");
			}
			if(i%2 == 0) {
				valuesql.append("\n\t\t");
			}
			i++;
		}
		valuesql.append(")");
		TextElement insert2 = new TextElement(valuesql.toString());
		foreachEle.addElement(insert2);
		document.getRootElement().addElement(batchInsertEle);
	}

	
	@Override
	public boolean clientGenerated(Interface interfaze, IntrospectedTable introspectedTable) {
		IntrospectedColumn keyColumns = null;
		if(introspectedTable.getPrimaryKeyColumns().size()>0)
			keyColumns = introspectedTable.getPrimaryKeyColumns().get(0);
		
		Method batchInsert = new Method("insertBatch");
		Parameter batchInsertParam = new Parameter(
				new FullyQualifiedJavaType("List<"+introspectedTable.getBaseRecordType()+">"),"list");
		batchInsert.addParameter(batchInsertParam);
		batchInsert.setVisibility(JavaVisibility.PUBLIC);
		batchInsert.addJavaDocLine("/**");
		batchInsert.addJavaDocLine(" * @mbg.generated");
		batchInsert.addJavaDocLine(" * 批量插入");
		batchInsert.addJavaDocLine(" */");
		batchInsert.setAbstract(true);
		batchInsert.setReturnType(new FullyQualifiedJavaType("int"));
		
		Method batchUpdate = new Method("updateBatchByPrimaryKeySelective");
		Parameter batchUpdateParam = new Parameter(
				new FullyQualifiedJavaType("List<"+introspectedTable.getBaseRecordType()+">"),"list");
		batchUpdate.addParameter(batchUpdateParam);
		batchUpdate.setVisibility(JavaVisibility.PUBLIC);
		batchUpdate.addJavaDocLine("/**");
		batchUpdate.addJavaDocLine(" * @mbg.generated");
		batchUpdate.addJavaDocLine(" * 批量更新");
		batchUpdate.addJavaDocLine(" */");
		batchUpdate.setAbstract(true);
		batchUpdate.setReturnType(new FullyQualifiedJavaType("int"));
		
		
		Method batchUpdate2 = new Method("updateBatchByPrimaryKey");
		batchUpdate2.addParameter(batchUpdateParam);
		batchUpdate2.setVisibility(JavaVisibility.PUBLIC);
		batchUpdate2.addJavaDocLine("/**");
		batchUpdate2.addJavaDocLine(" * @mbg.generated");
		batchUpdate2.addJavaDocLine(" * 批量更新");
		batchUpdate2.addJavaDocLine(" */");
		batchUpdate2.setAbstract(true);
		batchUpdate2.setReturnType(new FullyQualifiedJavaType("int"));
		
		interfaze.addImportedType(new FullyQualifiedJavaType("java.util.List"));
		interfaze.addMethod(batchInsert);
		if(keyColumns!=null) {
			interfaze.addMethod(batchUpdate);
			interfaze.addMethod(batchUpdate2);
		}
		
		return super.clientGenerated(interfaze, introspectedTable);
	}
}
