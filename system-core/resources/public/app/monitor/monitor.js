layui.use('element', function(){
  var element = layui.element,
  $ = layui.$;
  
  var moitorChart = echarts.init(document.getElementById('moitor-chart'));
  var option = {
		    grid:[
		        {top:10,bottom:"50%"},
		        {top:"55%",bottom:0}
		    ],
		    xAxis: [{
		        type: 'category',
		        boundaryGap: false,
		        gridIndex:0
		    },{
		        type: 'category',
		        boundaryGap: false,
		        gridIndex: 1
		    }],
		    yAxis: [{
		        type: 'value',
		        gridIndex:0
		    },{
		        type: 'value',
		        gridIndex:1
		    }],
		    series: [{
		        data: [820, 932, 901, 934, 1290, 1330, 1320],
		        xAxisIndex:0,
		        yAxisIndex:0,
		        type: 'line',
		        areaStyle: {}
		    },{
		        data: [111, 3, 343, 543, 1290, 2330, 1320],
		        type: 'line',
		        xAxisIndex:1,
		        yAxisIndex:1,
		        areaStyle: {}
		    }]
		};
  moitorChart.setOption(option,true);
});
