layui.define(['table', 'laydate', 'layer', 'form'], function (exports) { //提示：模块也可以依赖其它模块，如：layui.define('layer', callback);
	/* 变量定义 */
	var $ = layui.$,
		table = layui.table,
		layer = layui.layer,
		form = layui.form,
		laydate = layui.laydate,
		windowSize = { width: '700px', height: '600px' },
		searchWindowSize = { width: '1024px', height: '468px' },
		optName;

	/** 定义table **/
	var tableObj = table.render({
		id: 'main-table',
		elem: '#table', //指定原始表格元素选择器（推荐id选择器）
		url: '/boss/boss_opt_log/query-list',
		method: 'post',
		autoSort: false,
		totalRow: false,
		size: 'sm',
		even: true,
		toolbar: '#toolbar',
		defaultToolbar: ['filter'],
		height: 'full-70',
		limits: [10, 30, 50, 100],
		limit: 30,
		page: true,
		cols: [[
			{ type: 'checkbox' },
			{
				field: 'optUserName', title: '操作人', sort: false, width: 100, templet: function (d) {
					switch (d.optUserName) {
						case null:
							return '--';
						case '':
							return '';
						default:
							return d.optUserName;
					}
				}
			},
			{
				field: 'source', title: '来源', sort: false, width: 100, templet: function (d) {
					switch (d.source) {
						case null:
							return '--';
						case '':
							return '';
						case -1:
							return '未知';
						case 1:
							return '后台';
						case 2:
							return 'APP前端';
						default:
							return d.source;
					}
				}
			},
			{
				field: 'comment', title: '操作描述', width: 250, sort: false, templet: function (d) {
					switch (d.comment) {
						case null:
							return d.optItemCode;
						case '':
							return d.optItemCode;
						default:
							if (d.comment.includes("删除")) {
								return "<font color=red>" + d.comment + "</font>";
							} else if (d.comment.includes("编辑") || d.comment.includes("修改")) {
								return "<font color=blue>" + d.comment + "</font>";
							} else {
								return d.comment;
							}
					}
				}
			},
			{
				field: 'params', title: '入参', sort: false, templet: function (d) {
					switch (d.comment) {
						case null:
							return '--';
						case '':
							return '';
						default:
							return d.params;
					}
				}
			},
			{
				field: 'location', title: '操作人位置', sort: false, width: 220, templet: function (d) {
					switch (d.location) {
						case null:
							return d.ipAddr;
						case '':
							return d.ipAddr;
						default:

							return d.location + '(' + d.ipAddr + ')';
					}
				}
			},
			{
				field: 'createTime', title: '记录时间', sort: false, width: 150, templet: function (d) {
					switch (d.createTime) {
						case null:
							return '--';
						case '':
							return '';
						default:
							return d.createTime;
					}
				}
			},
			{ fixed: 'right', title: '操作', toolbar: '#optbar', width: 190 }
		]]
	});

	/** 顶部toolbar事件 **/
	table.on('toolbar(table)', function (obj) {
		switch (obj.event) {
			case 'refresh-opt':
				tableObj.reload();
				break;
			case 'addData-opt':
				var data = obj.data;
				optName = 'new';
				var layerindex = layer.open({
					type: 1,
					area: [windowSize.width, windowSize.height],
					title: '新增/添加',
					content: $('#windows'),
					resize: false,
					move: false,
					maxmin: false,
					shadeClose: false,
					btn: ['确定', '取消'],
					success: function (layero, index) {
						$('#win-form')[0].reset();
					},
					yes: function (index, layero) {
						$('#win-submit').trigger('click');
					}
				});
				//layer.full(layerindex);
				break;
			case 'batch-del-opt':
				var checkStatus = table.checkStatus('main-table');
				if (checkStatus.data.length == 0) {
					layer.alert('请先选择待删除行', { icon: 5 });
					return;
				}
				layer.confirm("你确定批量删除" + checkStatus.data.length + "个项目吗？", { icon: 3, title: '提示' },
					function (index) {//确定回调
						var keys = new Array();
						$.each(checkStatus.data, function (i, item) {
							keys.push(item.logId);
						});
						batchDelete(keys, index);
					}, function (index) {//取消回调
						layer.close(index);
					}
				);
				break;
			case 'clear-select-opt':
				var clearSelectindex = layer.open({
					type: 1,
					area: [200, 100],
					title: '清日志',
					content: $('#clear-log-win'),
					shadeClose: false,
					move: false,
					maxmin: false,
					btn: ['确定', '关闭'],
					success: function (layero, index) {
						$('#win-form-clear')[0].reset();
					},
					yes: function (index, layero) {
						$('#win-form-clear').trigger('click');
					}
				});
				break;
			case 'clear-all-opt':
				layer.confirm("你确定清除所有数据吗？", { icon: 3, title: '提示' },
					function (index) {//确定回调
						$.post(
							"/boss/boss_opt_log/delete_all_item",
							function (data2, textStatus, jqXHR) {
								layer.msg(data2.message);
								tableObj.reload();
							}, "json").error(function (xhr, status, info) {
								layer.msg(xhr.responseJSON.message);
								tableObj.reload();
							});

					}, function (index) {//取消回调
						layer.close(index);
					}
				);
				break;
		}
	});

	/** Windows表单提交事件 **/
	form.on('submit(win-define-submit)', function (data) {
		$.ajax({
			type: 'POST',
			url: '/boss/boss_opt_log_define/add_update_item',
			data: $(data.form).serializeArray(),
			success: function (data) {
				if (data.success) {
					layer.closeAll();
					tableObj.reload();
					layer.msg(data.message);
				} else {
					layer.msg(data.message, { icon: 5, anim: 6, time: 0, btn: ['确定'] });
				}
			},
			error: function (data) {
				layer.msg(data.responseJSON.message, { icon: 5, anim: 6, time: 0, btn: ['确定'] });
			},
			dataType: "json"
		});
		return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
	});

	/** 搜索框搜索事件 **/
	form.on('submit(search-opt)', function (data) {
		tableObj.reload({
			where: data.field,
			page: {
				curr: 1
			}
		});
		return false;
	});

	/** 右侧tool按钮事件 **/
	table.on('tool(table)', function (obj) {
		var data = obj.data;
		switch (obj.event) {
			case 'ignore-opt':

				layer.confirm("是否忽略“" + obj.data.optItemCode + "”？", { icon: 3, title: '提示' },
					function (index) {//确定回调
						$.post(
							"/boss/boss_opt_log_ignore/add_item",
							{ optItemCode: obj.data.optItemCode, enable: true },
							function (data2, textStatus, jqXHR) {
								layer.msg(data2.message);
								tableObj.reload();
							}, "json").error(function (xhr, status, info) {
								layer.msg(xhr.responseJSON.message);
								tableObj.reload();
							});
					}, function (index) {//取消回调
						layer.close(index);
					}
				);
				break;
			case 'define-opt':
				var layerindex = layer.open({
					type: 1,
					area: [windowSize.width, 100],
					title: '定义Code',
					content: $('#define-win'),
					shadeClose: false,
					move: false,
					maxmin: false,
					btn: ['确定', '关闭'],
					success: function (layero, index) {
						$('#optItemCode2-win').val(data.optItemCode);
						$('#codeDefine2-win').val(data.comment);
					},
					yes: function (index, layero) {
						$('#win-define-submit').trigger('click');
					}
				});
				break;
			case 'view-opt':
				optName = 'view';
				var layerindex = layer.open({
					type: 1,
					area: [windowSize.width, windowSize.height],
					title: '查看',
					content: $('#windows'),
					shadeClose: false,
					move: false,
					maxmin: false,
					btn: ['关闭'],
					success: function (layero, index) {
						$('#win-form')[0].reset();
						$.post(
							"/boss/boss_opt_log/get_item_by_key",
							{ logId: data.logId },
							function (data, textStatus, jqXHR) {
								form.val("win-form", data);
							}, "json");
					},
					btn1: function (index, layero) {
						layer.close(index);
					}
				});
				//layer.full(layerindex);
				break;
			case 'del-opt':
				layer.confirm("你确定删除操作人“" + obj.data.optUserName + "”的操作项记录吗？", { icon: 3, title: '提示' },
					function (index) {//确定回调
						var keys = new Array();
						keys.push(obj.data.logId);
						batchDelete(keys, index);
					}, function (index) {//取消回调
						layer.close(index);
					}
				);
				break;
		}
	});

	table.on('sort(table)', function (obj) {
		table.reload('main-table', {
			initSort: obj,
			where: {
				orderField: obj.field,
				orderType: obj.type
			}
		});
	});

	function batchDelete(keys, index) {
		$.ajax({
			type: 'POST',
			url: '/boss/boss_opt_log/delete_item',
			data: JSON.stringify(keys),
			success: function (data) {
				if (data.success) {
					layer.close(index);
				} else {
					layer.msg(data.message, { icon: 5, anim: 6, time: 0, btn: ['确定'] });
				}
				tableObj.reload();
				layer.msg(data.message);
			},
			error: function (data) {
				layer.msg(data.responseText, { icon: 5, anim: 6, title: '错误', time: 0, btn: ['确定'] });
			},
			contentType: "application/json",
			dataType: "json"
		});
	}

	/** 搜索框日历控件渲染___开始 **/
	var createTimeStrStart = laydate.render({
		elem: '#createTimeStrStart',
		type: 'datetime',
		calendar: true,
		done: function (value, date) {
			// 日历选择后触发事件
			createTimeStrEnd.config.min = {
				year: date.year,
				month: date.month - 1,//关键
				date: date.date,
				hours: date.hours,
				minutes: date.minutes,
				seconds: date.seconds
			};
		}
	});
	var createTimeStrEnd = laydate.render({
		elem: '#createTimeStrEnd',
		type: 'datetime',
		calendar: true,
		done: function (value, date) {
			if (value == "") {
				createTimeStrStart.config.max = {
					year: 2099
				}
			} else {
				createTimeStrStart.config.max = {
					year: date.year,
					month: date.month - 1,//关键
					date: date.date,
					hours: date.hours,
					minutes: date.minutes,
					seconds: date.seconds
				};
			}
		}
	});
	/** 搜索框日历控件渲染___结束 **/

	/** Window窗口日历控件渲染___开始 **/
	var createTimeStrStart_win = laydate.render({
		elem: '#createTimeStrStart-win',
		type: 'datetime',
		calendar: true,
		done: function (value, date) {
			createTimeStrEnd_win.config.min = {
				year: date.year,
				month: date.month - 1,//关键
				date: date.date,
				hours: date.hours,
				minutes: date.minutes,
				seconds: date.seconds
			};
		}
	});
	var createTimeStrEnd_win = laydate.render({
		elem: '#createTimeStrEnd-win',
		type: 'datetime',
		calendar: true,
		done: function (value, date) {
			if (value == "") {
				createTimeStrStart_win.config.max = {
					year: 2099
				}
			} else {
				createTimeStrStart_win.config.max = {
					year: date.year,
					month: date.month - 1,//关键
					date: date.date,
					hours: date.hours,
					minutes: date.minutes,
					seconds: date.seconds
				};
			}
		}
	});
	/** Window窗口日历控件渲染___结束 **/
	laydate.render({
		elem: '#createTime-win',
		type: 'datetime',
		calendar: true
	});
	/** 搜索框日历控件渲染___开始 **/
	var updateTimeStrStart = laydate.render({
		elem: '#createTime_0',
		type: 'date',
		calendar: true,
		done: function (value, date) {

		}
	});
	var updateTimeStrEnd = laydate.render({
		elem: '#updateTimeStrEnd',
		type: 'datetime',
		calendar: true,
		done: function (value, date) {
			if (value == "") {
				updateTimeStrStart.config.max = {
					year: 2099
				}
			} else {
				updateTimeStrStart.config.max = {
					year: date.year,
					month: date.month - 1,//关键
					date: date.date,
					hours: date.hours,
					minutes: date.minutes,
					seconds: date.seconds
				};
			}
		}
	});
	/** 搜索框日历控件渲染___结束 **/

	/** Window窗口日历控件渲染___开始 **/
	var updateTimeStrStart_win = laydate.render({
		elem: '#updateTimeStrStart-win',
		type: 'datetime',
		calendar: true,
		done: function (value, date) {
			updateTimeStrEnd_win.config.min = {
				year: date.year,
				month: date.month - 1,//关键
				date: date.date,
				hours: date.hours,
				minutes: date.minutes,
				seconds: date.seconds
			};
		}
	});
	var updateTimeStrEnd_win = laydate.render({
		elem: '#updateTimeStrEnd-win',
		type: 'datetime',
		calendar: true,
		done: function (value, date) {
			if (value == "") {
				updateTimeStrStart_win.config.max = {
					year: 2099
				}
			} else {
				updateTimeStrStart_win.config.max = {
					year: date.year,
					month: date.month - 1,//关键
					date: date.date,
					hours: date.hours,
					minutes: date.minutes,
					seconds: date.seconds
				};
			}
		}
	});
	/** Window窗口日历控件渲染___结束 **/
	laydate.render({
		elem: '#clear-log-laydate-range',
		range: ['#clear-log-start-date', '#clear-log-end-date']
	});

	//输出bossOptLogPage接口
	exports('bossOptLogPage', {});
});