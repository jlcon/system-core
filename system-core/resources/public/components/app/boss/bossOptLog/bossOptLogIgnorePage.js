layui.define(['table','laydate','layer','form'],function(exports){ //提示：模块也可以依赖其它模块，如：layui.define('layer', callback);
  /* 变量定义 */
  	var $ = layui.$,
  	table = layui.table,
  	layer = layui.layer,
  	form = layui.form,
  	laydate = layui.laydate,
  	windowSize = {width:'700px',height:'300px'},
  	searchWindowSize = {width:'1024px',height:'468px'},
  	optName;
  	
  	/** 定义table **/
  	var tableObj = table.render({
  	  id: 'main-table',
  	  elem: '#table', //指定原始表格元素选择器（推荐id选择器）
  	  url: '/boss/boss_opt_log_ignore/query_list',
  	  method:'post',
  	  autoSort : false,
  	  totalRow: false,
  	  size: 'sm',
	  even: true,
  	  toolbar: '#toolbar',
  	  defaultToolbar:['filter'],
  	  height: 'full-70',
  	  limits : [10,30,50,100],
  	  limit: 30,
  	  page: true,
  	  cols: [[
  		  {type:'checkbox'},
  		  {field: 'optItemCode', title: '忽略项code',sort:false,templet: function(d){
  			  switch(d.optItemCode) {
  			  	case null:
  			  		return '--';
  			  	case '':
  			  		return '';
  			  	default:
  			  		return d.optItemCode;
  			  }
  	      }},
  	      {field: 'enable', title: '是否可用',sort:false,width:150,templet: function(d){
  			  switch(d.enable) {
  			  	case true:
  			  		return '是';
  			  	case false:
  			  		return '否';
  			  	default:
  			  		return d.enable;
  			  }
  	      }},
  		  {field: 'updateTime', title: '记录更新时间',sort:false,width:150,templet: function(d){
  			  switch(d.updateTime) {
  			  	case null:
  			  		return '--';
  			  	case '':
  			  		return '';
  			  	default:
  			  		return d.updateTime;
  			  }
  	      }},
  	      {field: 'createTime', title: '记录创建时间',sort:false,width:150,templet: function(d){
  			  switch(d.createTime) {
  			  	case null:
  			  		return '--';
  			  	case '':
  			  		return '';
  			  	default:
  			  		return d.createTime;
  			  }
  	      }},
  		  {fixed: 'right', title:'操作', toolbar: '#optbar', width:165}
  	  ]]
  	});
  	
  	/** 顶部toolbar事件 **/
  	table.on('toolbar(table)', function(obj){
  		switch(obj.event){
  		  case 'refresh-opt':
			    tableObj.reload();
			  break;
  		  case 'addData-opt':
  			  var data = obj.data;
  			  optName = 'new';
  			  var layerindex = layer.open({
  				  type: 1,
  				  area: [windowSize.width,windowSize.height],
  				  title : '新增/添加',
  				  content: $('#windows'),
  				  resize : false,
  				  move : false,
  				  maxmin: false,
  				  shadeClose : false,
  				  btn : [ '确定', '取消' ],
  				  success : function(layero, index){
  					  $('#win-form')[0].reset();
  				  },
				  yes : function(index, layero) {
					  $('#win-submit').trigger('click');
				  }
  				});
  			  //layer.full(layerindex);
	      break;
  		  case 'batch-del-opt':
  			  var checkStatus = table.checkStatus('main-table');
  			  if(checkStatus.data.length ==0){
  				  layer.alert('请先选择待删除行',{icon: 5});
  				  return;
  			  }
  			  layer.confirm("你确定批量删除"+checkStatus.data.length+"个项目吗？", {icon: 3, title:'提示'},
	            function(index){//确定回调
			  		var keys = new Array();
			  		$.each(checkStatus.data,function(i,item){
			  			keys.push(item.logIgnoreId);
			  		});
			  		batchDelete(keys,index);
	            },function (index) {//取消回调
	               layer.close(index);
	            }
		      );
  		  break;
  		}
  	});
  	
  	/** Windows表单提交事件 **/
  	form.on('submit(win-submit)', function(data){
		  $.ajax({
			    type: 'POST',
			    url: optName=='new'?'/boss/boss_opt_log_ignore/add_item':'/boss/boss_opt_log_ignore/update_item',
			    data: $(data.form).serializeArray(),
			    success: function (data) {
		        if(data.success){
		        	layer.closeAll();
		        	tableObj.reload();
		        	layer.msg(data.message);
		        }else{
		            layer.msg(data.message,{icon: 5,anim: 6,time: 0,btn:['确定']});
		        }
			    },
			    error: function(data) {
			        layer.msg(data.responseJSON.message,{icon: 5,anim: 6,time: 0,btn:['确定']});
			    },
			    dataType: "json"
			});
		  return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
		});
  	
  	/** 搜索框搜索事件 **/
  	form.on('submit(search-opt)',function(data){
  		tableObj.reload({
  			where:data.field,
  			page: {
				curr: 1
			}
  		});
  	    return false;
  	});
  	
  	/** 右侧tool按钮事件 **/
  	table.on('tool(table)', function(obj){
  		var data = obj.data;
  		switch(obj.event){
  		  case 'edit-opt':
  			  optName = 'update';
  			  var layerindex = layer.open({
  				  type: 1,
  				  area: [windowSize.width,windowSize.height],
  				  title : '编辑',
  				  content: $('#windows'),
  				  shadeClose : false,
  				  move : false,
  				  maxmin: false,
  				  btn : [ '确定', '取消' ],
  				  success : function(layero, index){
  					  $('#win-form')[0].reset();
  					  $.post(
						  "/boss/boss_opt_log_ignore/get_item_by_key",
						  {logIgnoreId:data.logIgnoreId},
						  function(data, textStatus, jqXHR){
							  form.val("win-form",data);
						  },"json");
  				  },
				  yes : function(index, layero) {
					  $('#win-submit').trigger('click');
				  }
  				});
  			  	//layer.full(layerindex);
  			  break;
  		  case 'view-opt':
  			  optName = 'view';
  			  var layerindex = layer.open({
  				  type: 1,
  				  area: [windowSize.width,windowSize.height],
  				  title : '查看',
  				  content: $('#windows'),
  				  shadeClose : false,
  				  move : false,
  				  maxmin: false,
  				  btn : [ '关闭'],
  				  success : function(layero, index){
  					  $('#win-form')[0].reset();
  					  $.post(
						  "/boss/boss_opt_log_ignore/get_item_by_key",
						  {logIgnoreId:data.logIgnoreId},
						  function(data, textStatus, jqXHR){
							  form.val("win-form",data);
						  },"json");
  				  },
  				  btn1 : function(index, layero) {
  					  layer.closeAll();
				  }
  				});
  			  	//layer.full(layerindex);
  			  break;
  		  case 'del-opt':
  			  layer.confirm("你确定删除项目“"+obj.data.optItemCode+"”吗？", {icon: 3, title:'提示'},
			            function(index){//确定回调
    				  		var keys = new Array();
    				  		keys.push(obj.data.logIgnoreId);
    				  		batchDelete(keys,index);
			            },function (index) {//取消回调
			               layer.close(index);
			            }
			        );
  			  break;
  		}
  	});
  	
  	table.on('sort(table)', function(obj){
	  table.reload('main-table', {
	    initSort: obj,
	    where: {
	      orderField: obj.field,
	      orderType: obj.type
	    }
	  });
	});
  	
  	/** 搜索更多 **/
  	$('#search-more').click(function(){
  		layer.open({
			  type: 1,
			  area: [searchWindowSize.width,searchWindowSize.height],
			  title : '更多查询条件',
			  content: $('#windows-search'),
			  resize : true,
			  shadeClose : true,
			  shade : false,
			  maxmin: false,
			  btn : [ '查询', '关闭' ],
			  success : function(layero, index){
			  },
			  yes : function(index, layero) {
				tableObj.reload({where:form.val('win-search-form')});
			  }
			});
  	});
  	
  	function batchDelete(keys,index){
  		$.ajax({
                type: 'POST',
                url: '/boss/boss_opt_log_ignore/delete_item',
                data: JSON.stringify(keys),
                success: function (data) {
                    if(data.success){
                        layer.close(index);
                    }else{
                        layer.msg(data.message,{icon: 5,anim: 6,time: 0,btn:['确定']});
                    }
                    tableObj.reload();
                    layer.msg(data.message);
                },
                error: function(data) {
                    layer.msg(data.responseText,{icon: 5,anim: 6,title:'错误',time: 0,btn:['确定']});
                },
                contentType: "application/json",
                dataType: "json"
            });
  	}
  	
  	/** 搜索框日历控件渲染___开始 **/
  	var createTimeStrStart = laydate.render({
	    elem: '#createTimeStrStart',
	    type: 'datetime',
	    calendar: true,
	    done: function(value, date){
	    	// 日历选择后触发事件
	    	createTimeStrEnd.config.min = {                    
                      year:date.year,
                      month:date.month-1,//关键
                      date:date.date,
                      hours:date.hours,
                      minutes:date.minutes,
                      seconds:date.seconds
                  };
	    }
	  });
  	var createTimeStrEnd = laydate.render({
	    elem: '#createTimeStrEnd',
	    type: 'datetime',
	    calendar: true,
	    done: function(value, date){
	    	if(value == ""){
	    		createTimeStrStart.config.max = {
	    			year:2099
	    		}
	    	} else {
		    	createTimeStrStart.config.max = {                    
                    year:date.year,
                    month:date.month-1,//关键
                    date:date.date,
                    hours:date.hours,
                    minutes:date.minutes,
                    seconds:date.seconds
                };
	    	}
	    }
	  });
  	/** 搜索框日历控件渲染___结束 **/
  	
  	/** Window窗口日历控件渲染___开始 **/
  	var createTimeStrStart_win = laydate.render({
	    elem: '#createTimeStrStart-win',
	    type: 'datetime',
	    calendar: true,
	    done: function(value, date){
	    	createTimeStrEnd_win.config.min = {                    
                  year:date.year,
                  month:date.month-1,//关键
                  date:date.date,
                  hours:date.hours,
                  minutes:date.minutes,
                  seconds:date.seconds
              };
	    }
	  });
  	var createTimeStrEnd_win = laydate.render({
	    elem: '#createTimeStrEnd-win',
	    type: 'datetime',
	    calendar: true,
	    done: function(value, date){
	    	if(value == ""){
	    		createTimeStrStart_win.config.max = {
	    			year:2099
	    		}
	    	} else {
	    		createTimeStrStart_win.config.max = {                    
                    year:date.year,
                    month:date.month-1,//关键
                    date:date.date,
                    hours:date.hours,
                    minutes:date.minutes,
                    seconds:date.seconds
                };
	    	}
	    }
	  });
  	/** Window窗口日历控件渲染___结束 **/
  	laydate.render({
	    elem: '#createTime-win',
	    type: 'datetime',
	    calendar: true
	  });
  	/** 搜索框日历控件渲染___开始 **/
  	var updateTimeStrStart = laydate.render({
	    elem: '#updateTimeStrStart',
	    type: 'datetime',
	    calendar: true,
	    done: function(value, date){
	    	// 日历选择后触发事件
	    	updateTimeStrEnd.config.min = {                    
                      year:date.year,
                      month:date.month-1,//关键
                      date:date.date,
                      hours:date.hours,
                      minutes:date.minutes,
                      seconds:date.seconds
                  };
	    }
	  });
  	var updateTimeStrEnd = laydate.render({
	    elem: '#updateTimeStrEnd',
	    type: 'datetime',
	    calendar: true,
	    done: function(value, date){
	    	if(value == ""){
	    		updateTimeStrStart.config.max = {
	    			year:2099
	    		}
	    	} else {
		    	updateTimeStrStart.config.max = {                    
                    year:date.year,
                    month:date.month-1,//关键
                    date:date.date,
                    hours:date.hours,
                    minutes:date.minutes,
                    seconds:date.seconds
                };
	    	}
	    }
	  });
  	/** 搜索框日历控件渲染___结束 **/
  	
  	/** Window窗口日历控件渲染___开始 **/
  	var updateTimeStrStart_win = laydate.render({
	    elem: '#updateTimeStrStart-win',
	    type: 'datetime',
	    calendar: true,
	    done: function(value, date){
	    	updateTimeStrEnd_win.config.min = {                    
                  year:date.year,
                  month:date.month-1,//关键
                  date:date.date,
                  hours:date.hours,
                  minutes:date.minutes,
                  seconds:date.seconds
              };
	    }
	  });
  	var updateTimeStrEnd_win = laydate.render({
	    elem: '#updateTimeStrEnd-win',
	    type: 'datetime',
	    calendar: true,
	    done: function(value, date){
	    	if(value == ""){
	    		updateTimeStrStart_win.config.max = {
	    			year:2099
	    		}
	    	} else {
	    		updateTimeStrStart_win.config.max = {                    
                    year:date.year,
                    month:date.month-1,//关键
                    date:date.date,
                    hours:date.hours,
                    minutes:date.minutes,
                    seconds:date.seconds
                };
	    	}
	    }
	  });
  	/** Window窗口日历控件渲染___结束 **/
  	laydate.render({
	    elem: '#updateTime-win',
	    type: 'datetime',
	    calendar: true
	  });
  
  //输出bossOptLogIgnorePage接口
  exports('bossOptLogIgnorePage', {});
});