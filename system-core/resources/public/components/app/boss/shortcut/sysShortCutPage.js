layui.define(['table','laydate','layer','form'],function(exports){ //提示：模块也可以依赖其它模块，如：layui.define('layer', callback);
  var $ = layui.$,
  	table = layui.table,
  	layer = layui.layer,
  	form = layui.form,
  	laydate = layui.laydate;
  	var optName;
  	var tableObj = table.render({
  	  id: 'main-table',
  	  elem: '#table', //指定原始表格元素选择器（推荐id选择器）
  	  url: '/boss/shortcut/query-list',
  	  method:'post',
  	  autoSort : false,
  	  //size: 'sm',
	  even: true,
  	  toolbar: '#toolbar',
  	  height: 'full-70',
  	  limits : [10,30,50,100],
  	  limit: 30,
  	  page: true,
  	  cols: [[
  		  {type:'checkbox'},
  		  {field: 'name', title: '快捷名称',sort:false,width:200,templet: function(d){
  			  switch(d.name) {
  			  	case null:
  			  		return '空';
  			  	case '':
  			  		return '--';
  			  	default:
  			  		return d.name;
  			  }
  	      }},
  		  {field: 'url', title: '快捷地址',sort:false,templet: function(d){
  			  switch(d.url) {
  			  	case null:
  			  		return '空';
  			  	case '':
  			  		return '--';
  			  	default:
  			  		return d.url;
  			  }
  	      }},
  		  {field: 'enable', title: '是否可用',sort:false,width:100,templet: '#enable-switch-tpl'},
  		  {field: 'ownerType', title: '快捷所属',sort:false,width:100,templet: function(d){
  			  switch(d.ownerType) {
  			  	case null:
  			  		return '空';
  			  	case '':
  			  		return '--';
  			  	case 'system':
  			  		return '系统';
  			  	case 'user':
  			  		return '用户';
  			  	default:
  			  		return d.ownerType;
  			  }
  	      }},
  		  {field: 'openType', title: '链接类型',sort:false,width:100,templet: function(d){
  			  switch(d.openType) {
  			  	case null:
  			  		return '空';
  			  	case '':
  			  		return '--';
  			  	case 'linker':
  			  		return '外部链接';
  			  	case 'system':
  			  		return '系统链接';
  			  	default:
  			  		return d.openType;
  			  }
  	      }},
  		  {field: 'icon', title: '快捷图标',sort:false,width:150,templet: function(d){
  			  switch(d.icon) {
  			  	case null:
  			  		return '空';
  			  	case '':
  			  		return '--';
  			  	default:
  			  		return d.icon;
  			  }
  	      }},
  	      {field: 'sort', title: '排序',sort:false,width:100,templet: function(d){
  	    	  switch(d.sort) {
  	    	  case null:
  	    		  return '空';
  	    	  case '':
  	    		  return '--';
  	    	  default:
  	    		  return d.sort;
  	    	  }
  	      }},
  		  {fixed: 'right', title:'操作', toolbar: '#optbar', width:165}
  	  ]],
  	  parseData: function(res){
	  }
  	});
  	
  	/** 顶部toolbar事件 **/
  	table.on('toolbar(table)', function(obj){
  		switch(obj.event){
  		  case 'refresh-opt':
			    tableObj.reload();
			  break;
  		  case 'addData-opt':
  			  var data = obj.data;
  			  optName = 'new';
  			  layer.open({
  				  type: 1,
  				  area: ['700px','400px'],
  				  title : '新增/添加',
  				  content: $('#windows'),
  				  resize : false,
  				  move : false,
  				  shadeClose : false,
  				  btn : [ '确定', '取消' ],
  				  success : function(layero, index){
  					  $('#win-form')[0].reset();
  				  },
				  yes : function(index, layero) {
					  $('#win-submit').trigger('click');
				  }
  				});
	      break;
  		  case 'batch-del-opt':
  			  var checkStatus = table.checkStatus('main-table');
  			  if(checkStatus.data.length ==0){
  				  layer.alert('请先选择待删除行',{icon: 5});
  				  return;
  			  }
  			  layer.confirm("你确定批量删除"+checkStatus.data.length+"个项目吗？", {icon: 3, title:'提示'},
  			            function(index){//确定回调
      				  		var keys = new Array();
      				  		$.each(checkStatus.data,function(i,item){
      				  			keys.push(item.shortCutId);
      				  		});
      				  		batchDelete(keys,index);
  			            },function (index) {//取消回调
  			               layer.close(index);
  			            }
  			        );
  			  break;
  		}
  	});
  	form.on('switch(enable-switch)',function(data,attr){
  		$.ajax({
		    type: 'POST',
		    url: '/boss/shortcut/update-item',
		    data: {shortCutId:$(data.elem).attr('shortcutid'),enable:data.elem.checked},
		    success: function (data) {
		        if(data.success){
		        	layer.msg(data.message);
		        }else{
		            layer.msg(data.message,{icon: 5,anim: 6,time: 0,btn:['确定']});
		        }
		    },
		    error: function(data) {
		        layer.msg(data.responseJSON.message,{icon: 5,anim: 6,time: 0,btn:['确定']});
		    },
		    dataType: "json"
		});
  	});
  	form.on('submit(win-submit)', function(data){
		  $.ajax({
			    type: 'POST',
			    url: optName=='new'?'/boss/shortcut/add-item':'/boss/shortcut/update-item',
			    data: data.field,
			    success: function (data) {
			        if(data.success){
			        	layer.closeAll();
			        	tableObj.reload();
			        	layer.msg(data.message);
			        }else{
			            layer.msg(data.message,{icon: 5,anim: 6,time: 0,btn:['确定']});
			        }
			    },
			    error: function(data) {
			        layer.msg(data.responseJSON.message,{icon: 5,anim: 6,time: 0,btn:['确定']});
			    },
			    dataType: "json"
			});
		  return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
		});
  	
  	/** 右侧tool事件 **/
  	table.on('tool(table)', function(obj){
  		var data = obj.data;
  		switch(obj.event){
  		  case 'edit-opt':
  			  optName = 'update';
  			  layer.open({
  				  type: 1,
  				  area: ['700px','400px'],
  				  title : '编辑',
  				  content: $('#windows'),
  				  shadeClose : false,
  				  move : false,
  				  btn : [ '确定', '取消' ],
  				  success : function(layero, index){
  					  $('#win-form')[0].reset();
  					  $.post(
  							  "/boss/shortcut/get-item-bykey",
  							  {shortCutId:data.shortCutId},
  							  function(data, textStatus, jqXHR){
  								  form.val("win-form",data);
  							  },"json");
  				  },
				  yes : function(index, layero) {
					  $('#win-submit').trigger('click');
				  }
  				});
  			  break;
  		  case 'detail-opt':
  			  optName = 'view';
  			  layer.open({
  				  type: 1,
  				  area: ['700px','400px'],
  				  title : '查看',
  				  content: $('#windows'),
  				  shadeClose : false,
  				  move : false,
  				  btn : [ '取消' ],
  				  success : function(layero, index){
  					  $('#win-form')[0].reset();
  					  $.post(
  							  "/boss/shortcut/get-item-bykey",
  							  {shortCutId:data.shortCutId},
  							  function(data, textStatus, jqXHR){
  								  form.val("win-form",data);
  							  },"json");
  				  },
				  btn1 : function(index, layero) {
					  layer.close(index);
				  }
  				});
  			  break;
  		  case 'del-opt':
  			  layer.confirm("你确定删除项目“"+obj.data.name+"”吗？", {icon: 3, title:'提示'},
			            function(index){//确定回调
    				  		var keys = new Array();
    				  		keys.push(obj.data.shortCutId);
    				  		batchDelete(keys,index);
			            },function (index) {//取消回调
			               layer.close(index);
			            }
			        );
  			  break;
  		}
  	});
  	
  	form.on('submit(search-opt)',function(data){
  		tableObj.reload({where:data.field});
  	    return false;
  	});
  	
  	$('#search-more').click(function(){
  		layer.open({
			  type: 1,
			  area: ['1024px','468px'],
			  title : '更多查询条件',
			  content: $('#windows-search'),
			  resize : true,
			  shadeClose : true,
			  shade : false,
			  btn : [ '查询', '关闭' ],
			  success : function(layero, index){
			  },
			  yes : function(index, layero) {
				tableObj.reload({where:form.val('win-search-form')});
			  }
			});
  	});
  	
  	function batchDelete(keys,index){
  		$.ajax({
                type: 'POST',
                url: '/boss/shortcut/delete-item-bykey',
                data: JSON.stringify(keys),
                success: function (data) {
                    if(data.success){
                        layer.close(index);
                    }else{
                        layer.msg(data.message,{icon: 5,anim: 6,time: 0,btn:['确定']});
                    }
                    tableObj.reload();
                    layer.msg(data.message);
                },
                error: function(data) {
                    layer.msg(data.responseText,{icon: 5,anim: 6,title:'错误',time: 0,btn:['确定']});
                },
                contentType: "application/json",
                dataType: "json"
            });
  	}
  	laydate.render({
	    elem: '#createTimeStrStart',
	    type: 'datetime'
	  });
  	laydate.render({
	    elem: '#createTimeStrEnd',
	    type: 'datetime'
	  });
  	laydate.render({
	    elem: '#createTimeStrStart-win',
	    type: 'datetime'
	  });
  	laydate.render({
	    elem: '#createTimeStrEnd-win',
	    type: 'datetime'
	  });
  	laydate.render({
	    elem: '#createTime-win',
	    type: 'datetime'
	  });
  	laydate.render({
	    elem: '#updateTimeStrStart',
	    type: 'datetime'
	  });
  	laydate.render({
	    elem: '#updateTimeStrEnd',
	    type: 'datetime'
	  });
  	laydate.render({
	    elem: '#updateTimeStrStart-win',
	    type: 'datetime'
	  });
  	laydate.render({
	    elem: '#updateTimeStrEnd-win',
	    type: 'datetime'
	  });
  	laydate.render({
	    elem: '#updateTime-win',
	    type: 'datetime'
	  });
  
  //输出test接口
  exports('sysShortCutPage', {});
});